#!
cd hello
echo make sure your command window is big enough.
echo type r"<enter>" to execute.
echo g "<opcode><enter>" to execute through next "<opcode>"
echo type t"<enter>" to trace, t "<number><enter>" to step "<number>" times
echo type k"<enter>" to see data stack trace.
echo type q"<enter>" to quit.
#sleep 3
awk -f ../awk/interp.awk main.asm 
cd ..

