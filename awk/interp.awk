BEGIN {
    for ( i = 0; i < 256; i++ ) {
        aascii[sprintf("%c",i)]=i
    }

    hexstr["0"]=0
    hexstr["1"]=1
    hexstr["2"]=2
    hexstr["3"]=3
    hexstr["4"]=4
    hexstr["5"]=5
    hexstr["6"]=6
    hexstr["7"]=7
    hexstr["8"]=8
    hexstr["9"]=9
    hexstr["A"]=10
    hexstr["B"]=11
    hexstr["C"]=12
    hexstr["D"]=13
    hexstr["E"]=14
    hexstr["F"]=15
    bin2hex["0000"]="HX0"
    bin2hex["0001"]="HX1"
    bin2hex["0010"]="HX2"
    bin2hex["0011"]="HX3"
    bin2hex["0100"]="HX4"
    bin2hex["0101"]="HX5"
    bin2hex["0110"]="HX6"
    bin2hex["0111"]="HX7"
    bin2hex["1000"]="HX8"
    bin2hex["1001"]="HX9"
    bin2hex["1010"]="HXA"
    bin2hex["1011"]="HXB"
    bin2hex["1100"]="HXC"
    bin2hex["1101"]="HXD"
    bin2hex["1110"]="HXE"
    bin2hex["1111"]="HXF"

    sincos[0]=1
    sincos[1]=0
    sincos[2]=-1
    sincos[3]=0
    sincos[4]=1
    sincos[5]=0
    sincos[6]=-1
    sincos[7]=0
    INSTRSIZE=6
    DOWN=0
    RIGHT=1
    UP=2
    LEFT=3
    dirtext[0]="down"
    dirtext[1]="right"
    dirtext[2]="up"
    dirtext[3]="left"
    dirtext[4]="fore"
    dirtext[5]="back"
    gfdebug=2
    gtextx=0
    gtexty=0

    printf( "%c[2J", 27 )
    drow = 0
    dpag = 0
    crow = 0
    cpag = 0
    ccoloffset = 0
    inibble = 0
    DIRECT_VAL = 0
    INDIRECT_VAL = 1
    STAR_VAL = 2
    
    fnbase=ARGV[1]
    sub( /\.asm/, "", fnbase )
    system( "rm " fnbase ".awk.data.out" )
} {
        parseline($0)
} END {
    init_rowscols()
    grunning=1
    for ( i in aundefvarrow ) {
        if ( !( (i,0) in avariable ) ) {
            printf( "\nVariable %s never defined in row=%d, col=%d\n", i, aundefvarrow[i], aundefvarcol[i] )
            exit(1)
        } else {
            variablefixup(aundefvarrow[i],aundefvarcol[i],aundefvarpag[i],i)
        }
    }
    delete aundefvarrow
    delete aundefvarcol
    delete aundefvarpag
    while ( ( getline < "/dev/stdin" ) > 0 ) {
    if ( $1 == "t" ) {
        if ( $2 == 0 ) {
            ct=1
        } else {
            ct=0+$2
        }
        for ( i = 0; i <= ct; i++ ) {
            exe_marbles(0)
        }
    } else if ( $1 == "k" ) {
        for ( i = 0; i <= idstack; i++ ) {
            printf( "%c[%d;%dH%c[38;5;46m[r%d,c%d]", 27, datamaxrow+1+i, datamaxcol+1, 27, dstackrow[i], dstackcol[i] )
        }
        printf( "%c[%d;%dH%c[38;5;46m{r%d,c%d}", 27, datamaxrow+1+i, datamaxcol+1, 27, drow, dcol )
        printf( "%c[%d;%dH%c[38;5;46midstack=%d,gdstackrow=%d}", 27, datamaxrow+2+i, datamaxcol+1, 27, idstack, gdstackrow )
    } else if ( $1 == "r" ) {
        for ( i=0; i < 1000000; i++ ) {
            exe_marbles(0)
        }
    } else if ( $1 == "R" ) {
        for ( i=0; i < 1000000; i++ ) {
            exe_marbles(0.1)
        }
    } else if ( $1 == "g" ) {
        while ( exe_marbles(0) != $2 ) {
        }
    } else if ( $1 == "q" ) {
        printf( "%c[%d;%dH%c[38;5;46m%d instructions\n", 27, codemaxrow+datamaxrow+4, 1, 27, ccount )
        printf( "data %s [%d,%d], code %s [%d,%d]\n", dirtext[ddirect], drow, dcol, dirtext[cdirect], crow, ccol )
        writememh(fnbase ".awk.data.out", 0, 16384);
        exit(0)
    }
    }
    writememh(fnbase ".awk.data.out", 0, 16384);
}
function getdatabyte(r,c,p,i,ret) {
    ret=0
    for ( i=0; i<8; i++ ) {
        ret *= 2
        if ( (r,c*8+i,p) in adatabit ) {
           if ( adatabit[r,c*8+i,p] != 0 ) {
               ret++
           }
        }
    }
    return( ret )
}
function writememh( fn, istart, iend, i, len, r, c, p ) {
    len = (iend-istart)
    for ( i = 0; i < len; i++ )
        {
        if ( (i%16) == 0 )
            {
            printf( "// 0x%8.8x\n", i ) >>fn
            close( fn )
            }
        r=int((i+istart)/int(256/8))
        c=((i+istart) % int(256/8))
        p=0
        printf( "%2.2x\n", getdatabyte(r,c,p) ) >>fn
        close( fn )
        }
printf( "datamaxcol=%d\n", datamaxcol )
}
function parseline(lline,mline) {
    gsub( /;.*$/, "", lline )
    if ( lline ~ /^#include / ) {
        sub( /^#include /, "", lline )
        while ( ( getline mline < lline ) > 0 ) {
            parseline(mline)
        }
    } else if ( lline ~ /^DATA/ ) {
        gsection = "D"
    } else if ( lline ~ /^CODE/ ) {
        gsection = "C"
    } else if ( lline ~ /^MACRO/ ) {
        gsection = "M"
        gcmac=0
        while ( i=index(lline,"MACRO" ) ) {
            gcmac++
            aimac[gcmac]=i
            sub( /MACRO/, "     ", lline )
        }
    } else if ( lline ~ /^INIT/ ) {
        gsection = "I"
    } else {
        if ( gsection == "I" ) {
            sectioninit(lline)
        } else if ( gsection == "C" ) {
            sectioncode(lline)
        } else if ( gsection == "D" ) {
            sectiondata(lline)
        } else if ( gsection == "M" ) {
            if ( gcmac == 1 ) {
                mline=lline
            } else {
                mline=substr(lline,1,aimac[2]-1)
            }
            sub( / [ ]*$/, "", mline )
            sectionmacro(mline,1)
            if ( gcmac > 1 ) {
                for ( i = 2; i < gcmac; i++ ) {
                    mline=substr(lline,aimac[i],aimac[i+1]-aimac[i])
                    sub( / [ ]*$/, "", mline )
                    sectionmacro(mline,i)
                }
                mline=substr(lline,aimac[gcmac])
                sub( / [ ]*$/, "", mline )
                sectionmacro(mline,gcmac)
            }
        }
    }
}

function ascii( token, row, col, pag, i, val ) {
    val = token
    for ( i=7; i>=0; i-- ) {
        if ( 1 == (val % 2) ) {
            writedatabit(row, col+i, pag, 1)
            printonbit( row, col+i, pag )
        } else {
            writedatabit(row, col+i, pag, 0)
            printoffbit( row, col+i, pag )
        }
        val = int(val/2)
    }
}

function printonbit( row, col, pag ) {
    if ( grunning ) {
        if ( ( glastdrow != row ) || ( glastdcol != col ) || ( glastdpag != pag ) ) {
            printf( "%c[%d;%dH%c[38;5;46m%c[48;5;0m%1.1d", 27, glastdrow+1, glastdcol+1, 27, 27, readdatabit(glastdrow,glastdcol,glastdpag) )
        }
        printf( "%c[%d;%dH%c[38;5;0m%c[48;5;46m%1.1d%c[48;5;0m", 27, row+1, col+1, 27, 27, readdatabit(row,col,pag), 27 )
    } else {
        printf( "%c[%d;%dH%c[38;5;46m%1.1d", 27, row+1, col+1, 27, readdatabit(row,col,pag) )
    }
    glastdrow = row
    glastdcol = col
    glastdpag = pag
}

function printoffbit( row, col, pag ) {
    if ( grunning ) {
        if ( ( glastdrow != row ) || ( glastdcol != col ) || ( glastdpag != pag ) ) {
            printf( "%c[%d;%dH%c[38;5;46m%c[48;5;0m%1.1d", 27, glastdrow+1, glastdcol+1, 27, 27, readdatabit(glastdrow,glastdcol,glastdpag) )
        }
        printf( "%c[%d;%dH%c[38;5;0m%c[48;5;46m%1.1d%c[48;5;0m", 27, row+1, col+1, 27, 27, readdatabit(row,col,pag), 27 )
    } else {
        printf( "%c[%d;%dH%c[38;5;46m%1.1d", 27, row+1, col+1, 27, readdatabit(row,col,pag) )
    }
    glastdrow = row
    glastdcol = col
    glastdpag = pag
}

function exe_marbles(sleep,cinstr,ldrow,ldcol,ldpag,i,val,original,star) {
    printf( "%c[%d;%dH%c[38;5;196m%-*.*s", 27, glastcrow+datamaxrow+2, glastccol*INSTRSIZE+1, 27, INSTRSIZE, INSTRSIZE, acode[glastcrow,glastccol,glastcpag] )
    if ( ccol > codemaxcol ) {
        if ( gcwrapcol == 1 ) {
            ccol %= (codemaxcol+1)
        } else if ( gcwrapcol == -1 ) {
            ccol %= (codemaxcol+1)
            crow = codemaxrow-crow
        }
    } else if ( ccol < 0 ) {
        if ( gcwrapcol == 1 ) {
            ccol += (codemaxcol+1)
            ccol %= (codemaxcol+1)
        } else if ( gcwrapcol == -1 ) {
            ccol += (codemaxcol+1)
            ccol %= (codemaxcol+1)
            crow = codemaxrow-crow
        }
    }
    if ( gcwraprow == 1 ) {
        crow += (codemaxrow+1)
        crow %= (codemaxrow+1)
    }
    cinstr = acode[crow,ccol,cpag]
    original=cinstr
    star = 0
    while ( ( cinstr ~ /^\**[\?!+\-01io]/ ) || ( cinstr ~ /^C[RLUD]/ ) || ( cinstr ~ /^JD[RLUD]/ ) ) {
        if ( cinstr ~ /^\*/ ) {
            star = 1
            cinstr=substr(cinstr,2)
        } else {
            star = 0
        }
        if ( cinstr ~ /^\?/ ) {
            if ( readdatabit(drow,dcol,dpag,star) == 1 ) {
                cinstr = substr(cinstr,2)
            } else {
                cinstr = "NOP"
            }
        } else if ( cinstr ~ /^!/ ) {
            if ( readdatabit(drow,dcol,dpag,star) == 0 ) {
                cinstr = substr(cinstr,2)
            } else {
                cinstr = "NOP"
            }
        } else if ( cinstr ~ /^+/ ) {
            cinstr = substr(cinstr,2)
            fixlastdatapt(star)
            offsetrow( sincos[ddirect], star )
            offsetcol( sincos[ddirect+3], star )
            updatelastdatapt(star)
        } else if ( cinstr ~ /^\-/ ) {
            cinstr = substr(cinstr,2)
            fixlastdatapt(star)
            offsetrow( -sincos[ddirect] )
            offsetcol( -sincos[ddirect+3] )
            updatelastdatapt(star)
        } else if ( cinstr ~ /^[01io]/ ) {
            cinstr=handle01s(cinstr,star)
        } else if ( cinstr ~ /^JD[UDLR]/ ) {
            cinstr = datajumps(cinstr,star)
        } else if ( cinstr ~ /^CD/ ) {
            cinstr = substr(cinstr,3)
            cdirect = 0;
        } else if ( cinstr ~ /^CU/ ) {
            cinstr = substr(cinstr,3)
            cdirect = 2;
        } else if ( cinstr ~ /^CL/ ) {
            cinstr = substr(cinstr,3)
            cdirect = 3;
        } else if ( cinstr ~ /^CR/ ) {
            cinstr = substr(cinstr,3)
            cdirect = 1;
        }
        if ( cinstr == "" ) {
            cinstr="NOP"
        }
    }
    printf( "%c[%d;%dH%c[38;5;0m%c[48;5;196m%-*.*s%c[48;5;0m", 27, 1, datamaxcol+2, 27, 27, INSTRSIZE, INSTRSIZE, original, 27 )
    printf( "%c[%d;%dH%c[38;5;0m%c[48;5;196m%-*.*s%c[48;5;0m", 27, 2, datamaxcol+2, 27, 27, INSTRSIZE, INSTRSIZE, cinstr, 27 )
    if ( ( cinstr == "SKP" ) || ( cinstr == "K" ) ) {
        crow += sincos[cdirect]
        ccol += sincos[cdirect+3]
    } else if ( cinstr == "NOP" ) {
    } else if ( cinstr == "PUSW" ) {
        idstack++
        dstackcol[idstack]=data_ptr_mode*64+code_direction*8+data_direction
        dstackrow[idstack]=drow # placeholder
        dstackpag[idstack]=dpag # placeholder
        idstack++
        dstackcol[idstack]=nibble[1]*16+nibble[0]
        dstackrow[idstack]=nibble[3]*16+nibble[2]
        dstackpag[idstack]=nibble[5]*16+nibble[4]
        idstack++
        dstackcol[idstack]=(register_dw%256)
        dstackrow[idstack]=(int(register_dw/256)%256)
        dstackpag[idstack]=(int(register_dw/65536)%256)
        idstack++
        dstackcol[idstack]=(int(register_dw/(256*65536))%256)
        dstackrow[idstack]=(register_a%256)
        dstackpag[idstack]=(int(register_a/256)%256)
        idstack++
        dstackcol[idstack]=(register_b%256)
        dstackrow[idstack]=(int(register_b/256)%256)
        dstackpag[idstack]=0 #placeholder
    } else if ( cinstr == "POSW" ) {
        register_b=dstackrow[idstack]*256+dstackcol[idstack]
        idstack--
        register_a=dstackpag[idstack]*256+dstackrow[idstack]
        register_dw=dstackcol[idstack]*256*65536
        idstack--
        register_dw+=dstackpag[idstack]*65536+dstackrow[idstack]*256+dstackcol[idstack]
        idstack--
        nibble[0]=(dstackcol[idstack]%16)
        nibble[1]=int(dstackcol[idstack]/16)
        nibble[2]=(dstackrow[idstack]%16)
        nibble[3]=int(dstackrow[idstack]/16)
        nibble[4]=(dstackpag[idstack]%16)
        nibble[5]=int(dstackpag[idstack]/16)
        idstack--
        data_ptr_mode=int(dstackcol[idstack]/64)
        code_direction=(int(dstackcol[idstack]/8)%8)
        data_direction=(dstackcol[idstack]%8)
        idstack--
    } else if ( cinstr == "POC2D" ) {
        drow=cstackrow[icstack]
        dcol=cstackcol[icstack]
        dpag=cstackpag[icstack]
        icstack--
    } else if ( cinstr == "POD2C" ) {
        crow = dstackrow[idstack]
        ccol = dstackcol[idstack]
        cpag = dstackpag[idstack]
        idstack--
    } else if ( cinstr == "DNM" ) {
        data_ptr_mode = DIRECT_VAL;
    } else if ( cinstr == "DIM" ) {
        data_ptr_mode = INDIRECT_VAL;
    } else if ( cinstr == "DSM" ) {
        data_ptr_mode = STAR_VAL;
    } else if ( cinstr == "C<" ) {
        cdirect = ((cdirect+1)%4)
    } else if ( cinstr == "DD" ) {
        ddirect = 0;
    } else if ( cinstr == "DU" ) {
        ddirect = 2;
    } else if ( cinstr == "DL" ) {
        ddirect = 3;
    } else if ( cinstr == "DR" ) {
        ddirect = 1;
    } else if ( cinstr == "C>" ) {
        cdirect = ((cdirect+3)%4)
    } else if ( cinstr == "D<" ) {
        ddirect = ((ddirect+1)%4)
    } else if ( cinstr == "D<>" ) {
        ddirect = ((ddirect+2)%4)
    } else if ( cinstr == "D>" ) {
        ddirect = ((ddirect+3)%4)
    } else if ( cinstr ~ /^JD[UDLR][01+-]*/ ) {
        cinstr = datajumps(cinstr,star)
    } else if ( cinstr == "b+" ) {
        fixlastdatapt(star)
        offsetrow( 8*sincos[ddirect] )
        offsetcol( 8*sincos[ddirect+3] )
        updatelastdatapt(star)
        cinstr=substr(cinstr,3)
    } else if ( cinstr ~ /^b-[01+-]*/ ) {
        fixlastdatapt(star)
        offsetrow( -8*sincos[ddirect] )
        offsetcol( -8*sincos[ddirect+3] )
        updatelastdatapt(star)
        cinstr=substr(cinstr,3)
    } else if ( cinstr == "~" ) {
        writedatabit(drow,dcol,dpag,1 - readdatabit(drow,dcol,dpag))
        fixlastdatapt(star)
        updatelastdatapt(star)
    } else if ( cinstr == "JCD" ) {
        crow += sincos[DOWN]
        ccol += sincos[DOWN+3]
    } else if ( cinstr == "JCR" ) {
        crow += sincos[RIGHT]
        ccol += sincos[RIGHT+3]
    } else if ( cinstr == "JCL" ) {
        crow += sincos[LEFT]
        ccol += sincos[LEFT+3]
    } else if ( cinstr == "JCU" ) {
        crow += sincos[UP]
        ccol += sincos[UP+3]
    } else if ( cinstr == "PUD" ) {
        idstack++
        dstackcol[idstack]=dcol
        dstackrow[idstack]=drow
        dstackpag[idstack]=dpag
    } else if ( cinstr == "POD" ) {
        drow = dstackrow[idstack]
        dcol = dstackcol[idstack]
        dpag = dstackpag[idstack]
        idstack--
    } else if ( cinstr == "TOD" ) {
        drow = dstackrow[idstack]
        dcol = dstackcol[idstack]
        dpag = dstackpag[idstack]
    } else if ( cinstr == "QOD" ) {
        idstack--
    } else if ( cinstr ~ /^XDI[01+-]*/ ) {
        ldrow=dstackrow[idstack]
        ldcol=dstackcol[idstack]
        ldpag=dstackpag[idstack]
        dstackrow[idstack]=nibble[1]*16+nibble[0]
        dstackcol[idstack]=nibble[3]*16+nibble[2]
        dstackpag[idstack]=nibble[5]*16+nibble[4]
        inibble=0
        drow=ldrow
        dcol=ldcol
        dpag=ldpag
        updatelastdatapt(star)
        cinstr=substr(cinstr,4)
    } else if ( cinstr ~ /^XD[01+-]*/ ) {
        ldrow=dstackrow[idstack]
        ldcol=dstackcol[idstack]
        ldpag=dstackpag[idstack]
        dstackrow[idstack]=drow
        dstackcol[idstack]=dcol
        dstackpag[idstack]=dpag
        drow=ldrow
        dcol=ldcol
        dpag=ldpag
        updatelastdatapt(star)
        cinstr=substr(cinstr,3)
    } else if ( cinstr ~ /^PDI[01+-]*/ ) {
        idstack++ #0231 #Side effect
        dstackrow[idstack]=nibble[1]*16+nibble[0]
        dstackcol[idstack]=nibble[3]*16+nibble[2]
        dstackpag[idstack]=nibble[5]*16+nibble[4]
        inibble=0
        cinstr=substr(cinstr,4)
    } else if ( cinstr ~ /^LDI[01+-]*/ ) {
        ldrow=nibble[1]*16+nibble[0]
        ldcol=nibble[3]*16+nibble[2]
        ldpag=nibble[5]*16+nibble[4]
        inibble=0
        drow=ldrow
        dcol=ldcol
        dpag=ldpag
        cinstr=substr(cinstr,4)
    } else if ( cinstr ~ /^PCI[01+-]*/ ) {
        icstack++ #0231 #Side effect
        cstackrow[icstack]=nibble[1]*16+nibble[0]
        cstackcol[icstack]=nibble[3]*16+nibble[2]
        cstackpag[icstack]=nibble[5]*16+nibble[4]
        inibble=0
        cinstr=substr(cinstr,4)
    } else if ( cinstr == "PUC" ) {
        icstack++
        cstackrow[icstack]=crow
        cstackcol[icstack]=ccol
        cstackpag[icstack]=cpag
    } else if ( cinstr == "TOC" ) {
        crow=cstackrow[icstack]
        ccol=cstackcol[icstack]
        cpag=cstackpag[icstack]
    } else if ( cinstr == "POC" ) {
        crow=cstackrow[icstack]
        ccol=cstackcol[icstack]
        cpag=cstackpag[icstack]
        icstack--
    } else if ( cinstr == "QOC" ) {
        icstack--
    } else if ( cinstr ~ /^HX[0-9A-F]$/ ) {
        nibble[inibble++]=index("0123456789ABCDEF", substr(cinstr,3,1))-1
        cinstr=substr(cinstr,4,1)
        inibble = (inibble%6)
    } else if ( cinstr == "XC" ) {
        ldrow=cstackrow[icstack]
        ldcol=cstackcol[icstack]
        ldpag=cstackpag[icstack]
        cstackrow[icstack]=crow
        cstackcol[icstack]=ccol
        cstackpag[icstack]=cpag
        crow=ldrow
        ccol=ldcol
        cpag=ldpag
    } else if ( cinstr ~ /^LCI[01+-]*/ ) {
        ldrow=nibble[1]*16+nibble[0]
        ldcol=nibble[3]*16+nibble[2]
        ldpag=nibble[5]*16+nibble[4]
        inibble=0
        crow=ldrow-sincos[cdirect] #Not a good way
        ccol=ldcol-sincos[cdirect+3] #Not a good way
        cpag=ldpag #This isn't right, don't know what is.
        cinstr=substr(cinstr,4)
    } else if ( cinstr == "WZ" ) {
        #Do nothing here
    } else if ( ( cinstr == "HLT" ) || ( cinstr == "H" ) ) {
        printf( "%c[%d;%dH%c[38;5;46m%d instructions\n", 27, codemaxrow+datamaxrow+4, 1, 27, ccount )
        printf( "data %s [%d,%d,%d], code %s [%d,%d,%d]\n", dirtext[ddirect], drow, dcol, dpag, dirtext[cdirect], crow, ccol, cpag )
        writememh(fnbase ".awk.data.out", 0, 16384);
        exit(0)
    } else {
        printf( "%c[%d;%dH%c[38;5;46m[%d,%d,%d]%s: not defined\n", 27, codemaxrow+datamaxrow+3, 1, 27, crow, ccol, cpag, cinstr )
        printf( "%c[%d;%dH%c[38;5;46m%d instructions\n", 27, codemaxrow+datamaxrow+4, 1, 27, ccount )
        printf( "data %s [%d,%d,%d], code %s [%d,%d,%d]\n", dirtext[ddirect], drow, dcol, dpag, dirtext[cdirect], crow, ccol, cpag )
        writememh(fnbase ".awk.data.out", 0, 16384);
        exit(0)
    }

    star = 0
    while ( cinstr ~ /^\**[+-01]/ ) {
        if ( cinstr ~ /^\*/ ) {
            star = 1
            cinstr=substr(cinstr,2)
        } else {
            star = 0
        }
        if ( cinstr ~ /^+/ ) {
            cinstr = substr(cinstr,2)
            fixlastdatapt(star)
            offsetrow( sincos[ddirect], star )
            offsetcol( sincos[ddirect+3], star )
            updatelastdatapt(star)
        } else if ( cinstr ~ /^-/ ) {
            cinstr = substr(cinstr,2)
            fixlastdatapt(star)
            offsetrow( -sincos[ddirect] )
            offsetcol( -sincos[ddirect+3] )
            #offsetpag( #BUGBUG
            updatelastdatapt(star)
        } else if ( cinstr ~ /^[01io]/ ) {
            cinstr=handle01s(cinstr,star)
        } else if ( cinstr ~ /^CD/ ) {
            cinstr = substr(cinstr,3)
            cdirect = 0;
        } else if ( cinstr ~ /^CU/ ) {
            cinstr = substr(cinstr,3)
            cdirect = 2;
        } else if ( cinstr ~ /^CL/ ) {
            cinstr = substr(cinstr,3)
            cdirect = 3;
        } else if ( cinstr ~ /^CR/ ) {
            cinstr = substr(cinstr,3)
            cdirect = 1;
        }
        if ( cinstr == "" ) {
            cinstr="NOP"
        }
    }
    if ( ( original != "" ) && ( ( cinstr != "WZ" ) || ( 0 == readdatabit(drow,dcol,dpag) ) ) ) {
        crow += sincos[cdirect]
        ccol += sincos[cdirect+3]
        #cpag += BUGBUG
    } else {
        system( "sleep 0.5" )
        val = 0
        for ( i = 0; i < 8; i++ ) {
            val *= 2
            val += readdatabit(0,i,0)
        }
        writedatabit(0,8,0,0,star)
        fixlastdatapt(star)
        updatelastdatapt(star)
        system( "sleep 0.5" )
        printf( "%c[%d;%dH%c[38;5;0m%c[39;5;196m%c%c[39;5;0m", 27, codemaxrow+datamaxrow+3+gtexty, gtextx+1, 27, 27, val, 27 )
        if ( val == 13 ) {
            gtextx=0
            gtexty++
        } else {
            gtextx++
        }
    }
    printf( "%c[%d;%dH%c[38;5;0m%c[48;5;196m%-*.*s%c[48;5;0m", 27, crow+datamaxrow+2, ccol*INSTRSIZE+1, 27, 27, INSTRSIZE, INSTRSIZE, acode[crow,ccol,cpag], 27 )
    glastcrow = crow
    glastccol = ccol
    glastcpag = cpag
    ccount++
    if ( sleep > 0 ) {
        system( sprintf( "sleep %2.1f", sleep ) )
    }
    return original
}

function init_rowscols() {
    ddirect =  ginitddirect
    drow = ginitdrow
    dcol = ginitdcol
    dpag = ginitdpag
    cdirect = ginitcdirect
    crow = ginitcrow
    ccol =  ginitccol
    cpag =  ginitcpag
    idstack = -1
    icstack = -1
    gthis_col=1
    printf( "%c[%d;%dH%c[38;5;0m%c[48;5;196m%-*.*s%c[48;5;0m", 27, crow+datamaxrow+2, ccol*INSTRSIZE+1, 27, 27, INSTRSIZE, INSTRSIZE, acode[crow,ccol,cpag], 27 )
    gdstackrow = 0
    gdstackcol = 0
    gdstackpag = 0
    glastcrow = crow
    glastccol = ccol
    glastcpag = cpag
}

function updatelastdatapt(star) {
    printf( "%c[%d;%dH%c[38;5;0m%c[48;5;46m%1.1d%c[48;5;0m", 27, finaldrow(drow,dcol,dpag,star)+1, finaldcol(drow,dcol,dpag,star)+1, 27, 27, readdatabit(drow,dcol,dpag,star), 27 )
    glastdrow=finaldrow(drow,dcol,dpag)
    glastdcol=finaldcol(drow,dcol,dpag)
}

function fixlastdatapt(star) {
    printf( "%c[%d;%dH%c[38;5;46m%c[48;5;0m%1.1d", 27, glastdrow+1, glastdcol+1, 27, 27, readdatabit(glastdrow,glastdcol,glastdpag,star) )
}
function binstr( len, v, ret ) {
   ret=""
   while ( length( ret ) < len ) {
       ret = "" (v%2) ret
       v = int(v/2)
   }
   return( ret )
}
function readdatabit(row,col,pag,star) {
    if ( INDIRECT_MODE() ) { #This isn't quite right, should be -1 somehow
        pag=dstackpag[idstack+gdstackrow]
        row=dstackrow[idstack+gdstackrow]
        if ( star ) {
            col = dstackcol[idstack+gdstackrow]
        } else {
            col += dstackcol[idstack+gdstackrow] #This might be wrong
        }
    }
    return( adatabit[row,col,pag] )
}
function finaldrow(row,col,pag,star) {
    if ( INDIRECT_MODE() ) { #This isn't quite right, should be -1 somehow
        row=dstackrow[idstack+gdstackrow]
        pag=dstackpag[idstack+gdstackrow]
    }
    return( row )
}
function finaldcol(row,col,pag,star) {
    if ( INDIRECT_MODE() ) { #This isn't quite right, should be -1 somehow
        if ( star ) {
            col = dstackcol[idstack+gdstackrow]
        }
    }
    return( col )
}
function finaldpag(row,col,pag,star) {
    if ( INDIRECT_MODE() ) { #This isn't quite right, should be -1 somehow
        if ( star ) {
            pag = dstackpag[idstack+gdstackrow]
        }
    }
    return( pag )
}
function writedatabit(row,col,pag,val,star) {
    if ( INDIRECT_MODE() ) { #This isn't quite right, should be -1 somehow
        row=dstackrow[idstack+gdstackrow]
        pag=dstackpag[idstack+gdstackrow]
        if ( star ) {
            col = dstackcol[idstack+gdstackrow]
        } else {
            col += dstackcol[idstack+gdstackrow]
        }
    }
    adatabit[row,col,pag]=val
}
function offsetrow(val,star) {
    if ( INDIRECT_MODE() ) { #This isn't quite right, should be -1 somehow
        gdstackrow += val
    } else {
        drow += val
    }
}
function offsetcol(val,star) {
    if ( INDIRECT_MODE() ) { #This isn't quite right, should be -1 somehow
        if ( star ) {
            dstackcol[idstack+gdstackrow] += val
        } else {
            dcol += val
        }
    } else {
        dcol += val
    }
}
function undefinedvariable(row,col,pag,token) {
    aundefvarrow[token]=row
    aundefvarcol[token]=col
    aundefvarpag[token]=pag
}
function variablefixup(row,col,pag,token) {
    acode[row,col,pag]=bin2hex[avariable[token,0]]
    acode[row+1,col,pag]=bin2hex[avariable[token,1]]
    acode[row+2,col,pag]=bin2hex[avariable[token,2]]
    acode[row+3,col,pag]=bin2hex[avariable[token,3]]
    acode[row+4,col,pag]=bin2hex[avariable[token,4]]
    acode[row+5,col,pag]=bin2hex[avariable[token,5]]
    printf( "%c[%d;%dH%c[38;5;9m%-*.*s", 27, row+datamaxrow+2, col*INSTRSIZE+1, 27, INSTRSIZE, INSTRSIZE, acode[row,col,pag] )
    printf( "%c[%d;%dH%c[38;5;9m%-*.*s", 27, row+1+datamaxrow+2, col*INSTRSIZE+1, 27, INSTRSIZE, INSTRSIZE, acode[row+1,col,pag] )
    printf( "%c[%d;%dH%c[38;5;9m%-*.*s", 27, row+2+datamaxrow+2, col*INSTRSIZE+1, 27, INSTRSIZE, INSTRSIZE, acode[row+2,col,pag] )
    printf( "%c[%d;%dH%c[38;5;9m%-*.*s", 27, row+3+datamaxrow+2, col*INSTRSIZE+1, 27, INSTRSIZE, INSTRSIZE, acode[row+3,col,pag] )
    printf( "%c[%d;%dH%c[38;5;9m%-*.*s", 27, row+4+datamaxrow+2, col*INSTRSIZE+1, 27, INSTRSIZE, INSTRSIZE, acode[row+4,col,pag] )
    printf( "%c[%d;%dH%c[38;5;9m%-*.*s", 27, row+5+datamaxrow+2, col*INSTRSIZE+1, 27, INSTRSIZE, INSTRSIZE, acode[row+5,col,pag] )
}
function sectioninit(lline) {
    $0=lline
    if ( $2 == "DATAINIT" ) {
        ginitddirect = 0+$3
        ginitdrow = 0+$4
        ginitdcol = 0+$5
        ginitdpag = 0+$6
    } else if ( $2 == "CODEINIT" ) {
        ginitcdirect = 0+$3
        ginitcrow = 0+$4
        ginitccol = 0+$5
        ginitcpag = 0+$6
    } else if ( $2 == "CODESPACE" ) {
        gcwraprow=0
        gcwrapcol=0
        if ( $4 == "ROWCYLINDER" ) {
            gcwraprow=1
        } else if ( $4 == "CYLINDERCOL" ) {
            gcwrapcol=1
        } else if ( $4 == "KLEINCOL" ) {
            gcwraprow=1
            gcwrapcol=-1
        } else if ( $4 == "TORUS" ) {
            gcwraprow=1
            gcwrapcol=1
        }
    } else if ( $2 == "STACKSPACE" ) {
        stackmax = 0+$3
    } else if ( $2 == "STACKINIT" ) {
        ginitsdirect = 0+$3
        ginitsdrow = 0+$4
        ginitsdcol = 0+$5
        ginitsdpag = 0+$6
    }
}
function sectioncode(lline,i,len,token, nonspc) {
    if ( lline ~ /^x[0-9A-F][0-9A-F]*:[0-9A-F][0-9A-F]*:/ ) {
        token=substr( lline, 2 )
        sub( /^[^:][^:]*;/, "", lline )
        crow=strtohex(token)
        token=substr( lline, 1 )
        ccoloffset = strtohex(token)
        return
    }
    if ( lline ~ /^\$[^:][^:]*:/ ) {
        token = lline
        sub( /:.*/, "", token )
        len = length(token)+1
        sub( /^[^:][^:]*:/, "", lline )
        lline = sprintf( "%*.*s%s", len, len, "", lline )
        nonspc = lline
        sub( /^ [ ]*/, "", nonspc )
        len = index( lline, nonspc )
        ccol = int(len/INSTRSIZE)+ccoloffset
        avariable[token,0]= binstr(4,(crow%16))
        avariable[token,1]= binstr(4,int(crow/16))
        avariable[token,2]= binstr(4,(ccol%16))
        avariable[token,3]= binstr(4,int(ccol/16))
        avariable[token,4]= binstr(4,(cpag%16))
        avariable[token,5]= binstr(4,int(cpag/16))
    }
    if ( len = index( lline, "@" ) ) {
        token = substr( lline, len )
        gsub( /#/, " ", token )
        sub( / .*$/, "", token )
        if ( !( token in amacro ) ) {
            printf( "\nno macro named %s found.\n", token )
            exit(2)
        }
        for ( i = 1; i <= amacro[token]; i++ ) {
            sectioncode( sprintf( "%*.*s%s", len-1, len-1, "", amacro[token,i] ) )
        }
    } else if ( len = index( lline, "$" ) ) {
        token = substr( lline, len )
        gsub( /#/, " ", token )
        sub( / .*$/, "", token )
        ccol = int(len/INSTRSIZE)+ccoloffset
        if ( (token,0) in avariable ) {
            variablefixup(crow,ccol,cpag,token)
        } else {
            undefinedvariable(crow,ccol,cpag,token)
        }
        crow++
    } else {
        gsub( /#/, " ", lline )
        len = length( lline )
        ccol = ccoloffset
        for ( i = 1; i <= len; i += INSTRSIZE ) {
            token = substr( lline, i, INSTRSIZE )
            gsub( / /, "", token )
            if ( token != "" ) {
                acode[crow,ccol,cpag] = token
                printf( "%c[%d;%dH%c[38;5;9m%-*.*s", 27, crow+datamaxrow+2, ccol*INSTRSIZE+1, 27, INSTRSIZE, INSTRSIZE, token )
                initial_code[token]++
            }
            if ( codemaxcol < ccol ) {
               codemaxcol = ccol
            }
            ccol++
        }
        if ( codemaxrow < crow ) {
           codemaxrow = crow
        }
        crow++
    }
}
function sectiondata(lline,fsomethingdone,token,row,col,pag,state,i) {
    fsomethingdone = 0
    state = 0
    col = 0
    row = drow
    pag = dpag
    len = length( lline )
    for ( i = 0; i < len; i++ ) {
        token = substr( lline, i+1, 1 )
        if ( 0 == state ) {
            gsub( / /, "", token )
            if ( token != "" ) {
                if ( ( token == "r" ) || ( token == "u" ) ) {
                    fsomethingdone=1
                    writedatabit(row,col,pag,int(rand()*2))
                    printonbit( row, col, pag )
                    col++
                } else if ( token == "\"" ) {
                    state=1
                } else if ( token == "$" ) {
                    token=substr( lline, i+1 )
                    sub( /:.*$/, "", token )
                    avariable[token,0]= binstr(4,(row%16))
                    avariable[token,1]= binstr(4,int(row/16))
                    avariable[token,2]= binstr(4,(col%16))
                    avariable[token,3]= binstr(4,int(col/16))
                    avariable[token,4]= binstr(4,(pag%16))
                    avariable[token,5]= binstr(4,int(pag/16))
                    while ( (i < len) && ( substr( lline, i, 1 ) != ":" ) ) {
                        i++
                    }
                } else if ( token == "x" ) {
                    token=substr( lline, i+1 )
                    row=strtohex(token)
                    sub( /^x[0-9A-F][0-9A-F]:/, "", token )
                    i += 4
                    if ( token ~ /^[0-9A-F][0-9A-F]:/ ) {
                        col=strtohex(token)
                        sub( /^[0-9A-F][0-9A-F]:/, "", token )
                        i += 3
                    }
                    if ( token ~ /^[0-9A-F][0-9A-F]*:/ ) {
                        pag=strtohex(token)
                        sub( /^[0-9A-F][0-9A-F]:/, "", token )
                        i += 3
                    }
                } else if ( token != "0" ) {
                    fsomethingdone=1
                    writedatabit(row,col,pag,1)
                    printonbit( row, col,pag )
                    col++
                } else {
                    fsomethingdone=1
                    writedatabit(row,col,pag,0)
                    printoffbit( row, col,pag )
                    col++
                }
            }
        } else if ( 1 <= state ) {
            fsomethingdone=1
            if ( 2 == state ) {
                if ( token == "n" ) {
                    token="\n"
                } else if ( token == "t" ) {
                    token="\t"
                } else if ( token == "r" ) {
                    token="\r"
                }
                ascii(aascii[token],row,col,pag)
                col += 8;
                state=1
            } else if ( token == "\"" ) {
                ascii(0,row,col,pag)
                col += 8;
                state=0
            } else if ( token == "\\" ) {
                state=2
            } else {
                ascii(aascii[token],row,col,pag)
                col += 8;
            }
        }
        if ( fsomethingdone ) {
            dcol = col
            if ( datamaxcol < dcol ) {
                datamaxcol = dcol
            }
            if ( datamaxpag < dpag ) {
                datamaxpag = dpag
            }
        }
    }
    if ( fsomethingdone ) {
        drow = row
        dcol = col
        dpag = pag
        if ( datamaxrow < drow ) {
           datamaxrow = drow
        }
        if ( datamaxpag < dpag ) {
           datamaxpag = dpag
        }
        drow++
    }
}
function sectionmacro(lline,maccol) {
    if ( lline != "" ) {
        if ( lline ~ /^@[^=][^=]*=/ ) {
            gmacro[maccol]=lline
            sub( /=.*/, "", gmacro[maccol] )
            gmacroline[maccol]=0
        } else if ( lline ~ /^$/ ) {
            gmacro[maccol]=""
        } else if ( gmacro[maccol] != "" ) {
            gmacroline[maccol]++
            amacro[gmacro[maccol]]=gmacroline[maccol]
            amacro[gmacro[maccol],gmacroline[maccol]]=lline
        }
    }
}
function strtohex(str,val,len,i,ch) {
    val=0
    len=length(str)
    for ( i = 1; i <= len; i++ ) {
        ch = substr(str,i,1)
        if ( ch in hexstr ) {
            val *= 16
            val += hexstr[ch]
        } else {
            return(val)
        }
    }
    return(val)
}
function handle01s(cinstr,star) {
    while ( cinstr ~ /^[01io]/ ) {
        fixlastdatapt(star)
        if ( cinstr ~ /^i/ ) {
            writedatabit(drow,dcol,dpag,1,star)
            updatelastdatapt(star)
        } else if ( cinstr ~ /^o/ ) {
            writedatabit(drow,dcol,dpag,0,star)
            updatelastdatapt(star)
        } else {
            writedatabit(drow,dcol,dpag,0+substr(cinstr,1,1),star)
            updatelastdatapt(star)
            if ( substr(cinstr,2,1) != "-" ) {
                offsetrow( sincos[ddirect] )
                offsetcol( sincos[ddirect+3] )
                #offsetpag(#BUGBUG
            } else {
                cinstr = substr(cinstr,2)
            }
        }
        fixlastdatapt(star)
        cinstr = substr(cinstr,2)
    }
    return(cinstr)
}
function datajumps(cinstr,star) {
    cinstr = substr(cinstr,3)
    fixlastdatapt(star)
    if ( cinstr ~ /^D/ ) {
        offsetrow( sincos[DOWN] )
        offsetcol( sincos[DOWN+3] )
    } else if ( cinstr ~ /^U/ ) {
        offsetrow( sincos[UP] )
        offsetcol( sincos[UP+3] )
    } else if ( cinstr ~ /^L/ ) {
        offsetrow( sincos[LEFT] )
        offsetcol( sincos[LEFT+3] )
    } else if ( cinstr ~ /^R/ ) {
        offsetrow( sincos[RIGHT] )
        offsetcol( sincos[RIGHT+3] )
    }
    updatelastdatapt(star)
    cinstr=substr(cinstr,2)
    return( cinstr )
}
function INDIRECT_MODE() {
    return( data_ptr_mode == INDIRECT_VAL )
}
