BEGIN {
    for ( i = 0; i < 256; i++ ) {
        aascii[sprintf("%c",i)]=i
    }

    mne["NOP"]="00"
    mne["POD"]="01"
    mne["PUD"]="02"
    mne["TOD"]="03"
    mne["QOD"]="04"
    mne["XD"]="05"
    mne["XDI"]="06"
    mne["LDI"]="07"
    mne["SKP"]="08"
    mne["POC"]="09"
    mne["PUC"]="0A"
    mne["TOC"]="0B"
    mne["QOC"]="0C"
    mne["XC"]="0D"
    mne["XCI"]="0E"
    mne["LCI"]="0F"
    mne["DD"]="10"
    mne["DR"]="11"
    mne["DU"]="12"
    mne["DL"]="13"
    mne["JDD"]="14"
    mne["JDR"]="15"
    mne["JDU"]="16"
    mne["JDL"]="17"
    mne["CD"]="18"
    mne["CR"]="19"
    mne["CU"]="1A"
    mne["CL"]="1B"
    mne["JCD"]="1C"
    mne["JCR"]="1D"
    mne["JCU"]="1E"
    mne["JCL"]="1F"
    mne["HX0"]="20"
    mne["HX1"]="21"
    mne["HX2"]="22"
    mne["HX3"]="23"
    mne["HX4"]="24"
    mne["HX5"]="25"
    mne["HX6"]="26"
    mne["HX7"]="27"
    mne["HX8"]="28"
    mne["HX9"]="29"
    mne["HXA"]="2A"
    mne["HXB"]="2B"
    mne["HXC"]="2C"
    mne["HXD"]="2D"
    mne["HXE"]="2E"
    mne["HXF"]="2F"
    mne["-"]="30"
    mne["---"]="31"
    mne["b-"]="32"
    mne["d-"]="33"
    mne["+"]="34"
    mne["+++"]="35"
    mne["b+"]="36"
    mne["d+"]="37"
    mne["*-"]="38"
    mne["*+"]="39"
    mne["0"]="40"
    mne["1"]="41"
    mne["o"]="42"
    mne["i"]="43"
    mne["*0"]="44"
    mne["*1"]="45"
    mne["*o"]="46"
    mne["*i"]="47"
    mne["+o"]="48"
    mne["+i"]="49"
    mne["*+o"]="4A"
    mne["*+i"]="4B"
    mne["PDI"]="4E"
    mne["PCI"]="4F"
    mne["ADD"]="50"
    mne["ADC"]="51"
    mne["SUB"]="52"
    mne["SBC"]="53"
    mne["MUL"]="54"
    mne["LDA"]="58"
    mne["LDB"]="59"
    mne["WRA"]="5A"
    mne["WRB"]="5B"
    mne["LIA"]="5C"
    mne["LIB"]="5D"
    mne["PUSW"]="70"
    mne["POSW"]="71"
    mne["POC2D"]="72"
    mne["POD2C"]="73"
    mne["DNM"]="74"
    mne["DIM"]="75"
    mne["DSM"]="76"
    mne["WZ"]="7A"
    mne["W1"]="7B"
    mne["*WZ"]="7C"
    mne["*W1"]="7D"
    mne["!DD"]="90"
    mne["!DR"]="91"
    mne["!DU"]="92"
    mne["!DL"]="93"
    mne["!JDD"]="94"
    mne["!JDR"]="95"
    mne["!JDU"]="96"
    mne["!JDL"]="97"
    mne["!CD"]="98"
    mne["!CR"]="99"
    mne["!CU"]="9A"
    mne["!CL"]="9B"
    mne["!JCD"]="9C"
    mne["!JCR"]="9D"
    mne["!JCU"]="9E"
    mne["!JCL"]="9F"
    mne["?DD"]="B0"
    mne["?DR"]="B1"
    mne["?DU"]="B2"
    mne["?DL"]="B3"
    mne["?JDD"]="B4"
    mne["?JDR"]="B5"
    mne["?JDU"]="B6"
    mne["?JDL"]="B7"
    mne["?CD"]="B8"
    mne["?CR"]="B9"
    mne["?CU"]="BA"
    mne["?CL"]="BB"
    mne["?JCD"]="BC"
    mne["?JCR"]="BD"
    mne["?JCU"]="BE"
    mne["?JCL"]="BF"
    mne["HLT"]="79"
    mne["*!DD"]="D0"
    mne["*!DR"]="D1"
    mne["*!DU"]="D2"
    mne["*!DL"]="D3"
    mne["*!JDD"]="D4"
    mne["*!JDR"]="D5"
    mne["*!JDU"]="D6"
    mne["*!JDL"]="D7"
    mne["*!CD"]="D8"
    mne["*!CR"]="D9"
    mne["*!CU"]="DA"
    mne["*!CL"]="DB"
    mne["*!JCD"]="DC"
    mne["*!JCR"]="DD"
    mne["*!JCU"]="DE"
    mne["*!JCL"]="DF"
    mne["*?DD"]="F0"
    mne["*?DR"]="F1"
    mne["*?DU"]="F2"
    mne["*?DL"]="F3"
    mne["*?JDD"]="F4"
    mne["*?JDR"]="F5"
    mne["*?JDU"]="F6"
    mne["*?JDL"]="F7"
    mne["*?CD"]="F8"
    mne["*?CR"]="F9"
    mne["*?CU"]="FA"
    mne["*?CL"]="FB"
    mne["*?JCD"]="FC"
    mne["*?JCR"]="FD"
    mne["*?JCU"]="FE"
    mne["*?JCL"]="FF"

    hexstr["0"]=0
    hexstr["1"]=1
    hexstr["2"]=2
    hexstr["3"]=3
    hexstr["4"]=4
    hexstr["5"]=5
    hexstr["6"]=6
    hexstr["7"]=7
    hexstr["8"]=8
    hexstr["9"]=9
    hexstr["A"]=10
    hexstr["B"]=11
    hexstr["C"]=12
    hexstr["D"]=13
    hexstr["E"]=14
    hexstr["F"]=15
    bin2hex["0000"]="HX0"
    bin2hex["0001"]="HX1"
    bin2hex["0010"]="HX2"
    bin2hex["0011"]="HX3"
    bin2hex["0100"]="HX4"
    bin2hex["0101"]="HX5"
    bin2hex["0110"]="HX6"
    bin2hex["0111"]="HX7"
    bin2hex["1000"]="HX8"
    bin2hex["1001"]="HX9"
    bin2hex["1010"]="HXA"
    bin2hex["1011"]="HXB"
    bin2hex["1100"]="HXC"
    bin2hex["1101"]="HXD"
    bin2hex["1110"]="HXE"
    bin2hex["1111"]="HXF"

    sincos[0]=1
    sincos[1]=0
    sincos[2]=-1
    sincos[3]=0
    sincos[4]=1
    sincos[5]=0
    sincos[6]=-1
    sincos[7]=0
    INSTRSIZE=6
    DOWN=0
    RIGHT=1
    UP=2
    LEFT=3
    dirtext[0]="down"
    dirtext[1]="right"
    dirtext[2]="up"
    dirtext[3]="left"
    dirtext[4]="fore"
    dirtext[5]="back"
    gfdebug=2
    gtextx=0
    gtexty=0
    drow = 0
    dpag = 0
    crow = 0
    cpag = 0
    ccoloffset = 0
} {
        fnbase=FILENAME
        sub(/\.asm/, "", fnbase )
        parseline($0)
} END {
    init_rowscols()
    grunning=1
    for ( i in aundefvarrow ) {
        if ( !( (i,0) in avariable ) ) {
            printf( "\nVariable %s never defined in row=%d, col=%d\n", i, aundefvarrow[i], aundefvarcol[i] )
            exit(1)
        } else {
            variablefixup(aundefvarrow[i],aundefvarcol[i],aundefvarpag[i],i)
        }
    }
    delete aundefvarrow
    delete aundefvarcol
    delete aundefvarpag
    fn=fnbase ".awk.data.hex"
    printf( "" ) >fn
    close(fn)
    for ( ipag = 0; ipag < 2; ipag++ ) {
        for ( irow = 0; irow < 32*8; irow++ ) {
            for ( icol = 0; icol < 256/8; icol++ ) {
                pwr=128
                byte=0
                for ( ibit = 0; ibit < 8; ibit++ ) {
                    if ( (irow,(icol*8+ibit),ipag) in adatabit ) {
                        if ( adatabit[irow,(icol*8+ibit),ipag] ) { #BUGBUG
                            byte += pwr
                        }
                    }
                    pwr/=2
                }
               printf( "%2.2x ", byte ) >>fn
               close(fn)
           }
           printf( "\n" ) >>fn
           close(fn)
        }
    }
    fn=fnbase ".awk.code.hex"
    close(fn)
    printf( "\n" ) >fn
    ipag=0
    for ( irow = 0; irow < 128; irow++ ) {
       for ( icol = 0; icol < 128; icol++ ) {
           if ( (irow,icol,ipag) in acode ) {
               if ( acode[irow,icol,ipag] in mne ) {
                   printf( "%2.2s", mne[acode[irow,icol,ipag]] ) >>fn
               } else {
                   printf( "%s not in mne\n", acode[irow,icol,ipag] )
                   exit(0)
               }
               close(fn)
           } else {
               printf( "00" ) >>fn
               close(fn)
           }
           if ( icol && ( (icol%16) == 0 ) ) {
               if ( (icol%128) != 0 ) {
                   printf( "\n    " ) >>fn
                   close(fn)
               }
           } else {
               printf( " " ) >>fn
               close(fn)
           }
       }
       printf( "\n" ) >>fn
       close(fn)
    }
}
function parseline(lline,mline) {
    gsub( /;.*$/, "", lline )
    if ( lline ~ /^#include / ) {
        sub( /^#include /, "", lline )
        while ( ( getline mline < lline ) > 0 ) {
            parseline(mline)
        }
    } else if ( lline ~ /^DATA/ ) {
        gsection = "D"
    } else if ( lline ~ /^CODE/ ) {
        gsection = "C"
    } else if ( lline ~ /^MACRO/ ) {
        gsection = "M"
        gcmac=0
        while ( i=index(lline,"MACRO" ) ) {
            gcmac++
            aimac[gcmac]=i
            sub( /MACRO/, "     ", lline )
        }
    } else if ( lline ~ /^INIT/ ) {
        gsection = "I"
    } else {
        if ( gsection == "I" ) {
            sectioninit(lline)
        } else if ( gsection == "C" ) {
            sectioncode(lline)
        } else if ( gsection == "D" ) {
            sectiondata(lline)
        } else if ( gsection == "M" ) {
            if ( gcmac == 1 ) {
                mline=lline
            } else {
                mline=substr(lline,1,aimac[2]-1)
            }
            sub( / [ ]*$/, "", mline )
            sectionmacro(mline,1)
            if ( gcmac > 1 ) {
                for ( i = 2; i < gcmac; i++ ) {
                    mline=substr(lline,aimac[i],aimac[i+1]-aimac[i])
                    sub( / [ ]*$/, "", mline )
                    sectionmacro(mline,i)
                }
                mline=substr(lline,aimac[gcmac])
                sub( / [ ]*$/, "", mline )
                sectionmacro(mline,gcmac)
            }
        }
    }
}

function ascii( token, row, col, pag, i, val ) {
    val = token
    for ( i=7; i>=0; i-- ) {
        if ( 1 == (val % 2) ) {
            writedatabit(row,col+i,pag,1)
        } else {
            writedatabit(row,col+i,pag,0)
        }
        val = int(val/2)
    }
}

function init_rowscols() {
    ddirect =  ginitddirect
    drow = ginitdrow
    dcol = ginitdcol
    dpag = ginitdpag
    cdirect = ginitcdirect
    crow = ginitcrow
    ccol =  ginitccol
    cpag =  ginitcpag
    idstack = -1
    icstack = -1
    gthis_col=1
    gdstackrow = 0
    gdstackcol = 0
    gdstackPag = 0
    glastcrow = crow
    glastccol = ccol
    glastcpag = cpag
}

function binstr( len, v, ret ) {
   ret=""
   while ( length( ret ) < len ) {
       ret = "" (v%2) ret
       v = int(v/2)
   }
   return( ret )
}
function writedatabit(row,col,pag,val,star) {
    if ( pag == 255 ) { #This isn't quite right, should be -1 somehow
        row=dstackrow[idstack+gdstackrow]
        pag=dstackpag[idstack+gdstackrow]
        if ( star ) {
            col = dstackcol[idstack+gdstackrow]
        } else {
            col += dstackcol[idstack+gdstackrow]
        }
    }
    adatabit[row,col,pag]=val
}
function offsetrow(val,star) {
    if ( drow == 255 ) { #This isn't quite right, should be -1 somehow
        gdstackrow += val
    } else {
        drow += val
    }
}
function offsetcol(val,star) {
    if ( drow == 255 ) { #This isn't quite right, should be -1 somehow
        if ( star ) {
            dstackcol[idstack+gdstackrow] += val
        } else {
            dcol += val
        }
    } else {
        dcol += val
    }
}
function undefinedvariable(row,col,pag,token) {
    aundefvarrow[token]=row
    aundefvarcol[token]=col
    aundefvarpag[token]=pag
}
function variablefixup(row,col,pag,token) {
    acode[row,col,pag]=bin2hex[avariable[token,0]]
    acode[row+1,col,pag]=bin2hex[avariable[token,1]]
    acode[row+2,col,pag]=bin2hex[avariable[token,2]]
    acode[row+3,col,pag]=bin2hex[avariable[token,3]]
    acode[row+4,col,pag]=bin2hex[avariable[token,4]]
    acode[row+5,col,pag]=bin2hex[avariable[token,5]]
}
function sectioninit(lline) {
    $0=lline
    if ( $2 == "DATAINIT" ) {
        ginitddirect = 0+$3
        ginitdrow = 0+$4
        ginitdcol = 0+$5
        ginitdpag = 0+$6
    } else if ( $2 == "CODEINIT" ) {
        ginitcdirect = 0+$3
        ginitcrow = 0+$4
        ginitccol = 0+$5
        ginitcpag = 0+$6
    } else if ( $2 == "CODESPACE" ) {
        gcwraprow=0
        gcwrapcol=0
        if ( $4 == "ROWCYLINDER" ) {
            gcwraprow=1
        } else if ( $4 == "CYLINDERCOL" ) {
            gcwrapcol=1
        } else if ( $4 == "KLEINCOL" ) {
            gcwraprow=1
            gcwrapcol=-1
        } else if ( $4 == "TORUS" ) {
            gcwraprow=1
            gcwrapcol=1
        }
    } else if ( $2 == "STACKSPACE" ) {
        stackmax = 0+$3
    } else if ( $2 == "STACKINIT" ) {
        ginitsdirect = 0+$3
        ginitsdrow = 0+$4
        ginitsdcol = 0+$5
        ginitsdpag = 0+$6
    }
}
function sectioncode(lline,i,len,token, nonspc) {
    if ( lline ~ /^x[0-9A-F][0-9A-F]*:[0-9A-F][0-9A-F]*:/ ) {
        token=substr( lline, 2 )
        sub( /^[^:][^:]*;/, "", lline )
        crow=strtohex(token)
        token=substr( lline, 1 )
        ccoloffset = strtohex(token)
        return
    }
    if ( lline ~ /^\$[^:][^:]*:/ ) {
        token = lline
        sub( /:.*/, "", token )
        len = length(token)+1
        sub( /^[^:][^:]*:/, "", lline )
        lline = sprintf( "%*.*s%s", len, len, "", lline )
        nonspc = lline
        sub( /^ [ ]*/, "", nonspc )
        len = index( lline, nonspc )
        ccol = int(len/INSTRSIZE)+ccoloffset
        avariable[token,0]= binstr(4,(crow%16))
        avariable[token,1]= binstr(4,int(crow/16))
        avariable[token,2]= binstr(4,(ccol%16))
        avariable[token,3]= binstr(4,int(ccol/16))
        avariable[token,4]= binstr(4,(cpag%16))
        avariable[token,5]= binstr(4,int(cpag/16))
    }
    if ( len = index( lline, "@" ) ) {
        token = substr( lline, len )
        gsub( /#/, " ", token )
        sub( / .*$/, "", token )
        if ( !( token in amacro ) ) {
            printf( "\nno macro named %s found.\n", token )
            exit(2)
        }
        for ( i = 1; i <= amacro[token]; i++ ) {
            sectioncode( sprintf( "%*.*s%s", len-1, len-1, "", amacro[token,i] ) )
        }
    } else if ( len = index( lline, "$" ) ) {
        token = substr( lline, len )
        gsub( /#/, " ", token )
        sub( / .*$/, "", token )
        ccol = int(len/INSTRSIZE)
        if ( (token,0) in avariable ) {
            variablefixup(crow,ccol,cpag,token)
        } else {
            undefinedvariable(crow,ccol,cpag,token)
        }
        crow++
    } else {
        gsub( /#/, " ", lline )
        len = length( lline )+ccoloffset
        ccol = ccoloffset #New? or Borken?
        for ( i = 1; i <= len; i += INSTRSIZE ) {
            token = substr( lline, i, INSTRSIZE )
            gsub( / /, "", token )
            if ( token != "" ) {
                acode[crow,ccol,cpag] = token
                initial_code[token]++
            }
            if ( codemaxcol < ccol ) {
               codemaxcol = ccol
            }
            ccol++
        }
        if ( codemaxrow < crow ) {
           codemaxrow = crow
        }
        crow++
    }
}
function sectiondata(lline,fsomethingdone,token,row,col,pag,state,i) {
    fsomethingdone = 0
    state = 0
    col = 0
    row = drow
    pag = dpag
    len = length( lline )
    for ( i = 0; i < len; i++ ) {
        token = substr( lline, i+1, 1 )
        if ( 0 == state ) {
            gsub( / /, "", token )
            if ( token != "" ) {
                if ( ( token == "r" ) || ( token == "u" ) ) {
                    fsomethingdone=1
                    writedatabit(row,col,pag,int(rand()*2))
                    col++
                } else if ( token == "\"" ) {
                    state=1
                } else if ( token == "$" ) {
                    token=substr( lline, i+1 )
                    sub( /:.*$/, "", token )
                    avariable[token,0]= binstr(4,(row%16))
                    avariable[token,1]= binstr(4,int(row/16))
                    avariable[token,2]= binstr(4,(col%16))
                    avariable[token,3]= binstr(4,int(col/16))
                    avariable[token,4]= binstr(4,(pag%16))
                    avariable[token,5]= binstr(4,int(pag/16))
                    while ( (i < len) && ( substr( lline, i, 1 ) != ":" ) ) {
                        i++
                    }
                } else if ( token == "x" ) {
                    token=substr( lline, i+1 )
                    row=strtohex(token)
                    sub( /^x[0-9A-F][0-9A-F]:/, "", token )
                    i += 4
                    if ( token ~ /^[0-9A-F][0-9A-F]:/ ) {
                        col=strtohex(token)
                        sub( /^[0-9A-F][0-9A-F]:/, "", token )
                        i += 3
                    }
                    if ( token ~ /^[0-9A-F][0-9A-F]:/ ) {
                        pag=strtohex(token)
                        sub( /^[0-9A-F][0-9A-F]:/, "", token )
                        i += 3
                    }
                    printf( "row=%2.2x col=%2.2x pag=%2.2x\n", row, col, pag )
                } else if ( token != "0" ) {
                    fsomethingdone=1
                    writedatabit(row,col,pag,1)
                    col++
                } else {
                    fsomethingdone=1
                    writedatabit(row,col,pag,0)
                    col++
                }
            }
        } else if ( 1 <= state ) {
            fsomethingdone=1
            if ( 2 == state ) {
                if ( token == "n" ) {
                    token="\n"
                } else if ( token == "t" ) {
                    token="\t"
                } else if ( token == "r" ) {
                    token="\r"
                }
                ascii(aascii[token],row,col,pag)
                col += 8;
                state=1
            } else if ( token == "\"" ) {
                ascii(0,row,col,pag)
                col += 8;
                state=0
            } else if ( token == "\\" ) {
                state=2
            } else {
                ascii(aascii[token],row,col,pag)
                col += 8;
            }
        }
        if ( fsomethingdone ) {
            dcol = col
            if ( datamaxcol < dcol ) {
                datamaxcol = dcol
            }
        }
    }
    if ( fsomethingdone ) {
        drow = row
        dcol = col
        dpag = pag
        if ( datamaxrow < drow ) {
           datamaxrow = drow
        }
        if ( datamaxpag < dpag ) {
           datamaxpag = dpag
        }
        drow++
    }
}
function sectionmacro(lline,maccol) {
    if ( lline != "" ) {
        if ( lline ~ /^@[^=][^=]*=/ ) {
            gmacro[maccol]=lline
            sub( /=.*/, "", gmacro[maccol] )
            gmacroline[maccol]=0
        } else if ( lline ~ /^$/ ) {
            gmacro[maccol]=""
        } else if ( gmacro[maccol] != "" ) {
            gmacroline[maccol]++
            amacro[gmacro[maccol]]=gmacroline[maccol]
            amacro[gmacro[maccol],gmacroline[maccol]]=lline
        }
    }
}
function strtohex(str,val,len,i,ch) {
    val=0
    len=length(str)
    for ( i = 1; i <= len; i++ ) {
        ch = substr(str,i,1)
        if ( ch in hexstr ) {
            val *= 16
            val += hexstr[ch]
        } else {
            return(val)
        }
    }
    return(val)
}
function handle01s(cinstr,star) {
    while ( cinstr ~ /^[01io]/ ) {
        if ( cinstr ~ /^i/ ) {
            writedatabit(drow,dcol,dpag,1,star)
        } else if ( cinstr ~ /^o/ ) {
            writedatabit(drow,dcol,dpag,0,star)
        } else {
            writedatabit(drow,dcol,dpag,0+substr(cinstr,1,1),star)
            if ( substr(cinstr,2,1) != "-" ) {
                offsetrow( sincos[ddirect] )
                offsetcol( sincos[ddirect+3] )
            } else {
                cinstr = substr(cinstr,2)
            }
        }
        cinstr = substr(cinstr,2)
    }
    return(cinstr)
}
