BEGIN {
    for ( i = 0; i < 256; i++ ) {
        aascii[sprintf("%c",i)]=i
    }

#include mne.awk
    hexstr="00112233445566778899AaBbCcDdEeFf"
    bin2hex["0000"]="HX0"
    bin2hex["0001"]="HX1"
    bin2hex["0010"]="HX2"
    bin2hex["0011"]="HX3"
    bin2hex["0100"]="HX4"
    bin2hex["0101"]="HX5"
    bin2hex["0110"]="HX6"
    bin2hex["0111"]="HX7"
    bin2hex["1000"]="HX8"
    bin2hex["1001"]="HX9"
    bin2hex["1010"]="HXA"
    bin2hex["1011"]="HXB"
    bin2hex["1100"]="HXC"
    bin2hex["1101"]="HXD"
    bin2hex["1110"]="HXE"
    bin2hex["1111"]="HXF"

    INSTRSIZE=6
    DOWN_DIRECTION=0
    RIGHT_DIRECTION=1
    UP=2
    LEFT=3
    dirtext[0]="down"
    dirtext[1]="right"
    dirtext[2]="up"
    dirtext[3]="left"
    dirtext[4]="fore"
    dirtext[5]="back"
    gtextx=0
    gtexty=0
    drow = 0
    dpag = 0
    crow = 0
    cpag = 0
    ccoloffset = 0
    readmemh("/home/notsystem/nanocpu/adder/stack-init.in", code_stack_row)
    readmemh("/home/notsystem/nanocpu/adder/stack-init.in", code_stack_col)
    readmemh("/home/notsystem/nanocpu/adder/stack-init.in", code_stack_pag)
    readmemh("/home/notsystem/nanocpu/adder/stack-init.in", data_stack_row)
    readmemh("/home/notsystem/nanocpu/adder/stack-init.in", data_stack_col)
    readmemh("/home/notsystem/nanocpu/adder/stack-init.in", data_stack_pag)
    readmemh("/home/notsystem/nanocpu/adder/memory-data.in", memory_data)
    readmemh("/home/notsystem/nanocpu/adder/memory-code.in", memory_code)
    
    code_row = 255
    code_col = 0
    code_pag = 0
    code_read_en = 0
    data_read_en = 0
    data_bit_set_en = 0
    data_bit_clear_en = 0
    ind_data_pag = 0
    ind_data_row = 0
    idata_pag = 0
    data_row = 0
    data_col = 0
    data_pag = 0
    code_sp = 255
    data_sp = 255
    data_write_en = 0
    code_write_en = 0
    umathregister = 0
    register_dw = 0
    register_a = 0
    register_b = 0
    data_byte = 0
    data_push_en = 0
    data_swap_en = 0
    code_pop_en = 0
    code_swap_en = 0
    ustep = 7
    inibble = 0
    DOWN_DIRECTION= 0
    RIGHT_DIRECTION= 1
    UP_DIRECTION=2
    LEFT_DIRECTION=3
    FORE_DIRECTION=4
    BACK_DIRECTION=5

    data_direction = RIGHT_DIRECTION
    code_direction = DOWN_DIRECTION
     CPU_NOP = 0x00

     CPU_POD = 0x01
     CPU_PUD = 0x02
     CPU_TOD = 0x03
     CPU_QOD = 0x04
     CPU_XD  = 0x05
     CPU_XDI = 0x06
     CPU_LDI = 0x07
     CPU_SKP = 0x08

     CPU_POC = 0x09
     CPU_PUC = 0x0A
     CPU_TOC = 0x0B
     CPU_QOC = 0x0C
     CPU_XC  = 0x0D
     CPU_XCI = 0x0E
     CPU_LCI = 0x0F

     CPU_DD = 0x10
     CPU_DR = 0x11
     CPU_DU = 0x12
     CPU_DL = 0x13
     CPU_JDD = 0x14
     CPU_JDR = 0x15
     CPU_JDU = 0x16
     CPU_JDL = 0x17
     CPU_CD = 0x18
     CPU_CR = 0x19
     CPU_CU = 0x1A
     CPU_CL = 0x1B
     CPU_JCD = 0x1C
     CPU_JCR = 0x1D
     CPU_JCU = 0x1E
     CPU_JCL = 0x1F
     CPU_HX0 = 0x20
     CPU_HX1 = 0x21
     CPU_HX2 = 0x22
     CPU_HX3 = 0x23
     CPU_HX4 = 0x24
     CPU_HX5 = 0x25
     CPU_HX6 = 0x26
     CPU_HX7 = 0x27
     CPU_HX8 = 0x28
     CPU_HX9 = 0x29
     CPU_HXA = 0x2A
     CPU_HXB = 0x2B
     CPU_HXC = 0x2C
     CPU_HXD = 0x2D
     CPU_HXE = 0x2E
     CPU_HXF = 0x2F

     CPU_N1D  = 0x30 #-
     CPU_N3D  = 0x31 #---
     CPU_N8D  = 0x32 #b-
     CPU_N32D = 0x33 #d-
     CPU_P1D  = 0x34 #+
     CPU_P3D  = 0x35 #+++
     CPU_P8D  = 0x36 #b+
     CPU_P32D = 0x37 #d+
     CPU_siN1  = 0x38 #*-
     CPU_siN3  = 0x39 #*---
     CPU_siN8  = 0x3A #*b-
     CPU_siN32 = 0x3B #*d-

     CPU_siP1  = 0x3C #*+
     CPU_siP3  = 0x3D #*+++
     CPU_siP8  = 0x3E #*b+
     CPU_siP32 = 0x3F #*d+
     CPU_ZP   = 0x40 #0
     CPU_1P   = 0x41 #1
     CPU_ZNP  = 0x42 #o
     CPU_1NP  = 0x43 #i
     CPU_siZP  = 0x44 #*0
     CPU_si1P  = 0x45 #*1
     CPU_siZNP = 0x46 #*o
     CPU_si1NP = 0x47 #*i
     CPU_PZ   = 0x48 #+o
     CPU_P1   = 0x49 #+i
     CPU_siPZNP  = 0x4A #*+o
     CPU_siP1NP  = 0x4B #*+i
#               = 0x4C
#               = 0x4D
     CPU_PDI  = 0x4E
     CPU_PCI  = 0x4F
     CPU_ADD  = 0x50
     CPU_ADC  = 0x51
     CPU_SUB  = 0x52
     CPU_SBC  = 0x53
     CPU_MUL  = 0x54
#               = 0x55
#               = 0x56
#               = 0x57
     CPU_LDA  = 0x58
     CPU_LDB  = 0x59
     CPU_WRA  = 0x5A
     CPU_WRB  = 0x5B
     CPU_LIA  = 0x5C
     CPU_LIB  = 0x5D
#           = 0x5E
#           = 0x5F
     CPU_DF   = 0x60 #This is enough to get started in 3D
     CPU_DB   = 0x61
     CPU_JDF  = 0x62
     CPU_JDB  = 0x63
     CPU_CF   = 0x64
     CPU_CB   = 0x65
     CPU_JCF  = 0x66
     CPU_JCB  = 0x67
#           = 0x61
#           = 0x62
#           = 0x63
#           = 0x64
#           = 0x65
#           = 0x66
#           = 0x67
#           = 0x68
#           = 0x69
#           = 0x6A
#           = 0x6B
#           = 0x6C
#           = 0x6D
#           = 0x6E
#           = 0x6F
     CPU_PUSW = 0x70
     CPU_POSW = 0x71
     CPU_POC2D = 0x72
     CPU_POD2C = 0x73
     CPU_DNM  = 0x74
     CPU_DIM  = 0x75
     CPU_DSM  = 0x76
#           = 0x77
#           = 0x78
     CPU_HLT = 0x79
     CPU_WZ  = 0x7A
     CPU_W1  = 0x7B
     CPU_iWZ = 0x7C
     CPU_iW1 = 0x7D
#           = 0x7E
#           = 0x7F
#           = 0x80
#           = 0x81
#           = 0x82
#           = 0x83
#           = 0x84
#           = 0x85
#           = 0x86
#           = 0x87
#           = 0x88
#           = 0x89
#           = 0x8A
#           = 0x8B
#           = 0x8C
#           = 0x8D
#           = 0x8E
#           = 0x8F
     CPU_0DD  = 0x90 #!
     CPU_0DR  = 0x91 #!
     CPU_0DU  = 0x92 #!
     CPU_0DL  = 0x93 #!
     CPU_0JDD = 0x94 #!
     CPU_0JDR = 0x95 #!
     CPU_0JDU = 0x96 #!
     CPU_0JDL = 0x97 #!
     CPU_0CD  = 0x98 #!
     CPU_0CR  = 0x99 #!
     CPU_0CU  = 0x9A #!
     CPU_0CL  = 0x9B #!
     CPU_0JCD = 0x9C #!
     CPU_0JCR = 0x9D #!
     CPU_0JCU = 0x9E #!
     CPU_0JCL = 0x9F #!
#           = 0xA0
#           = 0xA1
#           = 0xA2
#           = 0xA3
#           = 0xA4
#           = 0xA5
#           = 0xA6
#           = 0xA7
#           = 0xA8
#           = 0xA9
#           = 0xAA
#           = 0xAB
#           = 0xAC
#           = 0xAD
#           = 0xAE
#           = 0xAF
     CPU_1DD   = 0xB0 #?
     CPU_1DR   = 0xB1 #?
     CPU_1DU   = 0xB2 #?
     CPU_1DL   = 0xB3 #?
     CPU_1JDD  = 0xB4 #?
     CPU_1JDR  = 0xB5 #?
     CPU_1JDU  = 0xB6 #?
     CPU_1JDL  = 0xB7 #?
     CPU_1CD   = 0xB8 #?
     CPU_1CR   = 0xB9 #?
     CPU_1CU   = 0xBA #?
     CPU_1CL   = 0xBB #?
     CPU_1JCD  = 0xBC #?
     CPU_1JCR  = 0xBD #?
     CPU_1JCU  = 0xBE #?
     CPU_1JCL  = 0xBF #?
#           = 0xC0
#           = 0xC1
#           = 0xC2
#           = 0xC3
#           = 0xC4
#           = 0xC5
#           = 0xC6
#           = 0xC7
#           = 0xC8
#           = 0xC9
#           = 0xCA
#           = 0xCB
#           = 0xCC
#           = 0xCD
#           = 0xCE
#           = 0xCF
     CPU_i0DD  = 0xD0 #*!
     CPU_i0DR  = 0xD1 #*!
     CPU_i0DU  = 0xD2 #*!
     CPU_i0DL  = 0xD3 #*!
     CPU_i0JDD = 0xD4 #*!
     CPU_i0JDR = 0xD5 #*!
     CPU_i0JDU = 0xD6 #*!
     CPU_i0JDL = 0xD7 #*!
     CPU_i0CD  = 0xD8 #*!
     CPU_i0CR  = 0xD9 #*!
     CPU_i0CU  = 0xDA #*!
     CPU_i0CL  = 0xDB #*!
     CPU_i0JCD = 0xDC #*!
     CPU_i0JCR = 0xDD #*!
     CPU_i0JCU = 0xDE #*!
     CPU_i0JCL = 0xDF #*!
#           = 0xE0
#           = 0xE1
#           = 0xE2
#           = 0xE3
#           = 0xE4
#           = 0xE5
#           = 0xE6
#           = 0xE7
#           = 0xE8
#           = 0xE9
#           = 0xEA
#           = 0xEB
#           = 0xEC
#           = 0xED
#           = 0xEE
#           = 0xEF

     CPU_i1DD  = 0xF0 #*!
     CPU_i1DR  = 0xF1 #*!
     CPU_i1DU  = 0xF2 #*!
     CPU_i1DL  = 0xF3 #*!
     CPU_i1JDD = 0xF4 #*!
     CPU_i1JDR = 0xF5 #*!
     CPU_i1JDU = 0xF6 #*!
     CPU_i1JDL = 0xF7 #*!
     CPU_i1CD  = 0xF8 #*?
     CPU_i1CR  = 0xF9 #*?
     CPU_i1CU  = 0xFA #*?
     CPU_i1CL  = 0xFB #*?
     CPU_i1JCD = 0xFC #*?
     CPU_i1JCR = 0xFD #*?
     CPU_i1JCU = 0xFE #*?
     CPU_i1JCL = 0xFF #*?

    instruction = CPU_NOP
    data_ptr_mode = DIRECT_VAL
    DIRECT_VAL=0
    INDIRECT_VAL=1
    STAR_VAL=2
    for ( clk=0; clk <= int(190000*4/42); clk++ ) {
        if ( data_bit_set_en ) {
            if ( INDIRECT_MODE() ) {
                data_byte = or(data_byte,(2**idata_col_bits))
            } else {
                data_byte = or(data_byte,(2**data_col_bits))
            }
        } else if ( data_bit_clear_en ) {
            if ( INDIRECT_MODE() ) {
                data_byte = and(data_byte,and(255,(xor(255,(2**idata_col_bits)))))
            } else {
                data_byte = and(data_byte,and(255,(xor(255,(2**data_col_bits)))))
            }
        }
        code_address = or(and(63,code_row)*128,and(63,code_col))
        if ( code_read_en && POSEDGE(clk) ) {
            code_read_byte = memory_code[code_address]
        }
        if ( ustep == 0 ) {
            instruction = code_read_byte
        }
        data_col_bits = (7-and(7,data_col))
        idsp_addr = and(255,(data_sp+ind_data_row))
        idata_col_bits = (7-and(7,idata_col))
        idata_col = and(255,(data_col+data_stack_col[idsp_addr]))
        idata_row = data_stack_row[idsp_addr]
        idata_pag = data_stack_pag[idsp_addr]
        if ( INDIRECT_MODE() ) {
            data_address = or((and(1,idata_pag)*16384),or((idata_row*32),int(idata_col/8)))
        } else {
            data_address = or((and(1,data_pag)*16384),or((data_row*32),int(data_col/8)))
        }
        if ( data_read_en && POSEDGE(clk) ) {
            data_read_byte = memory_data[data_address]
            data_byte = data_read_byte
        }
        if ( data_write_en && POSEDGE(clk) ) {
            memory_data[data_address] = data_byte
        }

        if ( POSEDGE(clk) ) {
            if ( ustep == 0 ) {
                printf( "%4.4x:%6.6s %2.2x{%2.2x} {%4.4x} [%2.2x:%2.2x:%2.2x.%2.2x]i[%2.2x:%2.2x:%2.2x.%2.2x]\n", code_address, mne[instruction], data_byte, data_read_byte, data_address, data_row, data_col, data_pag, data_col_bits, idata_row, idata_col, idata_pag, idata_col_bits)
            }
            csae=1
            if ( csae ) { if ( instruction == CPU_POD ) {
                Fn_POD()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_PUC ) {
                Fn_PUC()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_POC ) {
                Fn_POC()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_TOC ) {
                Fn_TOC()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_QOC ) {
                Fn_QOC()
                csae=0
            } }
if ( csae ) { if ( ( instruction == CPU_HX0 ) || ( instruction == CPU_HX1 ) || ( instruction == CPU_HX2 ) || ( instruction == CPU_HX3 ) || ( instruction == CPU_HX4 ) || ( instruction == CPU_HX5 ) || ( instruction == CPU_HX6 ) || ( instruction == CPU_HX7 ) || ( instruction == CPU_HX8 ) || ( instruction == CPU_HX9 ) || ( instruction == CPU_HXA ) || ( instruction == CPU_HXB ) || ( instruction == CPU_HXC ) || ( instruction == CPU_HXD ) || ( instruction == CPU_HXE ) || ( instruction == CPU_HXF ) ) {
                Fn_HX_()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_LCI ) {
                Fn_LCI()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_DF ) {
                Fn_DF()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_DB ) {
                Fn_DB()
                csae=0
            } }
if ( csae ) { if ( ( instruction == CPU_DU ) || ( instruction == CPU_DD ) || ( instruction == CPU_DL ) || ( instruction == CPU_DR ) ) {
                Fn_DU()
                csae=0
            } }
if ( csae ) { if ( ( instruction == CPU_JDL ) || ( instruction == CPU_JDR ) || ( instruction == CPU_JDU ) || ( instruction == CPU_JDD ) ) {
                Fn_JDL()
                csae=0
            } }
if ( csae ) { if ( ( instruction == CPU_0JDL ) || ( instruction == CPU_0JDR ) || ( instruction == CPU_0JDU ) || ( instruction == CPU_0JDD ) || ( instruction == CPU_1JDL ) || ( instruction == CPU_1JDR ) || ( instruction == CPU_1JDU ) || ( instruction == CPU_1JDD ) ) {
                Fn_0JDL()
                csae=0
            } }
if ( csae ) { if ( ( instruction == CPU_CD ) || ( instruction == CPU_CR ) || ( instruction == CPU_CU ) || ( instruction == CPU_CL ) ) {
                Fn_CD()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_CF ) {
                Fn_CF()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_CB ) {
                Fn_CB()
                csae=0
            } }
if ( csae ) { if ( ( instruction == CPU_JCL ) || ( instruction == CPU_JCR ) || ( instruction == CPU_JCU ) || ( instruction == CPU_JCD ) ) {
                Fn_JCL()
                csae=0
            } }
if ( csae ) { if ( ( instruction == CPU_0JCL ) || ( instruction == CPU_0JCR ) || ( instruction == CPU_0JCU ) || ( instruction == CPU_0JCD ) || ( instruction == CPU_1JCL ) || ( instruction == CPU_1JCR ) || ( instruction == CPU_1JCU ) || ( instruction == CPU_1JCD ) ) {
                Fn_0JCL()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_P1D ) {
                Fn_P1D()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_N1D ) {
                Fn_N1D()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_P3D ) {
                Fn_P3D()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_N3D ) {
                Fn_N3D()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_PDI ) {
                Fn_PDI()
                csae=0
            } }
if ( csae ) { if ( ( instruction == CPU_0CU ) || ( instruction == CPU_0CD ) || ( instruction == CPU_0CL ) || ( instruction == CPU_0CR ) || ( instruction == CPU_1CU ) || ( instruction == CPU_1CD ) || ( instruction == CPU_1CL ) || ( instruction == CPU_1CR ) ) {
                Fn_1CU()
                csae=0
            } }
if ( csae ) { if ( ( instruction == CPU_P1 ) || ( instruction == CPU_PZ ) ) {
                Fn_P1()
                csae=0
            } }
if ( csae ) { if ( ( instruction == CPU_1NP ) || ( instruction == CPU_ZNP ) ) {
                Fn_1NP()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_DNM ) {
                Fn_DNM()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_DIM ) {
                Fn_DIM()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_DSM ) {
                Fn_DSM()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_PUSW ) {
                Fn_PUSW()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_HLT ) {
                Fn_HLT()
                csae=0
            } }
if ( csae ) { if ( instruction == CPU_NOP ) {
                Fn_NOP()
                csae=0
            } }
            #Fn_POSW()
            #Fn_POD2C()
            #Fn_POC2D()
            }
        }
    writememh("/home/notsystem/nanocpu/adder/memory-data.out", memory_data, 0, 16384)
}

function writememh( fn, pv, start, stop, i, len ) {
    len = (stop-start)
    printf( "" )>fn
    close(fn)
    for ( i = 0; i < len; i++ ) {
        if ( (i%16) == 0 ) {
            printf( "// 0x%8.8x\n", i ) >>fn
            close(fn)
        }
        printf( "%2.2x\n", pv[i+start] ) >>fn
        close(fn)
    }
}

function readmemh( fn, pv, oi, i, digit, val, len, lbuffer, ix ) {
    oi = 0
    while ( (getline < fn) > 0 ) {
        lbuffer=$0
        len=length(lbuffer)
        digit=0
        for ( i = 1; i <= len; i++ ) {
            if ( ix = index(hexstr,substr(lbuffer,i,1)) ) {
                ix = int((ix-1)/2)
                if ( digit == 0 ) {
                    val = ix
                    digit++
                } else {
                    val *= 16
                    val += ix
                    pv[oi]=val
                    oi++
                    val = 0
                    digit=0
                }
            }
        }
    }
    close(fn)
}

function Fn_NOP(csae) {
    csae=1
    if ( csae ) { if ( ustep == 0 ) {
        GO_CODE_DIRECTION(1) 
        code_read_en = 1
        ustep = 1
        csae = 0
    } }
    if ( csae ) { if ( ustep == 7 ) {
        GO_CODE_DIRECTION(1) 
        code_read_en = 1
        data_read_en = 1
        ustep = 6
        csae = 0
    } }
    if ( csae ) { if ( ustep == 5 ) {
        ustep = 0
        csae = 0
    } }
    if ( csae ) { if ( ustep == 1 ) {
        code_read_en = 0
        data_read_en = 0
        ustep = 0
        csae = 0
    } }
    if ( csae ) { if ( ustep == 6 ) {
        code_read_en = 0
        data_read_en = 0
        ustep = 5
        csae = 0
    } }
}
function Fn_HX_() {
    if ( ustep == 0 ) {
        GO_CODE_DIRECTION(1)
        nibble[inibble] = and(15,instruction)
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        inibble = inibble+1
        ustep = 0
    }
}
function Fn_PUSW() {
    if ( ustep == 0 ) { 
        data_sp = and(255,data_sp+1)
        ustep = 1
    } else if ( ustep == 1 ) { 
        data_stack_col[data_sp] = data_ptr_mode*64+code_direction*8+data_direction
        data_sp = and(255,data_sp+1)
        ustep = 2
    } else if ( ustep == 2 ) { 
        data_stack_col[data_sp] = nibble[1]*16+nibble[0]
        data_stack_row[data_sp] = nibble[3]*16+nibble[2]
        data_stack_pag[data_sp] = nibble[5]*16+nibble[4]
        ustep = 3
    } else if ( ustep == 3 ) { 
        data_stack_col[data_sp] = (register_dw%256)
        data_stack_row[data_sp] = ((register_dw/256)%256)
        data_stack_pag[data_sp] = ((register_dw/65536)%256)
        data_sp = and(255,data_sp+1)
        ustep = 4
    } else if ( ustep == 4 ) { 
        data_stack_col[data_sp] = ((register_dw/(256*65536))%256)
        data_stack_row[data_sp] = (register_a%256)
        data_stack_pag[data_sp] = ((register_a/256)%256)
        data_sp = and(255,data_sp+1)
        ustep = 5
    } else if ( ustep == 5 ) { 
        data_stack_col[data_sp] = (register_b%256)
        data_stack_row[data_sp] = ((register_b/256)%256)
        data_stack_pag[data_sp] = 0
        GO_CODE_DIRECTION(1)
        code_read_en = 1
        ustep = 6
    } else if ( ustep == 6 ) { 
        code_read_en = 0
        ustep = 0
    } 
}
function Fn_PUC() {
    if ( ustep == 0 ) {
        code_sp = and(255,code_sp+1)
        ustep = 1
    } else if ( ustep == 1 ) {
        code_stack_col[code_sp] = code_col
        code_stack_row[code_sp] = code_row
        code_stack_pag[code_sp] = code_pag
        GO_CODE_DIRECTION(1)
        code_read_en = 1
        ustep = 2
    } else if ( ustep == 2 ) {
        code_read_en = 0
        ustep = 0
    }
}
function Fn_PDI() {
    if ( ustep == 0 ) {
        data_sp = and(255,data_sp+1)
        ustep = 1
    } else if ( ustep == 1 ) {
        data_stack_row[data_sp]=(16*nibble[1]+nibble[0])
        data_stack_col[data_sp]=(16*nibble[3]+nibble[2])
        data_stack_pag[data_sp]=(16*nibble[5]+nibble[4])
        GO_CODE_DIRECTION(1)
        code_read_en = 1
        inibble = 0
        ustep = 2
    } else if ( ustep == 2 ) {
        code_read_en = 0
        ustep = 0
    }
}

function Fn_LCI() {
    if ( ustep == 0 ) {
        code_row=(16*nibble[1]+nibble[0])
        code_col=(16*nibble[3]+nibble[2])
        code_pag=(16*nibble[5]+nibble[4])
        inibble = 0
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 0
    }
}

function Fn_POD() {
    if ( ustep == 0 ) {
        data_row = data_stack_row[data_sp]
        data_col = data_stack_col[data_sp]
        data_pag = data_stack_pag[data_sp]
        GO_CODE_DIRECTION(1)
        code_read_en = 1
        data_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        data_sp = data_sp - 1
        code_read_en = 0
        data_read_en = 0
        ustep = 0
    }
}
function Fn_DNM() {
    if ( ustep == 0 ) {
        data_ptr_mode = DIRECT_VAL
        GO_CODE_DIRECTION(1)
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 0
    }
}
function Fn_DIM() {
    if ( ustep == 0 ) {
        data_ptr_mode = INDIRECT_VAL
        GO_CODE_DIRECTION(1)
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 0
    }
}
function Fn_DSM() {
    if ( ustep == 0 ) {
        data_ptr_mode = STAR_VAL
        GO_CODE_DIRECTION(1)
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 0
    }
}
function Fn_CF() {
    if ( ustep == 0 ) {
        code_direction = FORE_DIRECTION
        code_pag = and(255,code_pag+1)
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 0
    }
}
function Fn_CB() {
    if ( ustep == 0 ) {
        code_direction = BACK_DIRECTION
        code_pag = and(255,code_pag-1+256)
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 0
    }
}
function Fn_CD() {
    if ( ustep == 0 ) {
        code_direction = and(3,instruction)
        if ( and(3,instruction) == DOWN_DIRECTION ) {
            code_row = and(255,code_row+1)
        } else if ( and(3,instruction) == UP_DIRECTION ) {
            code_row = and(255,code_row-1+256)
        } else if ( and(3,instruction) == RIGHT_DIRECTION ) {
            code_col = and(255,code_col+1)
        } else if ( and(3,instruction) == LEFT_DIRECTION ) {
            code_col = and(255,code_col-1+256)
        }
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 0
    }
}
# -
function Fn_N1D() {
    if ( ustep == 0 ) {
        GO_OPPOSITE_DATA_DIRECTION(1) 
        GO_CODE_DIRECTION(1)
        data_read_en = 1
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        data_read_en = 0
        code_read_en = 0
        ustep = 0
    }
}
# i o
function Fn_1NP() {
    if ( ustep == 0 ) {
        GO_CODE_DIRECTION(1)
        code_read_en = 1
        if ( and(1,instruction) ) {
            data_bit_set_en = 1
        } else {
            data_bit_clear_en = 1
        }
        data_write_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        data_bit_set_en = 0
        data_bit_clear_en = 0
        data_write_en = 0
        code_read_en = 0
        ustep = 0
    }
}
# +
function Fn_P1D() {
    if ( ustep == 0 ) {
        GO_DATA_DIRECTION(1) 
        GO_CODE_DIRECTION(1)
        data_read_en = 1
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        data_read_en = 0
        code_read_en = 0
        ustep = 0
    }
}
function Fn_N3D() {
    if ( ustep == 0 ) {
        GO_OPPOSITE_DATA_DIRECTION(3) 
        GO_CODE_DIRECTION(1)
        data_read_en = 1
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        data_read_en = 0
        code_read_en = 0
        ustep = 0
    }
}
function Fn_P3D() {
    if ( ustep == 0 ) {
        GO_DATA_DIRECTION(3) 
        GO_CODE_DIRECTION(1)
        data_read_en = 1
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        data_read_en = 0
        code_read_en = 0
        ustep = 0
    }
}
function Fn_1CU() {
    if ( ustep == 0 ) {
        if ( data_pag == 0xFF ) {
            if ( and(int(data_byte/(2**idata_col_bits)),1) == and(1,int(instruction/32)) ) {
                code_direction=and(3,instruction)
                GO_CODE_THIS_DIRECTION(and(3,instruction),1) 
            } else {
               GO_CODE_DIRECTION(1)
            }
        } else {
            if ( and(int(data_byte/(2**data_col_bits)),1) == and(1,int(instruction/32)) ) {
                code_direction=and(3,instruction)
                GO_CODE_THIS_DIRECTION(and(3,instruction),1) 
            } else {
                GO_CODE_DIRECTION(1)
            }
        }
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 0
    }
}
function Fn_HLT() {
    if ( ustep == 0 ) {
        GO_OPPOSITE_CODE_DIRECTION(1) 
        ustep = 7
    }
}
function Fn_DF() {
    if ( ustep == 0 ) {
        GO_CODE_DIRECTION(1) 
        data_direction=FORE_DIRECTION
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 0
    }
}
function Fn_DB() {
    if ( ustep == 0 ) {
        GO_CODE_DIRECTION(1) 
        data_direction=BACK_DIRECTION
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 0
    }
}
function Fn_DU() {
    if ( ustep == 0 ) {
        GO_CODE_DIRECTION(1) 
        data_direction=and(3,instruction)
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 0
    }
}
function Fn_JDL() {
    if ( ustep == 0 ) {
        if ( and(3,instruction) == DOWN_DIRECTION ) {
            HANDLE_DATA_ROW_INDIRECT_P(1)
        } else if ( and(3,instruction) == RIGHT_DIRECTION ) {
            HANDLE_DATA_COL_INDIRECT_P(1)
        } else if ( and(3,instruction) == UP_DIRECTION ) {
            HANDLE_DATA_ROW_INDIRECT_N(1)
        } else if ( and(3,instruction) == LEFT_DIRECTION ) {
            HANDLE_DATA_COL_INDIRECT_N(1)
        }
        GO_CODE_DIRECTION(1)
        code_read_en = 1
        data_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        data_read_en = 0
        ustep = 0
    }
}
function Fn_0JDL() {
    if ( ustep == 0 ) {
        if ( INDIRECT_MODE() ) {
            # Both use data_byte snapshot, but might differ in [i]data_col_bits
            if ( and(int(data_byte/(2**idata_col_bits)),1) == and(1,int(instruction/32)) ) {
                GO_DATA_THIS_DIRECTION(and(3,instruction),1) 
                data_read_en = 1
            }
        } else {
            if ( and(int(data_byte/(2**data_col_bits)),1) == and(1,int(instruction/32)) ) {
                GO_DATA_THIS_DIRECTION(and(3,instruction),1) 
                data_read_en = 1
            }
        }
        GO_CODE_DIRECTION(1)
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        data_read_en = 0
        ustep = 0
    }
}
function Fn_JCL() {
    if ( ustep == 0 ) {
        GO_CODE_THIS_DIRECTION_AND_DIRECTION(and(3,instruction)) 
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 0
    }
}
function Fn_0JCL() {
    if ( ustep == 0 ) {
        if ( INDIRECT_MODE() ) {
            if ( and(int(data_byte/(2**idata_col_bits)),1) == and(1,int(instruction/32)) ) {
                GO_CODE_THIS_DIRECTION_AND_DIRECTION(and(3,instruction)) 
            } else {
                GO_CODE_DIRECTION(1)
            }
        } else {
            if ( and(int(data_byte/(2**data_col_bits)),1) == and(1,int(instruction/32)) ) {
                GO_CODE_THIS_DIRECTION_AND_DIRECTION(and(3,instruction)) 
            } else {
                GO_CODE_DIRECTION(1)
            }
        }
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 0
    }
}
// +i +o
function Fn_P1() {
    if ( ustep == 0 ) {
        GO_DATA_DIRECTION(1)
        data_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        data_read_en = 0
        ustep = 2
    } else if ( ustep == 2 ) {
        GO_CODE_DIRECTION(1)
        if ( and(1,instruction) ) {
            data_bit_set_en = 1
        } else {
            data_bit_clear_en = 1
        }
        code_read_en = 1
        data_write_en = 1
        ustep = 3
    } else if ( ustep == 3 ) {
        code_read_en = 0
        data_write_en = 0
        data_bit_set_en = 0
        data_bit_clear_en = 0
        ustep = 0
    }
}
function Fn_QOC() {
    if ( ustep == 0 ) {
        GO_CODE_DIRECTION(1)
        code_sp = code_sp - 1
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 1
    }
}
function Fn_POC() {
    if ( ustep == 0 ) {
        GO_CODE_DIRECTION_FROM_STACK(1)
        code_sp = code_sp - 1
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 0
    }
}
function Fn_TOC() {
    if ( ustep == 0 ) {
        GO_CODE_DIRECTION_FROM_STACK(1)
        code_read_en = 1
        ustep = 1
    } else if ( ustep == 1 ) {
        code_read_en = 0
        ustep = 0
    }
}

function INDIRECT_MODE() {
    return( data_ptr_mode == INDIRECT_VAL )
}
function HANDLE_DATA_PAG_INDIRECT_P(x) {
    if ( INDIRECT_MODE() ){
        ind_data_pag = and(255,ind_data_pag+x)
    } else{
        data_pag = and(255,data_pag+x)
    }
}
function HANDLE_DATA_PAG_INDIRECT_N(x) {
    if ( INDIRECT_MODE() ){
        ind_data_pag = and(255,ind_data_pag-x+256)
    } else{
        data_pag = and(255,data_pag-x+256)
    }
}
function HANDLE_DATA_ROW_INDIRECT_P(x) {
    if ( INDIRECT_MODE() ){
        ind_data_row = and(255,ind_data_row+x)
    } else{
        data_row = and(255,data_row+x)
    }
}
function HANDLE_DATA_ROW_INDIRECT_N(x) {
    if ( INDIRECT_MODE() ){
        ind_data_row = and(255,ind_data_row-x+256)
    } else{
        data_row = and(255,data_row-x+256)
    }
}
function HANDLE_DATA_COL_INDIRECT_P(x) {
    if ( INDIRECT_MODE() ){
        data_col = and(255,data_col+x)
    } else{
        data_col = and(255,data_col+x)
    }
}
function HANDLE_DATA_COL_INDIRECT_N(x) {
    if ( INDIRECT_MODE() ){
        data_col = and(255,data_col-x+256)
    } else{
        data_col = and(255,data_col-x+256)
    }
}
function JUMP_CODE_DOWN_AND() {
        if ( code_direction == DOWN_DIRECTION ){
            code_row = and(255,code_row+2)
        } else if ( code_direction == RIGHT_DIRECTION ) {
            code_row = and(255,code_row+1)
            code_col = and(255,code_col+1)
        } else if ( code_direction == LEFT_DIRECTION ) {
            code_row = and(255,code_row+1)
            code_col = and(255,code_col-1+256)
        }
}
function JUMP_CODE_RIGHT_AND() {
        if ( code_direction == DOWN_DIRECTION ){
            code_col = and(255,code_col+1)
            code_row = and(255,code_row+1)
        } else if ( code_direction == UP_DIRECTION ) {
            code_col = and(255,code_col+1)
            code_row = and(255,code_row-1+256)
        } else if ( code_direction == RIGHT_DIRECTION ) {
            code_col = and(255,code_col+2)
        }
}
function JUMP_CODE_UP_AND() {
        if ( code_direction == UP_DIRECTION ){
            code_row = and(255,code_row-2+256)
        } else if ( code_direction == RIGHT_DIRECTION ){
            code_row = and(255,code_row-1+256)
            code_col = and(255,code_col+1)
        } else if ( code_direction == LEFT_DIRECTION ){
            code_row = and(255,code_row-1+256)
            code_col = and(255,code_col-1+256)
        }
}
function JUMP_CODE_LEFT_AND() {
        if ( code_direction == DOWN_DIRECTION ){
            code_col = and(255,code_col-1+256)
            code_row = and(255,code_row+1)
        } else if ( code_direction == UP_DIRECTION ){
            code_row = and(255,code_row-1+256)
        } else if ( code_direction == LEFT_DIRECTION ){
            code_col = and(255,code_col-2+256)
        }
}
function GO_OPPOSITE_CODE_DIRECTION(x) {
    if ( code_direction == DOWN_DIRECTION ){
        code_row = and(255,code_row+x)
    } else if ( code_direction == UP_DIRECTION ){
        code_row = and(255,code_row-x+256)
    } else if ( code_direction == RIGHT_DIRECTION ){
        code_col = and(255,code_col+x)
    } else if ( code_direction == LEFT_DIRECTION ){
        code_col = and(255,code_col-x+256)
    }
}
function GO_CODE_THIS_DIRECTION(d,x) {
    if ( d == DOWN_DIRECTION ){
        code_row = and(255,code_row+x)
    } else if ( d == UP_DIRECTION ){
        code_row = and(255,code_row-x+256)
    } else if ( d == RIGHT_DIRECTION ){
        code_col = and(255,code_col+x)
    } else if ( d == LEFT_DIRECTION ){
        code_col = and(255,code_col-x+256)
    }
}
function GO_CODE_THIS_DIRECTION_AND_DIRECTION(d) {
    if ( d == DOWN_DIRECTION ){
        if ( code_direction == DOWN_DIRECTION ){
            code_row = and(255,code_row+2)
        } else if ( code_direction == UP_DIRECTION ){
        } else if ( code_direction == RIGHT_DIRECTION ){
            code_row = and(255,code_row+1)
            code_col = and(255,code_col+1)
        } else if ( code_direction == LEFT_DIRECTION ){
            code_row = and(255,code_row+1)
            code_col = and(255,code_col-1)
        }
    } else if ( d == UP_DIRECTION ){
        if ( code_direction == DOWN_DIRECTION ){
        } else if ( code_direction == UP_DIRECTION ){
            code_row = and(255,code_row-2+256)
        } else if ( code_direction == RIGHT_DIRECTION ){
            code_row = and(255,code_row-1+256)
            code_col = and(255,code_col+1)
        } else if ( code_direction == LEFT_DIRECTION ){
            code_row = and(255,code_row-1+256)
            code_col = and(255,code_col-1+256)
        }
    } else if ( d == RIGHT_DIRECTION ){
        if ( code_direction == DOWN_DIRECTION ){
            code_row = and(255,code_row+1)
            code_col = and(255,code_col+1)
        } else if ( code_direction == UP_DIRECTION ){
            code_row = and(255,code_row-1+256)
            code_col = and(255,code_col+1)
        } else if ( code_direction == RIGHT_DIRECTION ){
            code_col = and(255,code_col+2)
        } else if ( code_direction == LEFT_DIRECTION ){
        }
    } else if ( d == LEFT_DIRECTION ){
        if ( code_direction == DOWN_DIRECTION ){
            code_row = and(255,code_row+1)
            code_col = and(255,code_col-1+256)
        } else if ( code_direction == UP_DIRECTION ){
            code_row = and(255,code_row-1+256)
            code_col = and(255,code_col-1+256)
        } else if ( code_direction == RIGHT_DIRECTION ){
        } else if ( code_direction == LEFT_DIRECTION ){
            code_col = and(255,code_col-2+256)
        }
    }
}
function GO_CODE_DIRECTION_FROM_STACK(x) {
    if ( code_direction == DOWN_DIRECTION ){
        code_row = and(255,code_stack_row[code_sp]+x)
        code_col = code_stack_col[code_sp]
    } else if ( code_direction == UP_DIRECTION ){
        code_row = and(255,code_stack_row[code_sp]-x+256)
        code_col = code_stack_col[code_sp]
    } else if ( code_direction == RIGHT_DIRECTION ){
        code_col = and(255,code_stack_col[code_sp]+x)
        code_row = code_stack_row[code_sp]
    } else if ( code_direction == LEFT_DIRECTION ){
        code_row = code_stack_row[code_sp]
        code_col = and(255,code_stack_col[code_sp]-x+256)
    }
}
function GO_CODE_DIRECTION(x) {
    if ( code_direction == DOWN_DIRECTION ){
        code_row = and(255,code_row+x)
    } else if ( code_direction == UP_DIRECTION ){
        code_row = and(255,code_row-x+256)
    } else if ( code_direction == RIGHT_DIRECTION ){
        code_col = and(255,code_col+x)
    } else if ( code_direction == LEFT_DIRECTION ){
        code_col = and(255,code_col-x+256)
    }
}
function GO_OPPOSITE_DATA_DIRECTION(x) {
    if ( data_direction == DOWN_DIRECTION ){
        HANDLE_DATA_ROW_INDIRECT_N(x)
    } else if ( data_direction == UP_DIRECTION ){
        HANDLE_DATA_ROW_INDIRECT_P(x)
    } else if ( data_direction == RIGHT_DIRECTION ){
        HANDLE_DATA_COL_INDIRECT_N(x)
    } else if ( data_direction == LEFT_DIRECTION ){
        HANDLE_DATA_COL_INDIRECT_P(x)
    }
}
function GO_DATA_THIS_DIRECTION(d,x) {
    if ( d == DOWN_DIRECTION ){
        HANDLE_DATA_ROW_INDIRECT_P(x)
    } else if ( d == UP_DIRECTION ){
        HANDLE_DATA_ROW_INDIRECT_N(x)
    } else if ( d == RIGHT_DIRECTION ){
        HANDLE_DATA_COL_INDIRECT_P(x)
    } else if ( d == LEFT_DIRECTION ){
        HANDLE_DATA_COL_INDIRECT_N(x)
    }
}
function GO_DATA_DIRECTION(x) {
    if ( data_direction == DOWN_DIRECTION ){
        HANDLE_DATA_ROW_INDIRECT_P(x)
    } else if ( data_direction == UP_DIRECTION ){
        HANDLE_DATA_ROW_INDIRECT_N(x)
    } else if ( data_direction == RIGHT_DIRECTION ){
        HANDLE_DATA_COL_INDIRECT_P(x)
    } else if ( data_direction == LEFT_DIRECTION ){
        HANDLE_DATA_COL_INDIRECT_N(x)
    }
}
function POSEDGE(x) {
    return(and(x,1))
}
function NEGEDGE(x) {
    return(!and(x,1))
}
