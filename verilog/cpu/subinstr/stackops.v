            if (instruction == `CPU_PUD) begin
                data_push_en <= '1;
                ucodenext <= 1;
            end else if (instruction == `CPU_POD) begin
                data_pop_en <= '1;
                ucodenext <= 1;
            end else if (instruction == `CPU_TOD) begin
                data_top_en <= '1;
                ucodenext <= 1;
            end else if (instruction == `CPU_QOD) begin
                data_toss_en <= '1;
                ucodenext <= 1;
            end else if (instruction == `CPU_XD) begin
                data_swap_en <= '1;
                ucodenext <= 1;
            end else if (instruction == `CPU_PUC) begin
                code_push_en <= '1;
                ucodenext <= 1;
            end else if (instruction == `CPU_POC) begin
                code_pop_en <= '1;
                ucodenext <= 1;
            end else if (instruction == `CPU_TOC) begin
                code_top_en <= '1;
                ucodenext <= 1;
            end else if (instruction == `CPU_QOC) begin
                code_toss_en <= '1;
                ucodenext <= 1;
            end else if (instruction == `CPU_XC) begin
                code_swap_en <= '1;
                ucodenext <= 1;

