            end else if (instruction == `CPU_DD) begin
                data_direction <= `DOWN_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_DR) begin
                data_direction <= `RIGHT_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_DU) begin
                data_direction <= `UP_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_DL) begin
                data_direction <= `LEFT_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_JDD) begin
                data_row <= data_row+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_JDR) begin
                data_col <= data_col+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_JDU) begin
                data_row <= data_row-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_JDL) begin
                ucodenext <= 1;
                data_col <= data_col-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_0DD) begin
                if ( !data_bit ) data_direction <= `DOWN_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_0DR) begin
                if ( !data_bit ) data_direction <= `RIGHT_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_0DU) begin
                if ( !data_bit ) data_direction <= `UP_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_0DL) begin
                if ( !data_bit ) data_direction <= `LEFT_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_0JDD) begin
                if ( !data_bit ) data_row <= data_row+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_0JDR) begin
                if ( !data_bit ) data_col <= data_col+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_0JDU) begin
                if ( !data_bit ) data_row <= data_row-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_0JDL) begin
                if ( !data_bit ) data_col <= data_col-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_1DD) begin
                if ( data_bit ) data_direction <= `DOWN_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_1DR) begin
                if ( data_bit ) data_direction <= `RIGHT_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_1DU) begin
                if ( data_bit ) data_direction <= `UP_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_1DL) begin
                if ( data_bit ) data_direction <= `LEFT_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_1JDD) begin
                if ( data_bit ) data_row <= data_row+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_1JDR) begin
                if ( data_bit ) data_col <= data_col+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_1JDU) begin
                if ( data_bit ) data_row <= data_row-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_1JDL) begin
                if ( data_bit ) data_col <= data_col-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i0DD) begin
                if ( !idata_bit ) data_direction <= `DOWN_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_i0DR) begin
                if ( !idata_bit ) data_direction <= `RIGHT_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_i0DU) begin
                if ( !idata_bit ) data_direction <= `UP_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_i0DL) begin
                if ( !idata_bit ) data_direction <= `LEFT_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_i0JDD) begin
                if ( !idata_bit ) data_row <= data_row+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i0JDR) begin
                if ( !idata_bit ) data_col <= data_col+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i0JDU) begin
                if ( !idata_bit ) data_row <= data_row-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i0JDL) begin
                if ( !idata_bit ) data_col <= data_col-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i1DD) begin
                if ( idata_bit ) data_direction <= `DOWN_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_i1DR) begin
                if ( idata_bit ) data_direction <= `RIGHT_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_i1DU) begin
                if ( idata_bit ) data_direction <= `UP_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_i1DL) begin
                if ( idata_bit ) data_direction <= `LEFT_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_i1JDD) begin
                if ( idata_bit ) data_row <= data_row+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i1JDR) begin
                if ( idata_bit ) data_col <= data_col+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i1JDU) begin
                if ( idata_bit ) data_row <= data_row-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i1JDL) begin
                if ( data_bit ) data_col <= data_col-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_CD) begin
                code_direction <= `DOWN_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_CR) begin
                code_direction <= `RIGHT_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_CU) begin
                code_direction <= `UP_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_CL) begin
                code_direction <= `LEFT_DIRECTION;
                ucodenext <= 1;
            end else if (instruction == `CPU_JCD) begin
                code_row <= code_row+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_JCR) begin
                code_col <= code_col+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_JCU) begin
                code_row <= code_row-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_JCL) begin
                code_col <= code_col-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_0JCD) begin
                if ( !data_bit ) code_row <= code_row+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_0JCR) begin
                if ( !data_bit ) code_col <= code_col+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_0JCU) begin
                if ( !data_bit ) code_row <= code_row-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_0JCL) begin
                if ( !data_bit ) code_col <= code_col-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_1JCD) begin
                if ( data_bit ) code_row <= code_row+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_1JCR) begin
                if ( data_bit ) code_col <= code_col+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_1JCU) begin
                if ( data_bit ) code_row <= code_row-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_1JCL) begin
                if ( data_bit ) code_col <= code_col-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i0JCD) begin
                if ( !idata_bit ) code_row <= code_row+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i0JCR) begin
                if ( !idata_bit ) code_col <= code_col+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i0JCU) begin
                if ( !idata_bit ) code_row <= code_row-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i0JCL) begin
                if ( !idata_bit ) code_col <= code_col-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i1JCD) begin
                if ( idata_bit ) code_row <= code_row+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i1JCR) begin
                if ( idata_bit ) code_col <= code_col+1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i1JCU) begin
                if ( idata_bit ) code_row <= code_row-1;
                ucodenext <= 1;
            end else if (instruction == `CPU_i1JCL) begin
                if ( data_bit ) code_col <= code_col-1;
                ucodenext <= 1;

