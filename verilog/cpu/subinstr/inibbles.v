            end else if (instruction == `CPU_HX0) begin
                udata_low_nibble <= 4'b0000;
                if ( data_col[4] == '0 ) begin
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;
            end else if (instruction == `CPU_HX1) begin
                if ( data_col[4] == '0 ) begin
                    udata_low_nibble <= 4'b0001;
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_high_nibble <= 4'b0001;
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;
            end else if (instruction == `CPU_HX2) begin
                if ( data_col[4] == '0 ) begin
                    udata_low_nibble <= 4'b0010;
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_high_nibble <= 4'b0010;
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;
            end else if (instruction == `CPU_HX3) begin
                if ( data_col[4] == '0 ) begin
                    udata_low_nibble <= 4'b0011;
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_high_nibble <= 4'b0011;
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;
            end else if (instruction == `CPU_HX4) begin
                if ( data_col[4] == '0 ) begin
                    udata_low_nibble <= 4'b0100;
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_high_nibble <= 4'b0100;
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;
            end else if (instruction == `CPU_HX5) begin
                if ( data_col[4] == '0 ) begin
                    udata_low_nibble <= 4'b0101;
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_high_nibble <= 4'b0101;
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;
            end else if (instruction == `CPU_HX6) begin
                if ( data_col[4] == '0 ) begin
                    udata_low_nibble <= 4'b0110;
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_high_nibble <= 4'b0110;
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;
            end else if (instruction == `CPU_HX7) begin
                if ( data_col[4] == '0 ) begin
                    udata_low_nibble <= 4'b0111;
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_high_nibble <= 4'b0111;
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;
            end else if (instruction == `CPU_HX8) begin
                if ( data_col[4] == '0 ) begin
                    udata_low_nibble <= 4'b1000;
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_high_nibble <= 4'b1000;
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;
            end else if (instruction == `CPU_HX9) begin
                if ( data_col[4] == '0 ) begin
                    udata_low_nibble <= 4'b1001;
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_high_nibble <= 4'b1001;
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;
            end else if (instruction == `CPU_HXA) begin
                if ( data_col[4] == '0 ) begin
                    udata_low_nibble <= 4'b1010;
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_high_nibble <= 4'b1010;
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;
            end else if (instruction == `CPU_HXB) begin
                if ( data_col[4] == '0 ) begin
                    udata_low_nibble <= 4'b1011;
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_high_nibble <= 4'b1011;
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;
            end else if (instruction == `CPU_HXC) begin
                if ( data_col[4] == '0 ) begin
                    udata_low_nibble <= 4'b1100;
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_high_nibble <= 4'b1100;
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;
            end else if (instruction == `CPU_HXD) begin
                if ( data_col[4] == '0 ) begin
                    udata_low_nibble <= 4'b1101;
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_high_nibble <= 4'b1101;
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;
            end else if (instruction == `CPU_HXE) begin
                if ( data_col[4] == '0 ) begin
                    udata_low_nibble <= 4'b1110;
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_high_nibble <= 4'b1110;
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;
            end else if (instruction == `CPU_HXF) begin
                if ( data_col[4] == '0 ) begin
                    udata_low_nibble <= 4'b1111;
                    udata_write_low_nibble <= 1;
                end else if ( data_col[4] == '1 ) begin
                    udata_high_nibble <= 4'b1111;
                    udata_write_high_nibble <= 1;
                end
                ucodenext <= 1;

