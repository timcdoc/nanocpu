            end else if (instruction == `CPU_P1D) begin
		`GO_DATA_DIRECTION(1)
                udataread <= 1;
                ucodenext <= 1;
            end else if (instruction == `CPU_N1D) begin
		`GO_OPPOSITE_DATA_DIRECTION(1)
                udataread <= 1;
                ucodenext <= 1;
            end else if (instruction == `CPU_P3D) begin
		`GO_DATA_DIRECTION(3)
                udataread <= 1;
                ucodenext <= 1;
            end else if (instruction == `CPU_N3D) begin
		`GO_OPPOSITE_DATA_DIRECTION(3)
                udataread <= 1;
                ucodenext <= 1;
            end else if (instruction == `CPU_P8D) begin
		`GO_DATA_DIRECTION(8)
                udataread <= 1;
                ucodenext <= 1;
            end else if (instruction == `CPU_N8D) begin
		`GO_OPPOSITE_DATA_DIRECTION(8)
                udataread <= 1;
                ucodenext <= 1;
            end else if (instruction == `CPU_P32D) begin
		`GO_DATA_DIRECTION(32)
                udataread <= 1;
                ucodenext <= 1;
            end else if (instruction == `CPU_N32D) begin
		`GO_OPPOSITE_DATA_DIRECTION(32)
                udataread <= 1;
                ucodenext <= 1;

