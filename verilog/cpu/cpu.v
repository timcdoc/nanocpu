`ifndef _CPU_V_
`define _CPU_V_
`include "cpu-const.h"//$PWD"../cpu-const.v"
`include "cpu-instr.h"//$PWD"../cpu-instr.v"
module cpu (
    input clk, input rst,
    output reg [7:0] instruction,
    output reg [7:0] data_byte
);

    reg [7:0] code_row; reg [7:0] code_col; reg [7:0] code_pag;
    reg [7:0] ind_data_pag; //This is needed, don't know what for yet, once we turn on pages!
    reg [7:0] ind_data_row;
    reg [7:0] data_row; reg [7:0] data_col; reg [7:0] data_pag;
    reg [2:0] code_direction; reg [2:0] data_direction;
    reg udatanext;
    reg umathregister;
    reg [1:0] data_ptr_mode;
    reg code_read_en, code_write_en;
    reg data_read_en, data_write_en;
    reg data_bit_set_en, data_bit_clear_en;
    reg [2:0] ustep;
    reg [2:0] inibble;
    reg [3:0] nibble[0:5];
    reg [31:0] register_dw;
    reg [15:0] register_a; reg [15:0] register_b;

    reg [7:0] tmp_row; reg [7:0] tmp_col; reg [7:0] tmp_pag;
    reg [7:0] code_sp; reg [7:0] data_sp;
    reg [7:0] code_stack_row[0:255]; reg [7:0] code_stack_col[0:255]; reg [7:0] code_stack_pag[0:255];
    reg [7:0] data_stack_row[0:255]; reg [7:0] data_stack_col[0:255]; reg [7:0] data_stack_pag[0:255];
    reg [7:0] idsp_addr;
    reg [7:0] idata_row; reg [7:0] idata_col; reg [7:0] idata_pag;
    reg [2:0] idata_col_bits; reg [2:0] data_col_bits;
    reg [13:0] data_address; reg [13:0] code_address;
    wire [7:0] data_read_byte; wire [7:0] code_read_byte;

    localparam DURATION = 189990;

    codeBRAM_type codeBRAM (
	.read_clk(clk), .write_clk(clk),
	.data_in(instruction), .data_out(code_read_byte),
	.read_address(code_address), .write_address(code_address),
	.read_en(code_read_en), .write_en(code_write_en)
    );

    dataBRAM_type dataBRAM (
	.read_clk(clk), .write_clk(clk),
	.data_in(data_byte), .data_out(data_read_byte),
	.read_address(data_address), .write_address(data_address),
	.read_en(data_read_en), .write_en(data_write_en)
    );

    always @ (*) begin // These are the time wasters and arbitration latches.
        instruction <= code_read_byte;
        if ( data_bit_set_en ) begin 
            if ( `INDIRECT_MODE ) begin 
                data_byte[idata_col_bits] <= 1;
            end else begin
                data_byte[data_col_bits] <= 1;
            end
        end else if ( data_bit_clear_en ) begin 
            if ( `INDIRECT_MODE ) begin 
                data_byte[idata_col_bits] <= 0;
            end else begin
                data_byte[data_col_bits] <= 0;
            end
        end else begin
            data_byte <= data_read_byte;
        end
        code_address <= (('b00111111&code_row)<<7)|('b0111111&code_col);
        if ( `INDIRECT_MODE ) begin 
            data_address <= ((8'b00000001&idata_pag)<<14)|(idata_row<<5)|(idata_col>>3);
        end else begin
            data_address <= ((8'b00000001&data_pag)<<14)|(data_row<<5)|(data_col>>3);
        end
        data_col_bits <= (3'b111-(3'b111&data_col));
        idata_col_bits <= (3'b111-(3'b111&idata_col));
        idsp_addr <= 8'hFF&(data_sp+ind_data_row);
        idata_col <= 8'hFF&(data_col+data_stack_col[idsp_addr]);
        idata_row <= data_stack_row[idsp_addr];
        idata_pag <= data_stack_pag[idsp_addr];
    end

    always @ ( posedge clk ) begin

        if ( rst ) begin // A reasonable start for a reset.
            data_direction<=`RIGHT_DIRECTION;
            code_direction<=`DOWN_DIRECTION;
            {code_row,code_col,code_pag}<='hFF0000;
            {code_read_en,code_write_en,data_read_en,data_write_en,data_bit_set_en,data_bit_clear_en}<='b000000;
            {ind_data_pag,ind_data_row}<='h0000;
            idata_pag<='h00;
            {data_row,data_col,data_pag}<='h000000;
            code_sp<='hFF;
            data_sp<='hFF;
            umathregister<=0;
            register_dw<='b0000000000000000000000000000000;
            register_a<='b00000000000000000;
            register_b<='b00000000000000000;
            data_byte<='b00000000;
            data_ptr_mode<=`DIRECT_VAL;
            instruction<=`CPU_NOP;
            ustep <= 'b000;
            inibble <= 3'b000;
        end else begin
            case ( instruction )
            `Fn_NOP_and_default
            `Fn_POD
            `Fn_PUC
            `Fn_PUD
            `Fn_POC
            `Fn_POD2C
            `Fn_POC2D
            `Fn_TOC
            `Fn_QOC
            `Fn_HX_
            `Fn_LCI
            `Fn_DF
            `Fn_DB
            `Fn_DU //etc
            `Fn_JDL //etc
            `Fn_0JDL //etc
            `Fn_CD
            `Fn_CF
            `Fn_CB
            `Fn_JCL //etc
            `Fn_0JCL //etc
            `Fn_P1D
            `Fn_N1D
            `Fn_P3D
            `Fn_N3D
            `Fn_PDI
            `Fn_1CU
            `Fn_P1 //etc
            `Fn_1NP //etc
            `Fn_DNM
            `Fn_DIM
            `Fn_DSM
            `Fn_PUSW
            `Fn_POSW
            `Fn_HLT
            endcase
        end
    end

    initial begin
        $readmemh("/home/notsystem/nanocpu/adder/adder.stack.hex", code_stack_row);
        $readmemh("/home/notsystem/nanocpu/adder/adder.stack.hex", code_stack_col);
        $readmemh("/home/notsystem/nanocpu/adder/adder.stack.hex", code_stack_pag);
        $readmemh("/home/notsystem/nanocpu/adder/adder.stack.hex", data_stack_row);
        $readmemh("/home/notsystem/nanocpu/adder/adder.stack.hex", data_stack_col);
        $readmemh("/home/notsystem/nanocpu/adder/adder.stack.hex", data_stack_pag);
    end

endmodule
`endif
