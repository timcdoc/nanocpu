module codeBRAM_type (
    input read_clk,
    input write_clk,
    input [7:0]data_in,
    output reg [7:0]data_out,

    input [13:0] read_address,
    input [13:0] write_address,

    input read_en,
    input write_en
);

    reg [7:0] memory_data [16383:0];

    always @( posedge read_clk ) begin
        if ( read_en ) begin
            data_out <= memory_data[read_address];   
        end 
    end 

    always @( posedge write_clk ) begin
        if ( write_en ) begin
            memory_data[write_address] <= data_in;
        end
    end

    initial begin
        $readmemh("/home/notsystem/nanocpu/adder/adder.awk.code.hex", memory_data,0,16383);
    end
endmodule
module dataBRAM_type (
    input read_clk,
    input write_clk,
    input [7:0]data_in,
    output reg [7:0]data_out,

    input [13:0] read_address,
    input [13:0] write_address,

    input read_en,
    input write_en
);

    localparam DURATION = 189990;
    reg [7:0] memory_data [16383:0];

    always @( posedge read_clk ) begin
        if ( read_en ) begin
            data_out <= memory_data[read_address];   
        end 
    end 

    always @( posedge write_clk ) begin
        if ( write_en ) begin
            memory_data[write_address] <= data_in;
        end
    end

    initial begin
        $readmemh("/home/notsystem/nanocpu/adder/adder.awk.data.hex", memory_data,0,16383);
    end

    initial begin
	#(DURATION)
        $writememh("/home/notsystem/nanocpu/adder/adder.verilog.data.out", memory_data,0,16383);
    end
endmodule

