#Begin section to get around verilog include brain damage
for i in *.v
do
    grep "\`include" $i | sed 's&`include "\([^"][^"]*\)"//\(\$[A-Z][A-Z]*\)"\([^"][^"]*\)"&echo -e // Danger: Automatically generated to get around verilogs braindead include, dont even get me started --timc\\\\n\\`include \\"`echo -en \2`/\3\\">\1&'|bash
done
#END section to get around verilog include brain damage
#apio init -b iCE40-HX8K-EVB
apio clean
apio verify
apio build -v
apio sim
ls -als ../../adder/adder.awk.data.out
diff -b ../../adder/adder.awk.data.out ../../adder/adder.verilog.data.out && echo PASS || echo FAIL
gtkwave -S gtkwave.tcl cpu_tb.vcd cpu_tb.gtkw
#apio raw 'yosys -p "read_verilog cpu.v; show" -q'
#apio upload
