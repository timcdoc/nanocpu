`timescale 1 ns / 10 ps
`include "cpu-const.h"//$PWD"../cpu-const.v"

module cpu_tb();
    reg clk;
    reg rst;
    reg memory_data_read_en;
    reg memory_data_write_en;
    reg memory_code_read_en;
    reg memory_code_write_en;
    wire [7:0] read_byte;
    wire [7:0] instruction;
    
    localparam DURATION = 19000000;

    always begin
	#41.67
	clk =~clk;
    end
    
    cpu uut (
	.clk(clk),
	.rst(rst),
	.instruction(instruction),
	.data_byte(read_byte)
    );

    initial begin
        clk=0;
        #(2*41.67);
        rst=1;
        #(2*41.67);
        rst=0;

    end

    initial begin
	$dumpfile("cpu_tb.vcd");
	$dumpvars(0, cpu_tb);
	#(DURATION)
        $display("Finished!");
        $finish;
    end
endmodule
	
