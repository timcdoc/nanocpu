`include "memory/memory.v"
module nanocpu(input clk,
    input rc_en, input [7:0] crow, input [4:0] ccol,
    output [7:0] rc_data,
    input wd_en, input rd_en, input [7:0] drow, input [4:0] dcol, input [2:0] dsubcol, input wd_bit, output rd_bit
);

    memory #(.INIT_FILE("memory.in")) mem (
        .clk(clk),
        .rc_en(rc_en),
        .crow(crow),
        .ccol(ccol),
        .rc_data(rc_data),
        .wd_en(wd_en),
        .rd_en(rd_en),
        .drow(drow),
        .dcol(dcol),
        .dsubcol(dsubcol),
        .wd_bit(wd_bit),
        .rd_bit(rd_bit)
    );

//    always @ ( posedge clk ) begin
//        instr<=mem[{crow,ccol}];
//    end

endmodule
