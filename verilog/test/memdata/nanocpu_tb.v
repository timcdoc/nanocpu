`timescale 10 ns / 1 ps
`include "cpu-const.h"//$PWD"../cpu-const.v"

module nanocpu_tb();

wire rd_bit; 
reg clk = 0;
reg rd_en = 0;
reg [`MEM_ROWS_BITS_1:0] drow = 0;
reg [`MEM_COLS_BITS_1:0] dcol = 0;
reg [`BYTES_BITS_1:0] dsubcol = 0;

localparam DURATION=10000;
integer i;
integer j;
integer k;

always begin
    #41.677
    clk = ~clk;
end

nanocpu uut ( .clk(clk), .rd_en(rd_en), .drow(drow), .dcol(dcol), .dsubcol(dsubcol), .rd_bit(rd_bit) );

initial begin

  $dumpfile("nanocpu_tb.vcd");
  $dumpvars(0, nanocpu_tb);
	#10
    rd_en=1; drow='h00; dcol='h00;
    for (i=0; i<4; i=i+1) begin
        for (j=0; j<4; j=j+1) begin
            for (k=0; k<8; k=k+1) begin
                #(2*41.67)
                drow=i;
                dcol=j;
                dsubcol=k;
                rd_en=1;
                #(2*41.67)
                rd_en=0;
            end
        end
    end


   #(DURATION) $display("Finished");
  $finish;
end

endmodule
