`timescale 1 ns / 10 ps
`include "cpu-const.h"//$PWD"../../cpu-const.v"

module data_and_code_stack_and_pointer_tb ();
    reg clk = 0;
    reg rst = 0;
    reg data_push_en = 0;
    reg data_pop_en = 0;
    reg data_top_en = 0;
    reg data_toss_en = 0;
    reg data_swap_en = 0;
    reg [7:0] data_idata_row;
    reg [7:0] data_idata_col;
    wire [7:0] data_odata_row;
    wire [7:0] data_odata_col;
    reg code_push_en = 0;
    reg code_pop_en = 0;
    reg code_top_en = 0;
    reg code_toss_en = 0;
    reg code_swap_en = 0;
    reg [7:0] code_idata_row;
    reg [7:0] code_idata_col;
    wire [7:0] code_odata_row;
    wire [7:0] code_odata_col;
    
    localparam DURATION = 10000;

    always begin
	#41.67
	clk =~clk;
    end
    
    code_stack_and_pointer code_uut (
	.clk(clk),
	.rst(rst),
	.push_en(code_push_en),
	.pop_en(code_pop_en),
	.top_en(code_top_en),
	.toss_en(code_toss_en),
	.swap_en(code_swap_en),
	.idata_row(code_idata_row),
	.idata_col(code_idata_col),
	.odata_row(code_odata_row),
	.odata_col(code_odata_col)
    );

    data_stack_and_pointer data_uut (
	.clk(clk),
	.rst(rst),
	.push_en(data_push_en),
	.pop_en(data_pop_en),
	.top_en(data_top_en),
	.toss_en(data_toss_en),
	.swap_en(data_swap_en),
	.idata_row(data_idata_row),
	.idata_col(data_idata_col),
	.odata_row(data_odata_row),
	.odata_col(data_odata_col)
    );

    initial begin
        #(2*41.67)
        rst=1;
        #(2*41.67)
        rst=0;

	#(2*41.67)
	code_idata_row='hBE; code_idata_col='hEF; code_push_en=1;
	data_idata_row='h33; data_idata_col='h79; data_push_en=1;
	#(2*41.67)
	code_idata_row='h00; code_idata_col='h00; code_push_en=0;
	data_idata_row='h00; data_idata_col='h00; data_push_en=0;

	#(2*41.67)
	code_idata_row='hA5; code_idata_col='h5A; code_push_en=1;
	data_idata_row='h27; data_idata_col='h72; data_push_en=1;
	#(2*41.67)
	code_idata_row='h00; code_idata_col='h00; code_push_en=0;
	data_idata_row='h00; data_idata_col='h00; data_push_en=0;

	#(2*41.67)
	code_toss_en=1;
	data_toss_en=1;
	#(2*41.67)
	code_toss_en=0;
	data_toss_en=0;

	#(2*41.67)
	code_swap_en=1;
	data_swap_en=1;
	#(2*41.67)
	code_swap_en=0;
	data_swap_en=0;

	#(2*41.67)
	code_swap_en=1;
	data_swap_en=1;
	#(2*41.67)
	code_swap_en=0;
	data_swap_en=0;

	#(2*41.67)
	code_swap_en=1;
	data_swap_en=1;
	#(2*41.67)
	code_swap_en=0;
	data_swap_en=0;

	#(2*41.67)
	code_pop_en=1;
	data_pop_en=1;
	#(2*41.67)
	code_pop_en=0;
	data_pop_en=0;

    end

    initial begin
	$dumpfile("rowcol_tb.vcd");
	$dumpvars(1, data_and_code_stack_and_pointer_tb);
	#(DURATION)
	$display("Finished!");
	$finish;
    end
endmodule
