`timescale 10 ns / 1 ps
`include "cpu-const.h"//$PWD"../cpu-const.v"

module nanocpu_tb();

wire [`CODE_BITS_1:0] rc_data; 
reg clk = 0;
reg rc_en = 0;
reg [`MEM_ROWS_BITS_1:0] crow = 0;
reg [`MEM_COLS_BITS_1:0] ccol = 0;

localparam DURATION=10000;
integer i;
integer j;

always begin
    #41.677
    clk = ~clk;
end

nanocpu uut ( .clk(clk), .rc_en(rc_en), .crow(crow), .ccol(ccol), .rc_data(rc_data));

initial begin

  $dumpfile("nanocpu_tb.vcd");
  $dumpvars(0, nanocpu_tb);
	#10
    rc_en=1; crow='h00; ccol='h00;
    for (i=0; i<4; i=i+1) begin
        for (j=0; j<4; j=j+1) begin
            #(2*41.67)
            ccol=i;
            crow=j;
            rc_en=1;
            #(2*41.67)
            rc_en=0;
        end
    end


   #(DURATION) $display("Finished");
  $finish;
end

endmodule
