module nanocpu(input clk,
`ifdef MAKE_CODE_TEST
    input rc_en, input [`MEM_ROWS_BITS_1:0] crow, input [`MEM_COLS_BITS_1:0] ccol,
    output [`CODE_BITS_1:0] rc_data
`ifdef MAKE_DATA_TEST
    ,
`endif
`endif
`ifdef MAKE_DATA_TEST
    input rd_en, input [`MEM_ROWS_BITS_1:0] drow, input [`MEM_COLS_BITS_1:0] dcol, input [`BYTES_BITS_1:0] dsubcol, output rd_bit
`endif
);

    memory #(.INIT_FILE("memory.in")) mem (
        .clk(clk),
`ifdef MAKE_CODE_TEST
        .rc_en(rc_en),
        .crow(crow),
        .ccol(ccol),
        .rc_data(rc_data)
`ifdef MAKE_DATA_TEST
    ,
`endif
`endif
`ifdef MAKE_DATA_TEST
        .rd_en(rd_en),
        .drow(drow),
        .dcol(dcol),
        .dsubcol(dsubcol),
        .rd_bit(rd_bit)
`endif
    );

//    always @ ( posedge clk ) begin
//        instr<=mem[{crow,ccol}];
//    end

endmodule
