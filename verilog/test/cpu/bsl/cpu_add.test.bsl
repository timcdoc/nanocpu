Digital_Bus
     Name:              instruction[7:0]
          Edge:               166680.0 00
          Edge:               333360.0 58
          Edge:               500040.0 00
          Edge:               666720.0 59
          Edge:               833400.0 50
          Edge:               916740.0 5A
          Edge:               1083420.0 5B
          Edge:               1250100.0 00
Digital_Bus
     Name:              code_row[7:0]
          Edge:               83340.0 80
          Edge:               208350.0 81
          Edge:               291690.0 82
          Edge:               541710.0 83
          Edge:               625050.0 84
          Edge:               1291770.0 85
          Edge:               1375110.0 86
          Edge:               1458450.0 87
          Edge:               1541790.0 88
          Edge:               1625130.0 89
          Edge:               1708470.0 8A
          Edge:               1791810.0 8B
          Edge:               1875150.0 8C
          Edge:               1958490.0 8D
          Edge:               2041830.0 8E
          Edge:               2125170.0 8F
          Edge:               2208510.0 90
          Edge:               2291850.0 91
          Edge:               2375190.0 92
          Edge:               2458530.0 93
          Edge:               2541870.0 94
          Edge:               2625210.0 95
          Edge:               2708550.0 96
          Edge:               2791890.0 97
          Edge:               2875230.0 98
          Edge:               2958570.0 99
          Edge:               3041910.0 9A
          Edge:               3125250.0 9B
          Edge:               3208590.0 9C
          Edge:               3291930.0 9D
          Edge:               3375270.0 9E
          Edge:               3458610.0 9F
          Edge:               3541950.0 A0
          Edge:               3625290.0 A1
          Edge:               3708630.0 A2
          Edge:               3791970.0 A3
          Edge:               3875310.0 A4
          Edge:               3958650.0 A5
          Edge:               4041990.0 A6
          Edge:               4125330.0 A7
          Edge:               4208670.0 A8
          Edge:               4292010.0 A9
          Edge:               4375350.0 AA
          Edge:               4458690.0 AB
          Edge:               4542030.0 AC
          Edge:               4625370.0 AD
          Edge:               4708710.0 AE
          Edge:               4792050.0 AF
          Edge:               4875390.0 B0
          Edge:               4958730.0 B1
          Edge:               5042070.0 B2
          Edge:               5125410.0 B3
          Edge:               5208750.0 B4
          Edge:               5292090.0 B5
          Edge:               5375430.0 B6
          Edge:               5458770.0 B7
          Edge:               5542110.0 B8
          Edge:               5625450.0 B9
          Edge:               5708790.0 BA
          Edge:               5792130.0 BB
          Edge:               5875470.0 BC
          Edge:               5958810.0 BD
          Edge:               6042150.0 BE
          Edge:               6125490.0 BF
          Edge:               6208830.0 C0
          Edge:               6292170.0 C1
          Edge:               6375510.0 C2
          Edge:               6458850.0 C3
          Edge:               6542190.0 C4
          Edge:               6625530.0 C5
          Edge:               6708870.0 C6
          Edge:               6792210.0 C7
          Edge:               6875550.0 C8
          Edge:               6958890.0 C9
          Edge:               7042230.0 CA
          Edge:               7125570.0 CB
          Edge:               7208910.0 CC
          Edge:               7292250.0 CD
          Edge:               7375590.0 CE
          Edge:               7458930.0 CF
          Edge:               7542270.0 D0
          Edge:               7625610.0 D1
          Edge:               7708950.0 D2
          Edge:               7792290.0 D3
          Edge:               7875630.0 D4
          Edge:               7958970.0 D5
          Edge:               8042310.0 D6
          Edge:               8125650.0 D7
          Edge:               8208990.0 D8
          Edge:               8292330.0 D9
          Edge:               8375670.0 DA
          Edge:               8459010.0 DB
          Edge:               8542350.0 DC
          Edge:               8625690.0 DD
          Edge:               8709030.0 DE
          Edge:               8792370.0 DF
          Edge:               8875710.0 E0
          Edge:               8959050.0 E1
          Edge:               9042390.0 E2
          Edge:               9125730.0 E3
          Edge:               9209070.0 E4
          Edge:               9292410.0 E5
          Edge:               9375750.0 E6
          Edge:               9459090.0 E7
          Edge:               9542430.0 E8
          Edge:               9625770.0 E9
          Edge:               9709110.0 EA
          Edge:               9792450.0 EB
          Edge:               9875790.0 EC
          Edge:               9959130.0 ED
Digital_Bus
     Name:              read_byte[7:0]
          Edge:               250020.0 5A
          Edge:               416700.0 A5
          Edge:               583380.0 11
          Edge:               750060.0 01
Digital_Bus
     Name:              register_a[15:0]
          Edge:               83340.0 0000
          Edge:               375030.0 5A00
          Edge:               458370.0 5AA5
          Edge:               916740.0 6BA6
Digital_Bus
     Name:              register_b[15:0]
          Edge:               83340.0 0000
          Edge:               708390.0 1100
          Edge:               791730.0 1101
          Edge:               916740.0 0000
Digital_Bus
     Name:              register_dw[31:0]
          Edge:               83340.0 00000000
          Edge:               875070.0 00006BA6
Digital_Bus
     Name:              write_byte[7:0]
          Edge:               83340.0 00
          Edge:               958410.0 6B
          Edge:               1041750.0 A6
          Edge:               1125090.0 00

