Digital_Bus
     Name:              instruction[7:0]
          Edge:               250020.0 0A
          Edge:               333360.0 19
          Edge:               416700.0 1D
          Edge:               500040.0 0D
          Edge:               583380.0 18
          Edge:               666720.0 1C
          Edge:               750060.0 0B
          Edge:               833400.0 0A
          Edge:               916740.0 0C
          Edge:               1000080.0 09
          Edge:               1083420.0 00
Digital_Bus
     Name:              code_direction[1:0]
          Edge:               83340.0 00
          Edge:               375030.0 01
          Edge:               625050.0 00
Digital_Bus
     Name:              code_row[7:0]
          Edge:               83340.0 80
          Edge:               333360.0 81
          Edge:               666720.0 82
          Edge:               708390.0 83
          Edge:               750060.0 84
          Edge:               833400.0 85
          Edge:               916740.0 86
          Edge:               1000080.0 87
          Edge:               1083420.0 88
          Edge:               1166760.0 89
          Edge:               1250100.0 8A
          Edge:               1333440.0 8B
          Edge:               1416780.0 8C
          Edge:               1500120.0 8D
          Edge:               1583460.0 8E
          Edge:               1666800.0 8F
          Edge:               1750140.0 90
          Edge:               1833480.0 91
          Edge:               1916820.0 92
          Edge:               2000160.0 93
          Edge:               2083500.0 94
          Edge:               2166840.0 95
          Edge:               2250180.0 96
          Edge:               2333520.0 97
          Edge:               2416860.0 98
          Edge:               2500200.0 99
          Edge:               2583540.0 9A
          Edge:               2666880.0 9B
          Edge:               2750220.0 9C
          Edge:               2833560.0 9D
          Edge:               2916900.0 9E
          Edge:               3000240.0 9F
          Edge:               3083580.0 A0
          Edge:               3166920.0 A1
          Edge:               3250260.0 A2
          Edge:               3333600.0 A3
          Edge:               3416940.0 A4
          Edge:               3500280.0 A5
          Edge:               3583620.0 A6
          Edge:               3666960.0 A7
          Edge:               3750300.0 A8
          Edge:               3833640.0 A9
          Edge:               3916980.0 AA
          Edge:               4000320.0 AB
          Edge:               4083660.0 AC
          Edge:               4167000.0 AD
          Edge:               4250340.0 AE
          Edge:               4333680.0 AF
          Edge:               4417020.0 B0
          Edge:               4500360.0 B1
          Edge:               4583700.0 B2
          Edge:               4667040.0 B3
          Edge:               4750380.0 B4
          Edge:               4833720.0 B5
          Edge:               4917060.0 B6
          Edge:               5000400.0 B7
          Edge:               5083740.0 B8
          Edge:               5167080.0 B9
          Edge:               5250420.0 BA
          Edge:               5333760.0 BB
          Edge:               5417100.0 BC
          Edge:               5500440.0 BD
          Edge:               5583780.0 BE
          Edge:               5667120.0 BF
          Edge:               5750460.0 C0
          Edge:               5833800.0 C1
          Edge:               5917140.0 C2
          Edge:               6000480.0 C3
          Edge:               6083820.0 C4
          Edge:               6167160.0 C5
          Edge:               6250500.0 C6
          Edge:               6333840.0 C7
          Edge:               6417180.0 C8
          Edge:               6500520.0 C9
          Edge:               6583860.0 CA
          Edge:               6667200.0 CB
          Edge:               6750540.0 CC
          Edge:               6833880.0 CD
          Edge:               6917220.0 CE
          Edge:               7000560.0 CF
          Edge:               7083900.0 D0
          Edge:               7167240.0 D1
          Edge:               7250580.0 D2
          Edge:               7333920.0 D3
          Edge:               7417260.0 D4
          Edge:               7500600.0 D5
          Edge:               7583940.0 D6
          Edge:               7667280.0 D7
          Edge:               7750620.0 D8
          Edge:               7833960.0 D9
          Edge:               7917300.0 DA
          Edge:               8000640.0 DB
          Edge:               8083980.0 DC
          Edge:               8167320.0 DD
          Edge:               8250660.0 DE
          Edge:               8334000.0 DF
          Edge:               8417340.0 E0
          Edge:               8500680.0 E1
          Edge:               8584020.0 E2
          Edge:               8667360.0 E3
          Edge:               8750700.0 E4
          Edge:               8834040.0 E5
          Edge:               8917380.0 E6
          Edge:               9000720.0 E7
          Edge:               9084060.0 E8
          Edge:               9167400.0 E9
          Edge:               9250740.0 EA
          Edge:               9334080.0 EB
          Edge:               9417420.0 EC
          Edge:               9500760.0 ED
          Edge:               9584100.0 EE
          Edge:               9667440.0 EF
          Edge:               9750780.0 F0
          Edge:               9834120.0 F1
          Edge:               9917460.0 F2
Digital_Bus
     Name:              code_col[7:0]
          Edge:               83340.0 00
          Edge:               416700.0 01
          Edge:               458370.0 02
          Edge:               500040.0 03
          Edge:               583380.0 04
Digital_Signal
     Name:              code_push_en
          Edge:               83340.0 0
          Edge:               291690.0 1
          Edge:               333360.0 0
          Edge:               875070.0 1
          Edge:               916740.0 0
Digital_Signal
     Name:              code_pop_en
          Edge:               83340.0 0
          Edge:               1041750.0 1
          Edge:               1083420.0 0
Digital_Signal
     Name:              code_swap_en
          Edge:               83340.0 0
          Edge:               541710.0 1
          Edge:               583380.0 0
Digital_Signal
     Name:              code_top_en
          Edge:               83340.0 0
          Edge:               791730.0 1
          Edge:               833400.0 0
Digital_Signal
     Name:              code_toss_en
          Edge:               83340.0 0
          Edge:               958410.0 1
          Edge:               1000080.0 0

