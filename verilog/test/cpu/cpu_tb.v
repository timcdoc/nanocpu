`timescale 1 ns / 10 ps
`include "cpu-const.h"//$PWD"../../cpu-const.v"

module cpu_tb();
    reg clk = 0;
    reg rst = 0;
    reg [7:0] instruction;
    reg [7:0] read_byte;
    reg data_bit;
    reg idata_bit;
    wire data_push_en;
    wire data_pop_en;
    wire data_top_en;
    wire data_toss_en;
    wire data_swap_en;
    wire read_byte_en;
    wire write_byte_en;
    wire [7:0] write_byte;
    wire [1:0] data_direction;
    wire [1:0] code_direction;
    wire [7:0] code_row;
    wire [7:0] code_col;
    wire [7:0] data_row;
    wire [7:0] data_col;
    
    localparam DURATION = 10000;

    always begin
	#41.67
	clk =~clk;
    end
    
    cpu uut (
	.clk(clk),
	.rst(rst),
	.instruction(instruction),
	.read_byte(read_byte),
	.data_bit(data_bit),
	.idata_bit(idata_bit),
	.data_push_en(data_push_en),
	.data_pop_en(data_pop_en),
	.data_top_en(data_top_en),
	.data_toss_en(data_toss_en),
	.data_swap_en(data_swap_en),
	.write_byte_en(write_byte_en),
	.write_byte(write_byte),
	.data_direction(data_direction),
	.code_direction(code_direction),
	.code_row(code_row),
	.code_col(code_col),
	.data_row(data_row),
	.data_col(data_col)
    );

    // Hack until connected to actual memory.
    always @ (negedge write_byte_en or negedge read_byte_en ) begin
        read_byte=write_byte;
        data_bit<=read_byte[data_col[2:0]];
    end

    initial begin
        read_byte='b00000000;
        #(2*41.67);
        rst=1;
        #(2*41.67);
        rst=0;

`include "cpu-test.h"//$PWD"bsl/cpu-test.v"

    end

    initial begin
	$dumpfile("cpu_tb.vcd");
	$dumpvars(0, cpu_tb);
	#(DURATION)
	$display("Finished!");
	$finish;
    end
endmodule
	
