#Begin section to get around verilog include brain damage
for i in *.v
do
    grep "\`include" $i | sed 's&`include "\([^"][^"]*\)"//\(\$[A-Z][A-Z]*\)"\([^"][^"]*\)"&echo -e // Danger: Automatically generated to get around verilogs braindead include, dont even get me started --timc\\\\n\\`include \\"`echo -en \2`/\3\\">\1&'|bash
done
#END section to get around verilog include brain damage
#apio init -b iCE40-HX8K-EVB
apio clean
export PATH=/home/timc/.apio/packages/tools-oss-cad-suite/bin:/home/timc/.apio/packages/tools-oss-cad-suite/lib:/home/timc/.apio/packages/tools-system/bin:/home/timc/.apio/packages/toolchain-dfu/bin:/home/timc/.apio/packages/toolchain-icesprog/bin:/home/timc/.apio/packages/toolchain-fujprog/bin:/home/timc/.apio/packages/toolchain-verilator/bin:/home/timc/.apio/packages/toolchain-iverilog/bin:/home/timc/.apio/packages/toolchain-ecp5/bin:/home/timc/.apio/packages/toolchain-ice40/bin:/home/timc/.apio/packages/toolchain-yosys/bin:/home/timc/.apio/packages/tool-scons/script:/home/timc/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
rm bsl/*.log
rm totals.log
for i in bsl/*.test
do
    cp $i bsl/cpu-test.v
    apio build -v
    iverilog -B "/home/timc/.apio/packages/tools-oss-cad-suite/lib/ivl" -o cpu_tb.out -D VCD_OUTPUT=cpu_tb -D NO_ICE40_DEFAULT_ASSIGNMENTS "/home/timc/.apio/packages/tools-oss-cad-suite/share/yosys/ice40/cells_sim.v" -g2005-sv cpu.v cpu_tb.v
    vvp -M "/home/timc/.apio/packages/tools-oss-cad-suite/lib/ivl" cpu_tb.out
    gtkwave -S gtkwave.tcl cpu_tb.vcd cpu_tb.gtkw
    mv tim $i.log
    echo $i >>totals.log
    awk -f check.awk $i.bsl $i.log >>totals.log
done
