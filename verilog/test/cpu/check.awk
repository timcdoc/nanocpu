BEGIN {
    state=0
} {
    if ( FILENAME ~ /.test.bsl/ ) {
        code=FILENAME
        sub( /.test.bsl/, "", code )
        if ( ( $0 ~ /^Digital_Bus/ ) || ( $0 ~ /^Digital_Signal/ ) ) {
            state = 1
        } else if ( ( state == 1 ) && ( $0 ~ /^     Name:/ ) ) {
            state = 2
            lastname=$2
            aname[$2]=0
        } else if ( ( state == 2 ) && ( $0 ~ /^          Edge:/ ) ) {
            atime[lastname,aname[lastname]]="" $2
            avalue[lastname,aname[lastname]]="" $3
            aname[lastname]++
        }
    } else {
        if ( ( $0 ~ /^Digital_Bus/ ) || ( $0 ~ /^Digital_Signal/ ) ) {
            state = 1
        } else if ( ( state == 1 ) && ( $0 ~ /^     Name:/ ) ) {
            state = 2
            lastname=$2
            thisedge=0
        } else if ( ( state == 2 ) && ( $0 ~ /^          Edge:/ ) && ( lastname in aname ) ) {
            if ( ( atime[lastname,thisedge] == "" $2 ) && ( avalue[lastname,thisedge] == "" $3 ) ) {
                printf( "PASSED: %s %s %s %s\n", code, lastname, atime[lastname,thisedge], avalue[lastname,thisedge] )
            } else {
                printf( "FAILED: %s %s %s %s\n", code, thisname, atime[lastname,thisedge], avalue[lastname,thisedge] )
            }
            delete atime[lastname,thisedge]
            delete avalue[lastname,thisedge]
            thisedge++
        }
    }
} END {
    for ( thisname in aname ) {
        thisedge = aname[thisname]
        for ( i = 0; i < thisedge; i++ ) {
            if ( ( (thisname,i) in atime ) || ( (thisname,i) in avalue ) ) {
                printf( "FAILED: MISSING in post %s %s %s %s\n", code, thisname, atime[thisname,i], avalue[thisname,i] )
            }
        }
    }
}
