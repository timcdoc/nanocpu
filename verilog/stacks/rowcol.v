`ifndef _ROWCOL_V_
`define _ROWCOL_V_
`timescale 1 ns / 10 ps
`include "cpu-const.h"//$PWD"../cpu-const.v"

module code_stack_and_pointer(
    input clk,
    input rst,
    input push_en,
    input pop_en,
    input top_en,
    input toss_en,
    input swap_en,
    input [7:0] idata_row,
    output [7:0] odata_row,
    input [7:0] idata_col,
    output [7:0] odata_col
);
    
    stack_and_pointer row (
	.clk(clk),
	.rst(rst),
	.push_en(push_en),
	.pop_en(pop_en),
	.top_en(top_en),
	.toss_en(toss_en),
	.swap_en(swap_en),
	.idata(idata_row),
	.odata(odata_row)
    );

    stack_and_pointer col (
	.clk(clk),
	.rst(rst),
	.push_en(push_en),
	.pop_en(pop_en),
	.top_en(top_en),
	.toss_en(toss_en),
	.swap_en(swap_en),
	.idata(idata_col),
	.odata(odata_col)
    );


endmodule

module data_stack_and_pointer(
    input clk,
    input rst,
    input push_en,
    input pop_en,
    input top_en,
    input toss_en,
    input swap_en,
    input [7:0] idata_row,
    output [7:0] odata_row,
    input [7:0] idata_col,
    output [7:0] odata_col
);
    
    stack_and_pointer row (
	.clk(clk),
	.rst(rst),
	.push_en(push_en),
	.pop_en(pop_en),
	.top_en(top_en),
	.toss_en(toss_en),
	.swap_en(swap_en),
	.idata(idata_row),
	.odata(odata_row)
    );

    stack_and_pointer col (
	.clk(clk),
	.rst(rst),
	.push_en(push_en),
	.pop_en(pop_en),
	.top_en(top_en),
	.toss_en(toss_en),
	.swap_en(swap_en),
	.idata(idata_col),
	.odata(odata_col)
    );


endmodule
`endif
