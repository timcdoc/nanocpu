`timescale 1 ns / 10 ps
`include "cpu-const.h"//$PWD"../cpu-const.v"

module stack_and_pointer_tb();
    reg clk = 0;
    reg rst = 0;
    reg push_en = 0;
    reg pop_en = 0;
    reg top_en = 0;
    reg toss_en = 0;
    reg swap_en = 0;
    reg [7:0] idata;
    wire [7:0] odata;
    
    localparam DURATION = 10000;

    always begin
	#41.67
	clk =~clk;
    end
    
    stack_and_pointer uut (
	.clk(clk),
	.rst(rst),
	.push_en(push_en),
	.pop_en(pop_en),
	.top_en(top_en),
	.toss_en(toss_en),
	.swap_en(swap_en),
	.idata(idata),
	.odata(odata)
    );

    initial begin
        #(2*41.67)
        rst=1;
        #(2*41.67)
        rst=0;

	#(2*41.67)
	idata='hDE; push_en=1;
	#(2*41.67)
	idata='h00; push_en=0;

	#(2*41.67)
	idata='hA5; push_en=1;
	#(2*41.67)
	idata='h00; push_en=0;

	#(2*41.67)
	toss_en=1;
	#(2*41.67)
	toss_en=0;

	#(2*41.67)
	swap_en=1;
	#(2*41.67)
	swap_en=0;

	#(2*41.67)
	swap_en=1;
	#(2*41.67)
	swap_en=0;

	#(2*41.67)
	swap_en=1;
	#(2*41.67)
	swap_en=0;

	#(2*41.67)
	pop_en=1;
	#(2*41.67)
	pop_en=0;

    end

    initial begin
	$dumpfile("stacks_tb.vcd");
	$dumpvars(0, stack_and_pointer_tb);
	#(DURATION)
	$display("Finished!");
	$finish;
    end
endmodule
	
