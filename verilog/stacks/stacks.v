`ifndef _STACKS_V_
`define _STACKS_V_
`include "cpu-const.h"//$PWD"../cpu-const.v"

module stack_and_pointer (
    input clk, input rst,
    input push_en,
    input pop_en,
    input top_en,
    input toss_en,
    input swap_en,
    input [7:0] idata,
    output reg [7:0] odata
);

    reg [0:7] current_sp;
    reg [7:0] current_data;
    reg [7:0] stack [0:255];

    always @ (posedge clk or posedge rst) begin
        if ( rst ) begin
            current_sp <= 8'hFF;
            current_data <= 8'h00;
            odata <= 8'h00;
        end else begin
            if ( push_en ) begin
                current_sp <= current_sp+1;
                current_data <= idata;
            end else if ( pop_en ) begin
                current_data <= stack[current_sp];
                odata <= stack[current_sp];
            end else if ( swap_en ) begin
                current_data <= stack[current_sp];
                odata <= stack[current_sp];
                stack[current_sp]<=current_data;
            end else if ( top_en ) begin
                odata <= stack[current_sp];
                current_data <= stack[current_sp];
            end else if ( toss_en ) begin
                current_sp <= current_sp-1;
            end
        end
    end
    always @ (negedge push_en )
        begin
            stack[current_sp] <= current_data;
        end
    always @ (negedge pop_en )
        begin
            current_sp <= current_sp-1;
        end
endmodule
`endif
