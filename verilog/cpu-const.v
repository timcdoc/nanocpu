`ifndef _CPU_CONST_
`define _CPU_CONST_
`define STACK_DEPTH_1 511
`define STACK_DEPTH_BITS_1 8
`define DOWN_DIRECTION 3'b000
`define RIGHT_DIRECTION 3'b001
`define UP_DIRECTION 3'b010
`define LEFT_DIRECTION 3'b011
`define FORE_DIRECTION 3'b100
`define BACK_DIRECTION 3'b101

`define CPU_NOP 'h00
`define CPU_POD 'h01
`define CPU_PUD 'h02
`define CPU_TOD 'h03
`define CPU_QOD 'h04
`define CPU_XD  'h05
`define CPU_XDI 'h06
`define CPU_LDI 'h07
`define CPU_SKP 'h08

`define CPU_POC 'h09
`define CPU_PUC 'h0A
`define CPU_TOC 'h0B
`define CPU_QOC 'h0C
`define CPU_XC  'h0D
`define CPU_XCI 'h0E
`define CPU_LCI 'h0F

`define CPU_DD 'h10
`define CPU_DR 'h11
`define CPU_DU 'h12
`define CPU_DL 'h13
`define CPU_JDD 'h14
`define CPU_JDR 'h15
`define CPU_JDU 'h16
`define CPU_JDL 'h17
`define CPU_CD 'h18
`define CPU_CR 'h19
`define CPU_CU 'h1A
`define CPU_CL 'h1B
`define CPU_JCD 'h1C
`define CPU_JCR 'h1D
`define CPU_JCU 'h1E
`define CPU_JCL 'h1F
`define CPU_HX0 'h20
`define CPU_HX1 'h21
`define CPU_HX2 'h22
`define CPU_HX3 'h23
`define CPU_HX4 'h24
`define CPU_HX5 'h25
`define CPU_HX6 'h26
`define CPU_HX7 'h27
`define CPU_HX8 'h28
`define CPU_HX9 'h29
`define CPU_HXA 'h2A
`define CPU_HXB 'h2B
`define CPU_HXC 'h2C
`define CPU_HXD 'h2D
`define CPU_HXE 'h2E
`define CPU_HXF 'h2F

`define CPU_N1D  'h30 //-
`define CPU_N3D  'h31 //---
`define CPU_N8D  'h32 //b-
`define CPU_N32D 'h33 //d-
`define CPU_P1D  'h34 //+
`define CPU_P3D  'h35 //+++
`define CPU_P8D  'h36 //b+
`define CPU_P32D 'h37 //d+
`define CPU_iN1  'h38 //*-
`define CPU_iN3  'h39 //*---
`define CPU_iN8  'h3A //*b-
`define CPU_iN32 'h3B //*d-

`define CPU_iP1  'h3C //*+
`define CPU_iP3  'h3D //*+++
`define CPU_iP8  'h3E //*b+
`define CPU_iP32 'h3F //*d+
`define CPU_ZP   'h40 //0
`define CPU_1P   'h41 //1
`define CPU_ZNP  'h42 //o
`define CPU_1NP  'h43 //i
`define CPU_iZP  'h44 //*0
`define CPU_i1P  'h45 //*1
`define CPU_iZNP 'h46 //*o
`define CPU_i1NP 'h47 //*i
`define CPU_PZ   'h48 //+o
`define CPU_P1   'h49 //+i
`define CPU_iPZ  'h4A //*+o
`define CPU_iP1  'h4B //*+i
//               'h4C
//               'h4D
`define CPU_PDI  'h4E
`define CPU_PCI  'h4F
`define CPU_ADD  'h50
`define CPU_ADC  'h51
`define CPU_SUB  'h52
`define CPU_SBC  'h53
`define CPU_MUL  'h54
//               'h55
//               'h56
//               'h57
`define CPU_LDA  'h58
`define CPU_LDB  'h59
`define CPU_WRA  'h5A
`define CPU_WRB  'h5B
`define CPU_LIA  'h5C
`define CPU_LIB  'h5D
//           0x5E
//           0x5F
`define CPU_DF   'h60 //This is enough to get started in 3D
`define CPU_DB   'h61
`define CPU_JDF  'h62
`define CPU_JDB  'h63
`define CPU_CF   'h64
`define CPU_CB   'h65
`define CPU_JCF  'h66
`define CPU_JCB  'h67
//           0x68 4D?
//           0x69
//           0x6A
//           0x6B
//           0x6C
//           0x6D
//           0x6E
//           0x6F 4D?
`define CPU_PUSW 'h70 //not impl
`define CPU_POSW 'h71 //not impl
`define CPU_POC2D 'h72
`define CPU_POD2C 'h73
`define CPU_DNM  'h74
`define CPU_DIM  'h75
`define CPU_DSM  'h76
//           0x77
//           0x78
`define CPU_HLT 'h79
`define CPU_WZ  'h7A
`define CPU_W1  'h7B
`define CPU_iWZ 'h7C
`define CPU_iW1 'h7D
//           0x7E
//           0x7F
//           0x80
//           0x81
//           0x82
//           0x83
//           0x84
//           0x85
//           0x86
//           0x87
//           0x88
//           0x89
//           0x8A
//           0x8B
//           0x8C
//           0x8D
//           0x8E
//           0x8F
`define CPU_0DD  'h90 //!
`define CPU_0DR  'h91 //!
`define CPU_0DU  'h92 //!
`define CPU_0DL  'h93 //!
`define CPU_0JDD 'h94 //!
`define CPU_0JDR 'h95 //!
`define CPU_0JDU 'h96 //!
`define CPU_0JDL 'h97 //!
`define CPU_0CD  'h98 //!
`define CPU_0CR  'h99 //!
`define CPU_0CU  'h9A //!
`define CPU_0CL  'h9B //!
`define CPU_0JCD 'h9C //!
`define CPU_0JCR 'h9D //!
`define CPU_0JCU 'h9E //!
`define CPU_0JCL 'h9F //!
//           0xA0
//           0xA1
//           0xA2
//           0xA3
//           0xA4
//           0xA5
//           0xA6
//           0xA7
//           0xA8
//           0xA9
//           0xAA
//           0xAB
//           0xAC
//           0xAD
//           0xAE
//           0xAF
`define CPU_1DD   'hB0 //?
`define CPU_1DR   'hB1 //?
`define CPU_1DU   'hB2 //?
`define CPU_1DL   'hB3 //?
`define CPU_1JDD  'hB4 //?
`define CPU_1JDR  'hB5 //?
`define CPU_1JDU  'hB6 //?
`define CPU_1JDL  'hB7 //?
`define CPU_1CD   'hB8 //?
`define CPU_1CR   'hB9 //?
`define CPU_1CU   'hBA //?
`define CPU_1CL   'hBB //?
`define CPU_1JCD  'hBC //?
`define CPU_1JCR  'hBD //?
`define CPU_1JCU  'hBE //?
`define CPU_1JCL  'hBF //?
//           0xC0
//           0xC1
//           0xC2
//           0xC3
//           0xC4
//           0xC5
//           0xC6
//           0xC7
//           0xC8
//           0xC9
//           0xCA
//           0xCB
//           0xCC
//           0xCD
//           0xCE
//           0xCF
`define CPU_i0DD  'hD0 //*!
`define CPU_i0DR  'hD1 //*!
`define CPU_i0DU  'hD2 //*!
`define CPU_i0DL  'hD3 //*!
`define CPU_i0JDD 'hD4 //*!
`define CPU_i0JDR 'hD5 //*!
`define CPU_i0JDU 'hD6 //*!
`define CPU_i0JDL 'hD7 //*!
`define CPU_i0CD  'hD8 //*!
`define CPU_i0CR  'hD9 //*!
`define CPU_i0CU  'hDA //*!
`define CPU_i0CL  'hDB //*!
`define CPU_i0JCD 'hDC //*!
`define CPU_i0JCR 'hDD //*!
`define CPU_i0JCU 'hDE //*!
`define CPU_i0JCL 'hDF //*!
//           0xE0
//           0xE1
//           0xE2
//           0xE3
//           0xE4
//           0xE5
//           0xE6
//           0xE7
//           0xE8
//           0xE9
//           0xEA
//           0xEB
//           0xEC
//           0xED
//           0xEE
//           0xEF

`define CPU_i1DD  'hF0 //*!
`define CPU_i1DR  'hF1 //*!
`define CPU_i1DU  'hF2 //*!
`define CPU_i1DL  'hF3 //*!
`define CPU_i1JDD 'hF4 //*!
`define CPU_i1JDR 'hF5 //*!
`define CPU_i1JDU 'hF6 //*!
`define CPU_i1JDL 'hF7 //*!
`define CPU_i1CD  'hF8 //*?
`define CPU_i1CR  'hF9 //*?
`define CPU_i1CU  'hFA //*?
`define CPU_i1CL  'hFB //*?
`define CPU_i1JCD 'hFC //*?
`define CPU_i1JCR 'hFD //*?
`define CPU_i1JCU 'hFE //*?
`define CPU_i1JCL 'hFF //*?

`define DIRECT_VAL 2'b00
`define INDIRECT_VAL 2'b01
`define STAR_VAL 2'b10
`define DIRECT_MODE ( data_ptr_mode == `DIRECT_VAL )
`define INDIRECT_MODE ( data_ptr_mode == `INDIRECT_VAL )
`define STAR_MODE ( data_ptr_mode == `STAR_VAL )
`define IDATABIT data_byte[data_col[2:0]]
`define IF_TEST_AND_INDIRECT(test,code) \
    if ( ( `INDIRECT_MODE ) && ( data_byte[idata_col_bits] test ) ) begin \
        code; \
    end else if ( data_byte[data_col_bits] test ) begin \
        code; \
    end
`define IF_ELSE_TEST_AND_INDIRECT(test,ifcode,elsecode) \
    if ( ( `INDIRECT_MODE ) && ( data_byte[idata_col_bits] test ) ) begin \
        ifcode; \
    end else if ( data_byte[data_col_bits] test ) begin \
        ifcode; \
    end else begin \
        elsecode; \
    end
`define HANDLE_DATA_PAG_INDIRECT_P(x) \
    if ( `INDIRECT_MODE ) begin \
        ind_data_pag <= ind_data_pag+x; \
    end else begin \
        data_pag <= data_pag+x; \
    end
`define HANDLE_DATA_PAG_INDIRECT_N(x) \
    if ( `INDIRECT_MODE ) begin \
        ind_data_pag <= ind_data_pag-x; \
    end else begin \
        data_pag <= data_pag-x; \
    end
`define HANDLE_DATA_ROW_INDIRECT_P(x) \
    if ( `INDIRECT_MODE ) begin \
        ind_data_row <= ind_data_row+x; \
    end else begin \
        data_row <= data_row+x; \
    end
`define HANDLE_DATA_ROW_INDIRECT_N(x) \
    if ( `INDIRECT_MODE ) begin \
        ind_data_row <= ind_data_row-x; \
    end else begin \
        data_row <= data_row-x; \
    end
`define HANDLE_DATA_COL_INDIRECT_P(x) \
    if ( `INDIRECT_MODE ) begin \
        data_col <= data_col+x; \
    end else begin \
        data_col <= data_col+x; \
    end
`define HANDLE_DATA_COL_INDIRECT_N(x) \
    if ( `INDIRECT_MODE ) begin \
        data_col <= data_col-x; \
    end else begin \
        data_col <= data_col-x; \
    end
`define JUMP_CODE_DOWN_AND \
        if ( code_direction == `DOWN_DIRECTION ) begin \
            code_row <= code_row+2;\
        end else if ( code_direction == `RIGHT_DIRECTION ) begin\
            code_row <= code_row+1;\
            code_col <= code_col+1;\
        end else if ( code_direction == `LEFT_DIRECTION ) begin\
            code_row <= code_row+1;\
            code_col <= code_col-1;\
        end
`define JUMP_CODE_RIGHT_AND \
        if ( code_direction == `DOWN_DIRECTION ) begin \
            code_col <= code_col+1;\
            code_row <= code_row+1;\
        end else if ( code_direction == `UP_DIRECTION ) begin\
            code_col <= code_col+1;\
            code_row <= code_row-1;\
        end else if ( code_direction == `RIGHT_DIRECTION ) begin\
            code_col <= code_col+2;\
        end
`define JUMP_CODE_UP_AND \
        if ( code_direction == `UP_DIRECTION ) begin \
            code_row <= code_row-2; \
        end else if ( code_direction == `RIGHT_DIRECTION ) begin \
            code_row <= code_row-1; \
            code_col <= code_col+1; \
        end else if ( code_direction == `LEFT_DIRECTION ) begin \
            code_row <= code_row-1; \
            code_col <= code_col-1; \
        end
`define JUMP_CODE_LEFT_AND \
        if ( code_direction == `DOWN_DIRECTION ) begin  \
            code_col <= code_col-1; \
            code_row <= code_row+1; \
        end else if ( code_direction == `UP_DIRECTION ) begin \
            code_row <= code_row-1; \
        end else if ( code_direction == `LEFT_DIRECTION ) begin \
            code_col <= code_col-2; \
        end
`define IF_TEST_AND_INDIRECT(test,code) \
    if ( ( `INDIRECT_MODE ) && ( data_byte[idata_col_bits] test ) ) begin \
        code; \
    end else if ( data_byte[data_col_bits] == instruction[5] ) begin \
        code; \
    end
`define COND_JUMP_CODE_LEFT_AND_INDIRECT(test) \
    if ( ( `INDIRECT_MODE ) && ( data_byte[idata_col_bits] test ) ) begin \
        `JUMP_CODE_LEFT_AND \
    end else begin \
        `GO_CODE_DIRECTION(1) \
    end \
    `SAFE_SET_DATA_RW_STATE(pos_data_rw_state,neg_data_rw_state,`CODE_R_WAIT_STATE);
`define COND_JUMP_CODE_UP_AND_INDIRECT(test) \
    if ( ( `INDIRECT_MODE ) && ( data_byte[idata_col_bits] test ) ) begin \
        `JUMP_CODE_UP_AND \
    end else if ( data_byte[data_col_bits] test ) begin \
        `JUMP_CODE_UP_AND \
    end else begin \
        `GO_CODE_DIRECTION(1) \
    end \
    `SAFE_SET_DATA_RW_STATE(pos_data_rw_state,neg_data_rw_state,`CODE_R_WAIT_STATE);
`define COND_JUMP_CODE_RIGHT_AND_INDIRECT(test) \
    if ( ( `INDIRECT_MODE ) && ( data_byte[idata_col_bits] test ) ) begin \
        `JUMP_CODE_RIGHT_AND \
    end else if ( data_byte[data_col_bits] test ) begin \
        `JUMP_CODE_RIGHT_AND \
    end else begin \
        `GO_CODE_DIRECTION(1) \
    end \
    `SAFE_SET_DATA_RW_STATE(pos_data_rw_state,neg_data_rw_state,`CODE_R_WAIT_STATE);
`define COND_JUMP_CODE_DOWN_AND_INDIRECT(test) \
    if ( ( `INDIRECT_MODE ) && ( data_byte[idata_col_bits] test ) ) begin \
        `JUMP_CODE_DOWN_AND \
    end else if ( data_byte[data_col_bits] test ) begin \
        `JUMP_CODE_DOWN_AND \
    end else begin \
        `GO_CODE_DIRECTION(1) \
    end \
    `SAFE_SET_DATA_RW_STATE(pos_data_rw_state,neg_data_rw_state,`CODE_R_WAIT_STATE);
`define COND_JUMP_CODE_LEFT_AND(test) \
    if ( test ) begin \
        `JUMP_CODE_LEFT_AND \
    end else begin \
        `GO_CODE_DIRECTION(1) \
    end \
    `SAFE_SET_DATA_RW_STATE(pos_data_rw_state,neg_data_rw_state,`CODE_R_WAIT_STATE);
`define COND_JUMP_CODE_UP_AND(test) \
    if ( test ) begin \
        `JUMP_CODE_UP_AND \
    end else begin \
        `GO_CODE_DIRECTION(1) \
    end \
    `SAFE_SET_DATA_RW_STATE(pos_data_rw_state,neg_data_rw_state,`CODE_R_WAIT_STATE);
`define COND_JUMP_CODE_RIGHT_AND(test) \
    if ( test ) begin \
        `JUMP_CODE_RIGHT_AND \
    end else begin \
        `GO_CODE_DIRECTION(1) \
    end \
    `SAFE_SET_DATA_RW_STATE(pos_data_rw_state,neg_data_rw_state,`CODE_R_WAIT_STATE);
`define COND_JUMP_CODE_DOWN_AND(test) \
    if ( test ) begin \
        `JUMP_CODE_DOWN_AND \
    end else begin \
        `GO_CODE_DIRECTION(1) \
    end \
    `SAFE_SET_DATA_RW_STATE(pos_data_rw_state,neg_data_rw_state,`CODE_R_WAIT_STATE);
`define GO_OPPOSITE_CODE_DIRECTION(x) \
    if ( code_direction == `DOWN_DIRECTION ) begin \
        code_row <= code_row+x; \
    end else if ( code_direction == `UP_DIRECTION ) begin \
        code_row <= code_row-x; \
    end else if ( code_direction == `RIGHT_DIRECTION ) begin \
        code_col <= code_col+x; \
    end else if ( code_direction == `LEFT_DIRECTION ) begin \
        code_col <= code_col-x; \
    end
`define GO_CODE_THIS_DIRECTION(d,x) \
    if ( d == `DOWN_DIRECTION ) begin \
        code_row <= code_row+x; \
    end else if ( d == `UP_DIRECTION ) begin \
        code_row <= code_row-x; \
    end else if ( d == `RIGHT_DIRECTION ) begin \
        code_col <= code_col+x; \
    end else if ( d == `LEFT_DIRECTION ) begin \
        code_col <= code_col-x; \
    end
`define GO_CODE_THIS_DIRECTION_AND_DIRECTION(d) \
    if ( d == `DOWN_DIRECTION ) begin \
        if ( code_direction == `DOWN_DIRECTION ) begin \
            code_row <= code_row+2; \
        end else if ( code_direction == `UP_DIRECTION ) begin \
        end else if ( code_direction == `RIGHT_DIRECTION ) begin \
            code_row <= code_row+1; \
            code_col <= code_col+1; \
        end else if ( code_direction == `LEFT_DIRECTION ) begin \
            code_row <= code_row+1; \
            code_col <= code_col-1; \
        end\
    end else if ( d == `UP_DIRECTION ) begin \
        if ( code_direction == `DOWN_DIRECTION ) begin \
        end else if ( code_direction == `UP_DIRECTION ) begin \
            code_row <= code_row-2; \
        end else if ( code_direction == `RIGHT_DIRECTION ) begin \
            code_row <= code_row-1; \
            code_col <= code_col+1; \
        end else if ( code_direction == `LEFT_DIRECTION ) begin \
            code_row <= code_row-1; \
            code_col <= code_col-1; \
        end\
    end else if ( d == `RIGHT_DIRECTION ) begin \
        if ( code_direction == `DOWN_DIRECTION ) begin \
            code_row <= code_row+1; \
            code_col <= code_col+1; \
        end else if ( code_direction == `UP_DIRECTION ) begin \
            code_row <= code_row-1; \
            code_col <= code_col+1; \
        end else if ( code_direction == `RIGHT_DIRECTION ) begin \
            code_col <= code_col+2; \
        end else if ( code_direction == `LEFT_DIRECTION ) begin \
        end\
    end else if ( d == `LEFT_DIRECTION ) begin \
        if ( code_direction == `DOWN_DIRECTION ) begin \
            code_row <= code_row+1; \
            code_col <= code_col-1; \
        end else if ( code_direction == `UP_DIRECTION ) begin \
            code_row <= code_row-1; \
            code_col <= code_col-1; \
        end else if ( code_direction == `RIGHT_DIRECTION ) begin \
        end else if ( code_direction == `LEFT_DIRECTION ) begin \
            code_col <= code_col-2; \
        end\
    end
`define GO_CODE_DIRECTION_FROM_DATA_STACK(x) \
    if ( code_direction == `DOWN_DIRECTION ) begin \
        code_row <= data_stack_row[data_sp]+x; \
        code_col <= data_stack_col[data_sp]; \
    end else if ( code_direction == `UP_DIRECTION ) begin \
        code_row <= data_stack_row[data_sp]-x; \
        code_col <= data_stack_col[data_sp]; \
    end else if ( code_direction == `RIGHT_DIRECTION ) begin \
        code_col <= data_stack_col[data_sp]+x; \
        code_row <= data_stack_row[data_sp]; \
    end else if ( code_direction == `LEFT_DIRECTION ) begin \
        code_row <= data_stack_row[data_sp]; \
        code_col <= data_stack_col[data_sp]-x; \
    end
`define GO_CODE_DIRECTION_FROM_STACK(x) \
    if ( code_direction == `DOWN_DIRECTION ) begin \
        code_row <= code_stack_row[code_sp]+x; \
        code_col <= code_stack_col[code_sp]; \
    end else if ( code_direction == `UP_DIRECTION ) begin \
        code_row <= code_stack_row[code_sp]-x; \
        code_col <= code_stack_col[code_sp]; \
    end else if ( code_direction == `RIGHT_DIRECTION ) begin \
        code_col <= code_stack_col[code_sp]+x; \
        code_row <= code_stack_row[code_sp]; \
    end else if ( code_direction == `LEFT_DIRECTION ) begin \
        code_row <= code_stack_row[code_sp]; \
        code_col <= code_stack_col[code_sp]-x; \
    end
`define GO_CODE_DIRECTION(x) \
    if ( code_direction == `DOWN_DIRECTION ) begin \
        code_row <= code_row+x; \
    end else if ( code_direction == `UP_DIRECTION ) begin \
        code_row <= code_row-x; \
    end else if ( code_direction == `RIGHT_DIRECTION ) begin \
        code_col <= code_col+x; \
    end else if ( code_direction == `LEFT_DIRECTION ) begin \
        code_col <= code_col-x; \
    end
`define GO_OPPOSITE_DATA_DIRECTION(x) \
    if ( data_direction == `DOWN_DIRECTION ) begin \
        `HANDLE_DATA_ROW_INDIRECT_N(x) \
    end else if ( data_direction == `UP_DIRECTION ) begin \
        `HANDLE_DATA_ROW_INDIRECT_P(x) \
    end else if ( data_direction == `RIGHT_DIRECTION ) begin \
        `HANDLE_DATA_COL_INDIRECT_N(x) \
    end else if ( data_direction == `LEFT_DIRECTION ) begin \
        `HANDLE_DATA_COL_INDIRECT_P(x) \
    end
`define GO_DATA_THIS_DIRECTION(d,x) \
    if ( d == `DOWN_DIRECTION ) begin \
        `HANDLE_DATA_ROW_INDIRECT_P(x) \
    end else if ( d == `UP_DIRECTION ) begin \
        `HANDLE_DATA_ROW_INDIRECT_N(x) \
    end else if ( d == `RIGHT_DIRECTION ) begin \
        `HANDLE_DATA_COL_INDIRECT_P(x) \
    end else if ( d == `LEFT_DIRECTION ) begin \
        `HANDLE_DATA_COL_INDIRECT_N(x) \
    end
`define GO_DATA_DIRECTION(x) \
    if ( data_direction == `DOWN_DIRECTION ) begin \
        `HANDLE_DATA_ROW_INDIRECT_P(x) \
    end else if ( data_direction == `UP_DIRECTION ) begin \
        `HANDLE_DATA_ROW_INDIRECT_N(x) \
    end else if ( data_direction == `RIGHT_DIRECTION ) begin \
        `HANDLE_DATA_COL_INDIRECT_P(x) \
    end else if ( data_direction == `LEFT_DIRECTION ) begin \
        `HANDLE_DATA_COL_INDIRECT_N(x) \
    end
`define TWO_USTEP(first,second) \
    if ( ustep == 'b000 ) begin \
        first \
        ustep <= 'b001; \
    end else if ( ustep == 'b001 ) begin \
        second \
        ustep <= 'b000; \
    end
`endif
