`ifndef _MEMORY_V_
`define _MEMORY_V_
module memory #( parameter INIT_FILE="" ) (
    input clk,
`ifdef MAKE_CODE_TEST
    input rc_en, input [`MEM_ROWS_BITS_1:0] crow, input [`MEM_COLS_BITS_1:0] ccol,
	    output reg [`CODE_BITS_1:0] rc_data
`ifdef MAKE_DATA_TEST
    ,
`endif
`endif
`ifdef MAKE_DATA_TEST
    input rd_en, input [`MEM_ROWS_BITS_1:0] drow, input [`MEM_COLS_BITS_1:0] dcol,
	    input [`BYTES_BITS_1:0] dsubcol, output reg rd_byte, rd_highbyte, output reg rd_bit
`endif
);

    reg [`CODE_BITS_1:0] mem [0:`MEM_BYTES_1];
`ifdef MAKE_CODE_TEST
    always @ (posedge clk) begin
        if (rc_en == 1'b1) begin
            rc_data <= mem[{crow,ccol}];
        end
    end
`endif
`ifdef MAKE_DATA_TEST
    reg [7:0] dcolplusone;
    always @ (posedge clk) begin
        if (rd_en == 1'b1) begin
            rd_byte <= mem[{drow,dcol}];
            dcolplusone<=dcol+1
        end
    end
    always @ (negedge clk) begin
        if (rd_en == 1'b1) begin
            rd_bit <= rd_data[dsubcol];
            rd_highbyte <= mem[{drow,dcolplusone}];
        end
    end
`endif

    initial if (INIT_FILE) begin
    $readmemh(INIT_FILE, mem);
    end
endmodule
`endif
