//These are not yet needed functions.  I will probably prune out the ones not needed badly.
`define Fn_PUD CPU_PUD: begin\
    data_sp <= data_sp + 1;\
    data_push_en <= 1;\
    `GO_CODE_DIRECTION(1)\
    `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
end
`define Fn_TOD CPU_TOD: begin\
    data_row <= data_stack_row[data_sp];\
    data_col <= data_stack_col[data_sp];\
    `GO_CODE_DIRECTION(1)\
    `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)\
end
`define Fn_QOD CPU_QOD: begin\
    data_sp <= data_sp - 1;\
    `GO_CODE_DIRECTION(1)\
    `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)\
end
`define Fn_XD CPU_XD: begin\
    tmp_row <= data_stack_row[data_sp];\
    tmp_col <= data_stack_col[data_sp];\
    data_stack_row[data_sp] <= data_row;\
    data_stack_col[data_sp] <= data_col;\
    `SAFE_SET_DATA_RW_STATE(neg_data_rw_state,pos_data_rw_state,`DATA_R_WAIT_STATE);\
    data_swap_en <= 1;\
    `GO_CODE_DIRECTION(1)\
    `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)\
end
`define CPU_LDI: begin\
    if ( ustep == 3'b000 ) begin\
      if ( code_r_state == `CODE_R_IDLE_STATE ) begin\
        data_row<={nibble[1],nibble[0]};\
        data_col<={nibble[3],nibble[2]};\
        inibble <= 2'b00;\
        `GO_CODE_DIRECTION(1)\
        data_rw_state <= `DATA_R_STATE;\
        code_r_state <= `CODE_R_STATE;\
      end\
    end\
end
`define Fn_ CPU_XC: begin
    code_swap_en <= 1;
    `GO_CODE_DIRECTION(1)
    `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
    end
    // Needs a new macro for * indirect
           `CPU_i0DD, `CPU_i0DR, `CPU_i0DU, `CPU_i0DL: begin
    if ( `IDATABIT == 0 ) data_direction <= instruction[1:0];
    `GO_CODE_DIRECTION(1)
    end
           `CPU_i1DD, `CPU_i1DR, `CPU_i1DU, `CPU_i1DL: begin
    if ( `IDATABIT == 1 ) data_direction <= instruction[1:0];
    `GO_CODE_DIRECTION(1)
    end
//`CPU_i0CD, `CPU_i0CR, `CPU_i0CU, `CPU_i0CL,
//            end else if (instruction == `CPU_iN1) begin
//            end else if (instruction == `CPU_iP1) begin
`define Fn_ CPU_i0JDD, `CPU_i1JDD: begin
    if ( !`IDATABIT == instruction[5] ) begin
        `HANDLE_DATA_ROW_INDIRECT_P(1)
    end
    `GO_CODE_DIRECTION(1)
    end
`define Fn_ CPU_i0JDR, `CPU_i1JDR: begin
    if ( !`IDATABIT == instruction[5] ) begin
        `HANDLE_DATA_COL_INDIRECT_P(1)
    end
    `GO_CODE_DIRECTION(1)
    end
`define Fn_ CPU_i0JDU, `CPU_i1JDU: begin
    if ( !`IDATABIT == instruction[5] ) begin
        `HANDLE_DATA_ROW_INDIRECT_N(1)
    end
    `GO_CODE_DIRECTION(1)
    end
`define Fn_ CPU_i0JDL, `CPU_i1JDL: begin
    if ( !`IDATABIT == instruction[5] ) begin
        `HANDLE_DATA_COL_INDIRECT_N(1)
    end
    `GO_CODE_DIRECTION(1)
    end
`define Fn_ CPU_i0JCD, `CPU_i1JCD: begin
    `COND_JUMP_CODE_DOWN_AND(`IDATABIT == instruction[5])
    end
`define Fn_ CPU_i0JCR, `CPU_i1JCR: begin
    `COND_JUMP_CODE_RIGHT_AND(!`IDATABIT == instruction[5])
    end
`define Fn_ CPU_i0JCU, `CPU_i1JCU: begin
    `COND_JUMP_CODE_UP_AND(!`IDATABIT == instruction[5])
    end
`define Fn_ CPU_i0JCL, `CPU_i1JCL: begin
    `COND_JUMP_CODE_LEFT_AND(!`IDATABIT == instruction[5])
    end
`define Fn_ CPU_i0CD, `CPU_i1CD, `CPU_i0CR, `CPU_i1CR, `CPU_i0CU, `CPU_i1CU, `CPU_i0CL, `CPU_i1CL: begin
    if ( `IDATABIT == instruction[5] ) code_direction <= instruction[1:0];
    `GO_CODE_DIRECTION(1)
    end
`define Fn_ CPU_0DD, `CPU_1DD, `CPU_0DR, `CPU_1DR, `CPU_0DU, `CPU_1DU, `CPU_0DL, `CPU_1DL: begin
    `IF_TEST_AND_INDIRECT(==instruction[5], data_direction<=instruction[1:0] )
    `GO_CODE_DIRECTION(1)
    end
`define Fn_ CPU_P8D: begin
		`GO_DATA_DIRECTION(8)
    `GO_CODE_DIRECTION(1)
    end
`define Fn_ CPU_N8D: begin
		`GO_OPPOSITE_DATA_DIRECTION(8)
    `GO_CODE_DIRECTION(1)
    end
`define Fn_ CPU_P32D: begin
		`GO_DATA_DIRECTION(32)
    `GO_CODE_DIRECTION(1)
    end
`define Fn_ CPU_N32D: begin
		`GO_OPPOSITE_DATA_DIRECTION(32)
    `GO_CODE_DIRECTION(1)
    end

`define Fn_ CPU_WZ: begin
    `IF_TEST_AND_INDIRECT(==0, `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE));
    end
`define Fn_ CPU_W1: begin
    `IF_TEST_AND_INDIRECT(==1, `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE));
    end
`define Fn_ CPU_iWZ: begin
    if (`IDATABIT == 0 ) begin
        `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
        end
    end
`define Fn_ CPU_iW1: begin
    if (`IDATABIT == 1 ) begin
        `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
        end
    end
`define Fn_ CPU_PCI: begin
    if ( ustep == 3'b000 ) begin
      if ( code_r_state == `CODE_R_IDLE_STATE ) begin
        code_sp <= code_sp + 1;
        `GO_CODE_DIRECTION(1)
        ustep <= 3'b001;
      end
    end
    end
`define Fn_ CPU_LIA: begin
    register_a[15:8] <= {nibble[0],nibble[1]};
    register_a[7:0] <= {nibble[2],nibble[3]};
    `GO_CODE_DIRECTION(1)
    `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
    end
`define Fn_ CPU_LIB: begin
    register_b[15:8] <= {nibble[0],nibble[1]};
    register_b[7:0] <= {nibble[2],nibble[3]};
    `GO_CODE_DIRECTION(1)
    `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
    end
`define Fn_ CPU_LDA: begin
    if ( ustep == 'b000 ) begin
        register_a[15:8] <= data_byte;
        `GO_DATA_DIRECTION(8)
        ustep <= 'b001;
    end else if ( ustep == 'b001 ) begin
        register_a[7:0] <= data_byte;
        `GO_OPPOSITE_DATA_DIRECTION(8)
        `GO_CODE_DIRECTION(1)
        `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
        ustep <= 'b000;
    end
    end
`define Fn_ CPU_LDB: begin
    if ( ustep == 'b000 ) begin
        register_b[15:8] <= data_byte;
        `GO_DATA_DIRECTION(8)
        ustep <= 'b001;
    end else if ( ustep == 'b001 ) begin
        register_b[7:0] <= data_byte;
        `GO_OPPOSITE_DATA_DIRECTION(8)
        `GO_CODE_DIRECTION(1)
        `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
        ustep <= 'b000;
    end
    end
`define Fn_ CPU_WRA: begin
    if ( ustep == 'b000 ) begin
        data_byte<=register_a[15:8];
        ustep <= 'b001;
    end else if ( ustep == 'b001 ) begin
        data_byte<=register_a[7:0];
        `GO_CODE_DIRECTION(1)
        `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
        ustep <= 'b000;
    end
    end
`define Fn_ CPU_WRB: begin
    if ( ustep == 'b000 ) begin
        data_byte<=register_b[15:8];
        ustep <= 'b001;
    end else if ( ustep == 'b001 ) begin
        data_byte<=register_b[7:0];
        `GO_CODE_DIRECTION(1)
        `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
        ustep <= 'b000;
    end
    end
`define Fn_ CPU_ZP: begin
    `SET_BIT_IN_BYTE(0)
    `SAFE_SET_DATA_RW_STATE(neg_data_rw_state,pos_data_rw_state,`DATA_W_WAIT_STATE);
		udatanext <= 1;
    `GO_CODE_DIRECTION(1)
    `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
    end
`define Fn_ CPU_1P: begin
    `SET_BIT_IN_BYTE(1)
    `SAFE_SET_DATA_RW_STATE(neg_data_rw_state,pos_data_rw_state,`DATA_W_WAIT_STATE);
		udatanext <= 1;
    `GO_CODE_DIRECTION(1)
    `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
    end
`define Fn_ CPU_PZ: begin
    if ( ustep == 'b000 ) begin
        `GO_DATA_DIRECTION(1)
        ustep <= 'b001;
    end else if ( ustep == 'b001 ) begin
        `SET_BIT_IN_BYTE(0)
        `SAFE_SET_DATA_RW_STATE(neg_data_rw_state,pos_data_rw_state,`DATA_W_WAIT_STATE);
        `GO_CODE_DIRECTION(1)
        `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
        ustep <= 'b000;
    end
    end
`define Fn_ CPU_ADD: begin
    register_dw[16:0]<=register_a+register_b;
    umathregister <= 1;
    `GO_CODE_DIRECTION(1)
    `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
    end
`define Fn_ CPU_ADC: begin
    register_dw[16:0]<=register_a+register_b+register_dw[16];
    umathregister <= 1;
    `GO_CODE_DIRECTION(1)
    `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
    end
`define Fn_ CPU_SUB: begin
    //carry<=1;//Not right? BUGBUG
    register_dw[16:0]<=register_a-register_b;
    umathregister <= 1;
    `GO_CODE_DIRECTION(1)
    `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
    end
`define Fn_ CPU_SBC: begin
    //carry<=1; //This isn't right, need to review BUGBUG
    register_dw[16:0]<=register_a-register_b+~register_dw[16];
    umathregister <= 1;
    `GO_CODE_DIRECTION(1)
    `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
    end
`define Fn_ CPU_MUL: begin
    register_dw<=register_a*register_b;
    umathregister <= 1;
    `GO_CODE_DIRECTION(1)
    `SAFE_SET_CODE_R_STATE(neg_code_r_state,pos_code_r_state,`CODE_R_WAIT_STATE)
    end

