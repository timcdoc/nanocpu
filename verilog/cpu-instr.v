`define Fn_NOP_and_default default: begin \
    if ( ustep == 3'b000 ) begin \
        `GO_CODE_DIRECTION(1) \
        code_read_en <= 1; \
        ustep <= 3'b001; \
    end else if ( ustep == 3'b111 ) begin \
        `GO_CODE_DIRECTION(1) \
        code_read_en <= 1; \
        data_read_en <= 1; \
        ustep <= 3'b110; \
    end else if ( ustep == 3'b101 ) begin \
        ustep <= 3'b000; \
    end else if ( ustep == 3'b001 ) begin \
        code_row <= code_row; \
        code_col <= code_col; \
        code_read_en <= 0; \
        data_read_en <= 0; \
        ustep <= 3'b000; \
    end else if ( ustep == 3'b110 ) begin \
        code_row <= code_row; \
        code_col <= code_col; \
        code_read_en <= 0; \
        data_read_en <= 0; \
        ustep <= 3'b101; \
    end \
end
`define Fn_HX_ \
    `CPU_HX0, `CPU_HX1, `CPU_HX2, `CPU_HX3, `CPU_HX4, `CPU_HX5, `CPU_HX6, `CPU_HX7,\
    `CPU_HX8, `CPU_HX9, `CPU_HXA, `CPU_HXB, `CPU_HXC, `CPU_HXD, `CPU_HXE, `CPU_HXF: begin\
        if ( ustep == 3'b000 ) begin\
            `GO_CODE_DIRECTION(1)\
            nibble[inibble] <= ('b1111&instruction);\
            code_read_en <= 1; \
            ustep <= 3'b001;\
        end else if ( ustep == 3'b001 ) begin\
            code_read_en <= 0; \
            inibble <= inibble+1;\
            ustep <= 3'b000; \
        end\
    end
`define Fn_PUSW `CPU_PUSW: begin \
    if ( ustep == 3'b000 ) begin \
        data_sp <= data_sp+1;\
        ustep <= 3'b001; \
    end else if ( ustep == 3'b001 ) begin \
        data_stack_col[data_sp] <= {data_ptr_mode,code_direction,data_direction}; \
        data_stack_row[data_sp] <= data_row; \
        data_stack_pag[data_sp] <= data_pag; \
        data_sp <= data_sp+1;\
        ustep <= 3'b010; \
    end else if ( ustep == 3'b010 ) begin \
        data_stack_col[data_sp] <= {nibble[1],nibble[0]}; \
        data_stack_row[data_sp] <= {nibble[3],nibble[2]}; \
        data_stack_pag[data_sp] <= {nibble[5],nibble[4]}; \
        ustep <= 3'b011; \
    end else if ( ustep == 3'b011 ) begin \
        data_stack_col[data_sp] <= register_dw[7:0]; \
        data_stack_row[data_sp] <= register_dw[15:8]; \
        data_stack_pag[data_sp] <= register_dw[23:16]; \
        data_sp <= data_sp+1;\
        ustep <= 3'b100; \
    end else if ( ustep == 3'b100 ) begin \
        data_stack_col[data_sp] <= register_dw[31:24]; \
        data_stack_row[data_sp] <= register_a[7:0]; \
        data_stack_pag[data_sp] <= register_a[15:8]; \
        data_sp <= data_sp+1;\
        ustep <= 3'b101; \
    end else if ( ustep == 3'b101 ) begin \
        data_stack_col[data_sp] <= register_b[7:0]; \
        data_stack_row[data_sp] <= register_b[15:8]; \
        data_stack_pag[data_sp] <= 0; \
        `GO_CODE_DIRECTION(1)\
        code_read_en <= 1; \
        ustep <= 3'b110; \
    end else if ( ustep == 3'b110 ) begin \
        code_read_en <= 0; \
        ustep <= 3'b000; \
    end \
end
`define Fn_POSW `CPU_POSW: begin \
    if ( ustep == 3'b000 ) begin \
        register_b[7:0] <= data_stack_col[data_sp]; \
        register_b[15:8] <= data_stack_row[data_sp]; \
        data_sp <= data_sp-1;\
        ustep <= 3'b001; \
    end else if ( ustep == 3'b001 ) begin \
        register_dw[31:24] <= data_stack_col[data_sp]; \
        register_a[7:0] <= data_stack_row[data_sp]; \
        register_a[15:8] <= data_stack_pag[data_sp]; \
        data_sp <= data_sp-1;\
        ustep <= 3'b010; \
    end else if ( ustep == 3'b010 ) begin \
        register_dw[7:0] <= data_stack_col[data_sp]; \
        register_dw[15:8] <= data_stack_row[data_sp]; \
        register_dw[23:16] <= data_stack_pag[data_sp]; \
        data_sp <= data_sp-1;\
        ustep <= 3'b011; \
    end else if ( ustep == 3'b011 ) begin \
        {nibble[1],nibble[0],nibble[3],nibble[2],nibble[5],nibble[4]} <= \
            {data_stack_col[data_sp],data_stack_row[data_sp],data_stack_pag[data_sp]}; \
        data_sp <= data_sp-1;\
        ustep <= 3'b100; \
    end else if ( ustep == 3'b100 ) begin \
        {data_ptr_mode,code_direction,data_direction} <= data_stack_col[data_sp]; \
        `GO_CODE_DIRECTION(1)\
        code_read_en <= 1; \
        ustep <= 3'b101; \
    end else if ( ustep == 3'b101 ) begin \
        code_read_en <= 0; \
        ustep <= 3'b000; \
    end \
end
`define Fn_PUD `CPU_PUD: begin \
    if ( ustep == 3'b000 ) begin \
        data_sp <= data_sp+1;\
        ustep <= 3'b001; \
    end else if ( ustep == 3'b001 ) begin \
        data_stack_col[data_sp] <= data_col; \
        data_stack_row[data_sp] <= data_row; \
        data_stack_pag[data_sp] <= data_pag; \
        `GO_CODE_DIRECTION(1)\
        code_read_en <= 1; \
        ustep <= 3'b010; \
    end else if ( ustep == 3'b010 ) begin \
        code_read_en <= 0; \
        ustep <= 3'b000; \
    end \
end
`define Fn_PUC `CPU_PUC: begin \
    if ( ustep == 3'b000 ) begin \
        code_sp <= code_sp+1;\
        ustep <= 3'b001; \
    end else if ( ustep == 3'b001 ) begin \
        code_stack_col[code_sp] <= code_col; \
        code_stack_row[code_sp] <= code_row; \
        code_stack_pag[code_sp] <= code_pag; \
        `GO_CODE_DIRECTION(1)\
        code_read_en <= 1; \
        ustep <= 3'b010; \
    end else if ( ustep == 3'b010 ) begin \
        code_read_en <= 0; \
        ustep <= 3'b000; \
    end \
end
`define Fn_PDI `CPU_PDI: begin\
    if ( ustep == 3'b000 ) begin\
        data_sp <= data_sp + 1;\
        ustep <= 3'b001;\
    end else if ( ustep == 3'b001 ) begin \
        data_stack_row[data_sp]<={nibble[1],nibble[0]};\
        data_stack_col[data_sp]<={nibble[3],nibble[2]};\
        data_stack_pag[data_sp]<={nibble[5],nibble[4]};\
        `GO_CODE_DIRECTION(1)\
        code_read_en <= 1; \
        inibble <= 3'b000;\
        ustep <= 3'b010; \
    end else if ( ustep == 3'b010 ) begin \
        code_read_en <= 0; \
        ustep <= 3'b000; \
    end \
end

`define Fn_LCI `CPU_LCI: begin\
    if ( ustep == 3'b000 ) begin\
        code_row<={nibble[1],nibble[0]};\
        code_col<={nibble[3],nibble[2]};\
        code_pag<={nibble[5],nibble[4]};\
        inibble <= 3'b000;\
        code_read_en <= 1; \
        ustep <= 3'b001;\
    end else if ( ustep == 3'b001 ) begin \
        code_read_en <= 0; \
        ustep <= 3'b000; \
    end \
end

`define Fn_POC2D `CPU_POC2D: begin\
    if ( ustep == 'b000 ) begin\
        data_row <= code_stack_row[code_sp];\
        data_col <= code_stack_col[code_sp];\
        data_pag <= code_stack_pag[code_sp];\
        `GO_CODE_DIRECTION(1)\
        code_read_en <= 1; \
        data_read_en <= 1; \
        ustep <= 'b001;\
    end else if ( ustep == 3'b001 ) begin\
        code_sp <= code_sp - 1;\
        data_read_en <= 0; \
        code_read_en <= 0; \
        ustep <= 'b000;\
    end\
end
`define Fn_POD `CPU_POD: begin\
    if ( ustep == 'b000 ) begin\
        data_row <= data_stack_row[data_sp];\
        data_col <= data_stack_col[data_sp];\
        data_pag <= data_stack_pag[data_sp];\
        `GO_CODE_DIRECTION(1)\
        code_read_en <= 1; \
        data_read_en <= 1; \
        ustep <= 'b001;\
    end else if ( ustep == 3'b001 ) begin\
        data_sp <= data_sp - 1;\
        data_read_en <= 0; \
        code_read_en <= 0; \
        ustep <= 'b000;\
    end\
end
`define Fn_DNM `CPU_DNM: begin\
    if ( ustep == 3'b000 ) begin\
        data_ptr_mode <= `DIRECT_VAL;\
        `GO_CODE_DIRECTION(1)\
        code_read_en <= 1; \
        ustep <= 3'b001;\
    end else if ( ustep == 3'b001 ) begin\
        code_read_en <= 0; \
        ustep <= 3'b000;\
    end\
end
`define Fn_DIM `CPU_DIM: begin\
    if ( ustep == 3'b000 ) begin\
        data_ptr_mode <= `INDIRECT_VAL;\
        `GO_CODE_DIRECTION(1)\
        code_read_en <= 1; \
        ustep <= 3'b001;\
    end else if ( ustep == 3'b001 ) begin\
        code_read_en <= 0; \
        ustep <= 3'b000;\
    end\
end
`define Fn_DSM `CPU_DSM: begin\
    if ( ustep == 3'b000 ) begin\
        data_ptr_mode <= `STAR_VAL;\
        `GO_CODE_DIRECTION(1)\
        code_read_en <= 1; \
        ustep <= 3'b001;\
    end else if ( ustep == 3'b001 ) begin\
        code_read_en <= 0; \
        ustep <= 3'b000;\
    end\
end
`define Fn_CF `CPU_CF: begin\
    if ( ustep == 3'b000 ) begin\
        code_direction <= `FORE_DIRECTION;\
        code_pag <= code_pag+1;\
        code_read_en <= 1; \
        ustep <= 3'b001;\
    end else if ( ustep == 3'b001 ) begin\
        code_read_en <= 0; \
        ustep <= 3'b000;\
    end\
end
`define Fn_CB `CPU_CB: begin\
    if ( ustep == 3'b000 ) begin\
        code_direction <= `BACK_DIRECTION;\
        code_pag <= code_pag-1;\
        code_read_en <= 1; \
        ustep <= 3'b001;\
    end else if ( ustep == 3'b001 ) begin\
        code_read_en <= 0; \
        ustep <= 3'b000;\
    end\
end
`define Fn_CD `CPU_CD, `CPU_CR, `CPU_CU, `CPU_CL: begin\
    if ( ustep == 3'b000 ) begin\
        code_direction <= instruction[1:0];\
        if ( instruction[1:0] == `DOWN_DIRECTION ) begin\
            code_row <= code_row+1;\
        end else if ( instruction[1:0] == `UP_DIRECTION ) begin\
            code_row <= code_row-1;\
        end else if ( instruction[1:0] == `RIGHT_DIRECTION ) begin\
            code_col <= code_col+1;\
        end else if ( instruction[1:0] == `LEFT_DIRECTION ) begin\
            code_col <= code_col-1;\
        end\
        code_read_en <= 1; \
        ustep <= 3'b001;\
    end else if ( ustep == 3'b001 ) begin\
        code_read_en <= 0; \
        ustep <= 3'b000;\
    end\
end
// -
`define Fn_N1D `CPU_N1D: begin\
    if ( ustep == 3'b000 ) begin\
        `GO_OPPOSITE_DATA_DIRECTION(1) \
        `GO_CODE_DIRECTION(1)\
        data_read_en <= 1;\
        code_read_en <= 1;\
        ustep <= 3'b001;\
    end else if ( ustep == 'b001 ) begin\
        data_read_en <= 0;\
        code_read_en <= 0;\
        ustep <= 3'b000;\
    end\
end
// i o
`define Fn_1NP `CPU_1NP, `CPU_ZNP: begin\
    if ( ustep == 3'b000 ) begin\
        `GO_CODE_DIRECTION(1)\
        code_read_en <= 1;\
        if ( instruction[0] ) begin\
            data_bit_set_en <= 1;\
        end else begin\
            data_bit_clear_en <= 1;\
        end \
        data_write_en <= 1;\
        ustep <= 3'b001;\
    end else if ( ustep == 'b001 ) begin\
        data_bit_set_en <= 0;\
        data_bit_clear_en <= 0;\
        data_write_en <= 0;\
        code_read_en <= 0;\
        ustep <= 3'b000;\
    end\
end
// +
`define Fn_P1D `CPU_P1D: begin\
    if ( ustep == 3'b000 ) begin\
        `GO_DATA_DIRECTION(1) \
        `GO_CODE_DIRECTION(1)\
        data_read_en <= 1;\
        code_read_en <= 1;\
        ustep <= 3'b001;\
    end else if ( ustep == 'b001 ) begin\
        data_read_en <= 0;\
        code_read_en <= 0;\
        ustep <= 3'b000;\
    end\
end
`define Fn_N3D `CPU_N3D: begin\
    if ( ustep == 3'b000 ) begin\
        `GO_OPPOSITE_DATA_DIRECTION(3) \
        `GO_CODE_DIRECTION(1)\
        data_read_en <= 1;\
        code_read_en <= 1;\
        ustep <= 3'b001;\
    end else if ( ustep == 'b001 ) begin\
        data_read_en <= 0;\
        code_read_en <= 0;\
        ustep <= 3'b000;\
    end\
end
`define Fn_P3D `CPU_P3D: begin\
    if ( ustep == 3'b000 ) begin\
        `GO_DATA_DIRECTION(3) \
        `GO_CODE_DIRECTION(1)\
        data_read_en <= 1;\
        code_read_en <= 1;\
        ustep <= 3'b001;\
    end else if ( ustep == 'b001 ) begin\
        data_read_en <= 0;\
        code_read_en <= 0;\
        ustep <= 3'b000;\
    end\
end
`define Fn_1CU `CPU_0CU, `CPU_0CL, `CPU_0CD, `CPU_0CR, `CPU_1CU, `CPU_1CL, `CPU_1CD, `CPU_1CR: begin\
    if ( ustep == 3'b000 ) begin\
        if ( data_pag == 8'hFF ) begin \
            if ( data_byte[idata_col_bits] == instruction[5] ) begin \
                code_direction<=instruction[1:0];\
                `GO_CODE_THIS_DIRECTION(instruction[1:0],1) \
            end else begin \
               `GO_CODE_DIRECTION(1)\
            end\
        end else begin\
            if ( data_byte[data_col_bits] == instruction[5] ) begin \
                code_direction<=instruction[1:0];\
                `GO_CODE_THIS_DIRECTION(instruction[1:0],1) \
            end else begin \
                `GO_CODE_DIRECTION(1)\
            end\
        end\
        code_read_en <= 1;\
        ustep <= 3'b001;\
    end else if ( ustep == 'b001 ) begin\
        code_read_en <= 0;\
        ustep <= 3'b000;\
    end\
end
`define Fn_HLT `CPU_HLT: begin\
    if ( ustep == 3'b000 ) begin\
        `GO_OPPOSITE_CODE_DIRECTION(1) \
        ustep <= 3'b111;\
    end\
end
`define Fn_DF `CPU_DF: begin\
    if ( ustep == 3'b000 ) begin\
        `GO_CODE_DIRECTION(1) \
        data_direction<=`FORE_DIRECTION;\
        code_read_en <= 1;\
        ustep <= 3'b001;\
    end else if ( ustep == 'b001 ) begin\
        code_read_en <= 0;\
        ustep <= 3'b000;\
    end\
end
`define Fn_DB `CPU_DB: begin\
    if ( ustep == 3'b000 ) begin\
        `GO_CODE_DIRECTION(1) \
        data_direction<=`BACK_DIRECTION;\
        code_read_en <= 1;\
        ustep <= 3'b001;\
    end else if ( ustep == 'b001 ) begin\
        code_read_en <= 0;\
        ustep <= 3'b000;\
    end\
end
`define Fn_DU `CPU_DD, `CPU_DR, `CPU_DU, `CPU_DL: begin\
    if ( ustep == 3'b000 ) begin\
        `GO_CODE_DIRECTION(1) \
        data_direction<=instruction[1:0];\
        code_read_en <= 1;\
        ustep <= 3'b001;\
    end else if ( ustep == 'b001 ) begin\
        code_read_en <= 0;\
        ustep <= 3'b000;\
    end\
end
`define Fn_JDL `CPU_JDD, `CPU_JDR, `CPU_JDU, `CPU_JDL: begin\
    if ( ustep == 3'b000 ) begin\
        if ( instruction[1:0] == `DOWN_DIRECTION ) begin\
            `HANDLE_DATA_ROW_INDIRECT_P(1)\
        end else if ( instruction[1:0] == `RIGHT_DIRECTION ) begin\
            `HANDLE_DATA_COL_INDIRECT_P(1)\
        end else if ( instruction[1:0] == `UP_DIRECTION ) begin\
            `HANDLE_DATA_ROW_INDIRECT_N(1)\
        end else if ( instruction[1:0] == `LEFT_DIRECTION ) begin\
            `HANDLE_DATA_COL_INDIRECT_N(1)\
        end\
        `GO_CODE_DIRECTION(1)\
        code_read_en <= 1;\
        data_read_en <= 1;\
        ustep <= 3'b001;\
    end else if ( ustep == 'b001 ) begin\
        code_read_en <= 0;\
        data_read_en <= 0;\
        ustep <= 3'b000;\
    end\
end
`define Fn_0JDL `CPU_0JDD, `CPU_1JDD, `CPU_0JDR, `CPU_1JDR, `CPU_0JDU, `CPU_1JDU, `CPU_0JDL, `CPU_1JDL: begin\
    if ( ustep == 3'b000 ) begin\
        if ( data_pag == 8'hFF ) begin \
            if ( data_byte[idata_col_bits] == instruction[5] ) begin \
                `GO_DATA_THIS_DIRECTION(instruction[1:0],1) \
                data_read_en <= 1;\
            end\
        end else begin\
            if ( data_byte[data_col_bits] == instruction[5] ) begin \
                `GO_DATA_THIS_DIRECTION(instruction[1:0],1) \
                data_read_en <= 1;\
            end\
        end\
        `GO_CODE_DIRECTION(1)\
        code_read_en <= 1;\
        ustep <= 3'b001;\
    end else if ( ustep == 'b001 ) begin\
        code_read_en <= 0;\
        data_read_en <= 0;\
        ustep <= 3'b000;\
    end\
end
`define Fn_JCL `CPU_JCD, `CPU_JCR, `CPU_JCU, `CPU_JCL: begin\
    if ( ustep == 3'b000 ) begin\
        `GO_CODE_THIS_DIRECTION_AND_DIRECTION(instruction[1:0]) \
        code_read_en <= 1;\
        ustep <= 3'b001;\
    end else if ( ustep == 'b001 ) begin\
        code_read_en <= 0;\
        ustep <= 3'b000;\
    end\
end
`define Fn_0JCL `CPU_0JCD, `CPU_1JCD, `CPU_0JCR, `CPU_1JCR, `CPU_0JCU, `CPU_1JCU, `CPU_0JCL, `CPU_1JCL: begin\
    if ( ustep == 3'b000 ) begin\
        if ( data_pag == 8'hFF ) begin \
            if ( data_byte[idata_col_bits] == instruction[5] ) begin \
                `GO_CODE_THIS_DIRECTION_AND_DIRECTION(instruction[1:0]) \
            end else begin \
                `GO_CODE_DIRECTION(1)\
            end\
        end else begin\
            if ( data_byte[data_col_bits] == instruction[5] ) begin \
                `GO_CODE_THIS_DIRECTION_AND_DIRECTION(instruction[1:0]) \
            end else begin \
                `GO_CODE_DIRECTION(1)\
            end\
        end\
        code_read_en <= 1;\
        ustep <= 3'b001;\
    end else if ( ustep == 'b001 ) begin\
        code_read_en <= 0;\
        ustep <= 3'b000;\
    end\
end
// +i +o
`define Fn_P1 `CPU_P1, `CPU_PZ: begin\
    if ( ustep == 'b000 ) begin\
        `GO_DATA_DIRECTION(1)\
        data_read_en <= 1;\
        ustep <= 'b001;\
    end else if ( ustep == 'b001 ) begin\
        data_read_en <= 0;\
        ustep <= 'b010;\
    end else if ( ustep == 'b010 ) begin\
        `GO_CODE_DIRECTION(1)\
        if ( instruction[0] ) begin\
            data_bit_set_en <= 1;\
        end else begin\
            data_bit_clear_en <= 1;\
        end \
        code_read_en <= 1;\
        data_write_en <= 1;\
        ustep <= 'b011;\
    end else if ( ustep == 'b011 ) begin\
        code_read_en <= 0;\
        data_write_en <= 0;\
        data_bit_set_en <= 0;\
        data_bit_clear_en <= 0;\
        ustep <= 'b000;\
    end\
end
`define Fn_QOC `CPU_QOC: begin\
    if ( ustep == 'b000 ) begin\
        `GO_CODE_DIRECTION(1)\
        code_sp <= code_sp - 1;\
        code_read_en <= 1;\
        ustep <= 'b001;\
    end else if ( ustep == 'b001 ) begin\
        code_read_en <= 0;\
        ustep <= 'b001;\
    end\
end
`define Fn_POD2C `CPU_POD2C: begin\
    if ( ustep == 'b000 ) begin\
        `GO_CODE_DIRECTION_FROM_DATA_STACK(1)\
        data_sp <= data_sp - 1;\
        code_read_en <= 1;\
        ustep <= 'b001;\
    end else if ( ustep == 'b001 ) begin\
        code_read_en <= 0;\
        ustep <= 'b000;\
    end\
end
`define Fn_POC `CPU_POC: begin\
    if ( ustep == 'b000 ) begin\
        `GO_CODE_DIRECTION_FROM_STACK(1)\
        code_sp <= code_sp - 1;\
        code_read_en <= 1;\
        ustep <= 'b001;\
    end else if ( ustep == 'b001 ) begin\
        code_read_en <= 0;\
        ustep <= 'b000;\
    end\
end
`define Fn_TOC `CPU_TOC: begin\
    if ( ustep == 'b000 ) begin\
        `GO_CODE_DIRECTION_FROM_STACK(1)\
        code_read_en <= 1;\
        ustep <= 'b001;\
    end else if ( ustep == 'b001 ) begin\
        code_read_en <= 0;\
        ustep <= 'b000;\
    end\
end
