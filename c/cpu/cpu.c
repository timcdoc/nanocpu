#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include "../cpu-const.h"
#include "../cpu-instr.h"
#include "mne.h"

char hexstr[16]="0123456789ABCDEF";
unsigned char memory_data[16384];
unsigned char memory_code[16384];

#define toupper(x) (((x>='a')&&(x<='z'))?(x-'a'+'A'):x)

void writememh( char *fn, uint8_t *pv, uint32_t start, uint32_t stop )
{
    FILE *stream;
    int i,len;
    len = (stop-start);
    if ( stream = fopen( fn, "w" ) )
        {
        for ( i = 0; i < len; i++ )
            {
            if ( (i%16) == 0 )
                {
                fprintf( stream, "// 0x%8.8x\n", i );
                }
            fprintf( stream, "%2.2x\n", pv[i+start] );
            }
        }
    fclose(stream);
}
void readmemh( char *fn, uint8_t *pv )
{
    int i;
    int oi;
    int digit;
    uint8_t val;
    int len;
    FILE *stream;
    char lbuffer[256];
    char *psz;
    if ( stream = fopen( fn, "r" ) )
        {
        oi=0;
        while ( fgets( lbuffer, sizeof(lbuffer), stream ) && !feof( stream ) )
            {
            len=strlen(lbuffer);
            digit=0;
            for ( i = 0; i < len; i++ )
                {
                if ( psz = strchr(hexstr,toupper(lbuffer[i])) )
                    {
                    if ( digit == 0 )
                        {
                        val = (psz-hexstr);
                        digit++;
                        }
                    else
                        {
                        val *= 16;
                        val += (psz-hexstr);
                        pv[oi]=val;
                        oi++;
                        digit=0;
                        }
                    }
                }
            }
        }
    fclose(stream);
}

int main( int argc, char *argv[] )
{
    uint32_t clk = 0;
    uint8_t rst = 0;
    uint8_t data_byte;
    uint8_t instruction;
    uint8_t data_push_en;
    uint8_t data_swap_en;
    uint8_t code_pop_en;
    uint8_t code_swap_en;
    uint8_t code_row;
    uint8_t code_col;
    uint8_t code_pag;
    uint8_t ind_data_pag; //This is needed, don't know what for yet, once we turn on pages!
    uint8_t ind_data_row;
    uint8_t data_row;
    uint8_t data_col;
    uint8_t data_pag;
    uint8_t code_direction;
    uint8_t data_direction;
    uint8_t udatanext;
    uint8_t umathregister;
    uint8_t code_read_en;
    uint8_t code_write_en;
    uint8_t data_read_en;
    uint8_t data_write_en;
    uint8_t data_bit_set_en;
    uint8_t data_bit_clear_en;
    uint8_t ustep;
    uint8_t data_ptr_mode;
    uint8_t inibble;
    uint8_t nibble[6];
    uint32_t register_dw;
    uint16_t register_a;
    uint16_t register_b;

    uint8_t tmp_row;
    uint8_t tmp_col;
    uint8_t tmp_pag;
    uint8_t code_sp;
    uint8_t data_sp;
    uint8_t code_stack_row[256];
    uint8_t code_stack_col[256];
    uint8_t code_stack_pag[256];
    uint8_t data_stack_row[256];
    uint8_t data_stack_col[256];
    uint8_t data_stack_pag[256];
    uint8_t idsp_addr;
    uint8_t idata_row;
    uint8_t idata_col;
    uint8_t idata_pag;
    uint8_t idata_col_bits;
    uint8_t data_col_bits;
    uint16_t data_address;
    uint16_t code_address;
    uint8_t data_read_byte;
    uint8_t code_read_byte;

    // A reasonable start for a reset.
    data_direction = RIGHT_DIRECTION;
    code_direction = DOWN_DIRECTION;
    code_row = 0xFF;
    code_col = 0x00;
    code_pag = 0x00;
    code_read_en = 0;
    data_read_en = 0;
    data_bit_set_en = 0;
    data_bit_clear_en = 0;
    ind_data_pag = 0x00;
    ind_data_row = 0x00;
    idata_pag = 0x00;
    data_row = 0x00;
    data_col = 0x00;
    data_pag = 0x00;
    code_sp = 0xFF;
    data_sp = 0xFF;
    data_write_en = 0;
    code_write_en = 0;
    umathregister = 0;
    register_dw = 0;
    register_a = 0;
    register_b = 0;
    data_byte = 0;
    instruction = CPU_NOP;
    data_ptr_mode = DIRECT_VAL;
    data_push_en = 0;
    data_swap_en = 0;
    code_pop_en = 0;
    code_swap_en = 0;
    //ustep = 7;
    ustep = 0;
    inibble = 0;

    //Initial
    readmemh(argv[3], code_stack_row);
    readmemh(argv[3], code_stack_col);
    readmemh(argv[3], code_stack_pag);
    readmemh(argv[3], data_stack_row);
    readmemh(argv[3], data_stack_col);
    readmemh(argv[3], data_stack_pag);
    readmemh(argv[2], memory_data);
    readmemh(argv[1], memory_code);

    for ( clk=0; clk <= 190000*4/42; clk++ )
        {
        if ( ustep == 0 ) { instruction = code_read_byte; }
        if ( data_bit_set_en )
            {
            if ( INDIRECT_MODE() ) { data_byte |= (1<<idata_col_bits); }
            else { data_byte |= (1<<data_col_bits); }
            }
        else if ( data_bit_clear_en )
            {
            if ( INDIRECT_MODE() ) { data_byte &= (0xFF&(0xFF^(1<<idata_col_bits))); }
            else { data_byte &= (0xFF&(0xFF^(1<<data_col_bits))); }
            }
        code_address = ((0x3F&code_row)<<7)|(0x3F&code_col);
        if ( code_read_en && POSEDGE(clk) ) { code_read_byte = memory_code[code_address]; }
        data_col_bits = (7-(7&data_col));
        idsp_addr = 0xFF&(data_sp+ind_data_row);
        idata_col_bits = (7-(7&idata_col));
        idata_col = 0xFF&(data_col+data_stack_col[idsp_addr]);
        idata_row = data_stack_row[idsp_addr];
        idata_pag = data_stack_pag[idsp_addr];
        if ( INDIRECT_MODE() ) { data_address = ((1&idata_pag)<<14)|(idata_row<<5)|(idata_col>>3); }
        else { data_address = ((1&data_pag)<<14)|(data_row<<5)|(data_col>>3); }
        if ( data_read_en && POSEDGE(clk) ) { data_byte = data_read_byte = memory_data[data_address]; }
        if ( data_write_en && POSEDGE(clk) )
            {
            memory_data[data_address] = data_byte;
            }

        if ( POSEDGE(clk) )
            {
            if ( ustep == 0 )
                {
                printf( "%4.4x:%6.6s %2.2x{%2.2x} "
                    "{%4.4x} [%2.2x:%2.2x:%2.2x.%2.2x]"
                    "i[%2.2x:%2.2x:%2.2x.%2.2x]"
                    " \n", code_address, mne[instruction], data_byte, data_read_byte, data_address,
                    data_row, data_col, data_pag, data_col_bits,
                    idata_row, idata_col, idata_pag, idata_col_bits
                   );
                }
            switch ( instruction )
                {
            Fn_POD
            Fn_PUC
            Fn_POC
            Fn_SKP
            //Fn_POD2C
            //Fn_POC2D
            Fn_TOC
            Fn_QOC
            Fn_HX_
            Fn_LCI
            Fn_DF
            Fn_DB
            Fn_DU //etc
            Fn_JDL //etc
            Fn_0JDL //etc
            Fn_CD
            Fn_CF
            Fn_CB
            Fn_JCL //etc
            Fn_0JCL //etc
            Fn_P1D
            Fn_N1D
            Fn_P3D
            Fn_N3D
            Fn_PDI
            Fn_1CU
            Fn_P1 //etc
            Fn_1NP //etc
            Fn_DNM
            Fn_DIM
            Fn_DSM
            Fn_PUSW
            //Fn_POSW
            Fn_HLT
            Fn_NOP_and_default
                }
            }
        }


    writememh(argv[4], memory_data, 0, 16384);
    return(0);
}
