#define Fn_SKP case CPU_SKP:\
    if ( ustep == 0 ) {\
        GO_CODE_DIRECTION(2) \
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0;\
        ustep = 0;\
    }\
    break;
#define Fn_NOP_and_default default:\
    if ( ustep == 0 ) {\
        GO_CODE_DIRECTION(1) \
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0;\
        ustep = 0;\
    }\
    break;
#if 0
#define Fn_NOP_and_default default:\
    if ( ustep == 0 ) {\
        GO_CODE_DIRECTION(1) \
        code_read_en = 1; \
        ustep = 1; \
    }else if ( ustep == 7 ) {\
        GO_CODE_DIRECTION(1) \
        code_read_en = 1; \
        data_read_en = 1; \
        ustep = 6; \
    }else if ( ustep == 5 ) {\
        ustep = 0; \
    }else if ( ustep == 1 ) {\
        code_row = code_row; \
        code_col = code_col; \
        code_read_en = 0; \
        data_read_en = 0; \
        ustep = 0; \
    }else if ( ustep == 6 ) {\
        code_row = code_row; \
        code_col = code_col; \
        code_read_en = 0; \
        data_read_en = 0; \
        ustep = 5; \
    }\
    break;
#endif
#define Fn_HX_ \
   case CPU_HX0:case CPU_HX1:case CPU_HX2:case CPU_HX3:case CPU_HX4:case CPU_HX5:case CPU_HX6:case CPU_HX7:\
   case CPU_HX8:case CPU_HX9:case CPU_HXA:case CPU_HXB:case CPU_HXC:case CPU_HXD:case CPU_HXE:case CPU_HXF:\
        if ( ustep == 0 ) {\
            GO_CODE_DIRECTION(1)\
            nibble[inibble] = (0xF&instruction);\
            code_read_en = 1; \
            ustep = 1;\
        }else if ( ustep == 1 ) {\
            code_read_en = 0; \
            inibble = inibble+1;\
            ustep = 0; \
        }\
    break;
#define Fn_PUSW case CPU_PUSW:\
    if ( ustep == 0 ) { \
        data_sp = data_sp+1;\
        ustep = 1; \
    } else if ( ustep == 1 ) { \
        data_stack_col[data_sp] = data_ptr_mode*64+code_direction*8+data_direction;\
        data_sp = data_sp+1;\
        ustep = 2; \
    } else if ( ustep == 2 ) { \
        data_stack_col[data_sp] = nibble[1]*16+nibble[0];\
        data_stack_row[data_sp] = nibble[3]*16+nibble[2];\
        data_stack_pag[data_sp] = nibble[5]*16+nibble[4];\
        ustep = 3; \
    } else if ( ustep == 3 ) { \
        data_stack_col[data_sp] = (register_dw%256);\
        data_stack_row[data_sp] = ((register_dw/256)%256);\
        data_stack_pag[data_sp] = ((register_dw/65536)%256);\
        data_sp = data_sp+1;\
        ustep = 4; \
    } else if ( ustep == 4 ) { \
        data_stack_col[data_sp] = ((register_dw/(256*65536))%256);\
        data_stack_row[data_sp] = (register_a%256);\
        data_stack_pag[data_sp] = ((register_a/256)%256);\
        data_sp = data_sp+1;\
        ustep = 5; \
    } else if ( ustep == 5 ) { \
        data_stack_col[data_sp] = (register_b%256);\
        data_stack_row[data_sp] = ((register_b/256)%256);\
        data_stack_pag[data_sp] = 0; \
        GO_CODE_DIRECTION(1)\
        code_read_en = 1; \
        ustep = 6; \
    } else if ( ustep == 6 ) { \
        code_read_en = 0; \
        ustep = 0; \
    } \
    break;
#define Fn_PUC case CPU_PUC:\
    if ( ustep == 0 ) {\
        code_sp = code_sp+1;\
        ustep = 1; \
    }else if ( ustep == 1 ) {\
        code_stack_col[code_sp] = code_col; \
        code_stack_row[code_sp] = code_row; \
        code_stack_pag[code_sp] = code_pag; \
        GO_CODE_DIRECTION(1)\
        code_read_en = 1; \
        ustep = 2; \
    }else if ( ustep == 2 ) {\
        code_read_en = 0; \
        ustep = 0; \
    }\
    break;
#define Fn_PDI case CPU_PDI: \
    if ( ustep == 0 ) {\
        data_sp = data_sp + 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        data_stack_row[data_sp]=(16*nibble[1]+nibble[0]);\
        data_stack_col[data_sp]=(16*nibble[3]+nibble[2]);\
        data_stack_pag[data_sp]=(16*nibble[5]+nibble[4]);\
        GO_CODE_DIRECTION(1)\
        code_read_en = 1; \
        inibble = 0;\
        ustep = 2; \
    }else if ( ustep == 2 ) {\
        code_read_en = 0; \
        ustep = 0; \
    }\
    break;

#define Fn_LCI case CPU_LCI:\
    if ( ustep == 0 ) {\
        code_row=(16*nibble[1]+nibble[0]);\
        code_col=(16*nibble[3]+nibble[2]);\
        code_pag=(16*nibble[5]+nibble[4]);\
        inibble = 0;\
        code_read_en = 1; \
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0; \
        ustep = 0; \
    }\
    break;

#define Fn_POD case CPU_POD:\
    if ( ustep == 0 ) {\
        data_row = data_stack_row[data_sp];\
        data_col = data_stack_col[data_sp];\
        data_pag = data_stack_pag[data_sp];\
        GO_CODE_DIRECTION(1)\
        code_read_en = 1; \
        data_read_en = 1; \
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        data_sp = data_sp - 1;\
        code_read_en = 0; \
        data_read_en = 0; \
        ustep = 0;\
    }\
    break;
#define Fn_DNM case CPU_DNM: \
    if ( ustep == 0 ) {\
        data_ptr_mode = DIRECT_VAL;\
        GO_CODE_DIRECTION(1)\
        code_read_en = 1; \
        ustep = 1;\
    } else if ( ustep == 1 ) {\
        code_read_en = 0; \
        ustep = 0;\
    }\
    break;
#define Fn_DIM case CPU_DIM:\
    if ( ustep == 0 ) {\
        data_ptr_mode = INDIRECT_VAL;\
        GO_CODE_DIRECTION(1)\
        code_read_en = 1; \
        ustep = 1;\
    } else if ( ustep == 1 ) {\
        code_read_en = 0; \
        ustep = 0;\
    }\
    break;
#define Fn_DSM case CPU_DSM:\
    if ( ustep == 0 ) {\
        data_ptr_mode = STAR_VAL;\
        GO_CODE_DIRECTION(1)\
        code_read_en = 1; \
        ustep = 1;\
    } else if ( ustep == 1 ) {\
        code_read_en = 0; \
        ustep = 0;\
    }\
    break;
#define Fn_CF case CPU_CF:\
    if ( ustep == 0 ) {\
        code_direction = FORE_DIRECTION;\
        code_pag = code_pag+1;\
        code_read_en = 1; \
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0; \
        ustep = 0;\
    }\
    break;
#define Fn_CB case CPU_CB:\
    if ( ustep == 0 ) {\
        code_direction = BACK_DIRECTION;\
        code_pag = code_pag-1;\
        code_read_en = 1; \
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0; \
        ustep = 0;\
    }\
    break;
#define Fn_CD case CPU_CD:case CPU_CR:case CPU_CU:case CPU_CL:\
    if ( ustep == 0 ) {\
        code_direction = (3&instruction);\
        if ( (3&instruction) == DOWN_DIRECTION ) {\
            code_row = code_row+1;\
        }else if ( (3&instruction) == UP_DIRECTION ) {\
            code_row = code_row-1;\
        }else if ( (3&instruction) == RIGHT_DIRECTION ) {\
            code_col = code_col+1;\
        }else if ( (3&instruction) == LEFT_DIRECTION ) {\
            code_col = code_col-1;\
        }\
        code_read_en = 1; \
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0; \
        ustep = 0;\
    }\
    break;
// -
#define Fn_N1D case CPU_N1D:\
    if ( ustep == 0 ) {\
        GO_OPPOSITE_DATA_DIRECTION(1) \
        GO_CODE_DIRECTION(1)\
        data_read_en = 1;\
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        data_read_en = 0;\
        code_read_en = 0;\
        ustep = 0;\
    }\
    break;
// i o
#define Fn_1NP case CPU_1NP:case CPU_ZNP:\
    if ( ustep == 0 ) {\
        GO_CODE_DIRECTION(1)\
        code_read_en = 1;\
        if ( (1&instruction) ) {\
            data_bit_set_en = 1;\
        }else {\
            data_bit_clear_en = 1;\
        }\
        data_write_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        data_bit_set_en = 0;\
        data_bit_clear_en = 0;\
        data_write_en = 0;\
        code_read_en = 0;\
        ustep = 0;\
    }\
    break;
// +
#define Fn_P1D case CPU_P1D:\
    if ( ustep == 0 ) {\
        GO_DATA_DIRECTION(1) \
        GO_CODE_DIRECTION(1)\
        data_read_en = 1;\
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        data_read_en = 0;\
        code_read_en = 0;\
        ustep = 0;\
    }\
    break;
#define Fn_N3D case CPU_N3D:\
    if ( ustep == 0 ) {\
        GO_OPPOSITE_DATA_DIRECTION(3) \
        GO_CODE_DIRECTION(1)\
        data_read_en = 1;\
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        data_read_en = 0;\
        code_read_en = 0;\
        ustep = 0;\
    }\
    break;
#define Fn_P3D case CPU_P3D:\
    if ( ustep == 0 ) {\
        GO_DATA_DIRECTION(3) \
        GO_CODE_DIRECTION(1)\
        data_read_en = 1;\
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        data_read_en = 0;\
        code_read_en = 0;\
        ustep = 0;\
    }\
    break;
#define Fn_1CU case CPU_0CU:case CPU_0CL:case CPU_0CD:case CPU_0CR:case CPU_1CU:case CPU_1CL:case CPU_1CD:case CPU_1CR:\
    if ( ustep == 0 ) {\
        if ( data_pag == 0xFF ) {\
            if ( TEST_DATA_BIT( idata_col_bits, == (1&(instruction>>5)) ) ) {\
                code_direction=(3&instruction);\
                GO_CODE_THIS_DIRECTION((3&instruction),1) \
            }else {\
               GO_CODE_DIRECTION(1)\
            }\
        }else {\
            if ( TEST_DATA_BIT( data_col_bits, == (1&(instruction>>5)) ) ) {\
                code_direction=(3&instruction);\
                GO_CODE_THIS_DIRECTION((3&instruction),1) \
            }else {\
                GO_CODE_DIRECTION(1)\
            }\
        }\
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0;\
        ustep = 0;\
    }\
    break;
#define Fn_HLT case CPU_HLT:\
    if ( ustep == 0 ) {\
        GO_OPPOSITE_CODE_DIRECTION(1) \
        ustep = 7;\
    }\
    break;
#define Fn_DF case CPU_DF:\
    if ( ustep == 0 ) {\
        GO_CODE_DIRECTION(1) \
        data_direction=FORE_DIRECTION;\
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0;\
        ustep = 0;\
    }\
    break;
#define Fn_DB case CPU_DB:\
    if ( ustep == 0 ) {\
        GO_CODE_DIRECTION(1) \
        data_direction=BACK_DIRECTION;\
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0;\
        ustep = 0;\
    }\
    break;
#define Fn_DU case CPU_DD:case CPU_DR:case CPU_DU:case CPU_DL:\
    if ( ustep == 0 ) {\
        GO_CODE_DIRECTION(1) \
        data_direction=(3&instruction);\
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0;\
        ustep = 0;\
    }\
    break;
#define Fn_JDL case CPU_JDD:case CPU_JDR:case CPU_JDU:case CPU_JDL:\
    if ( ustep == 0 ) {\
        if ( (3&instruction) == DOWN_DIRECTION ) {\
            HANDLE_DATA_ROW_INDIRECT_P(1)\
        }else if ( (3&instruction) == RIGHT_DIRECTION ) {\
            HANDLE_DATA_COL_INDIRECT_P(1)\
        }else if ( (3&instruction) == UP_DIRECTION ) {\
            HANDLE_DATA_ROW_INDIRECT_N(1)\
        }else if ( (3&instruction) == LEFT_DIRECTION ) {\
            HANDLE_DATA_COL_INDIRECT_N(1)\
        }\
        GO_CODE_DIRECTION(1)\
        code_read_en = 1;\
        data_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0;\
        data_read_en = 0;\
        ustep = 0;\
    }\
    break;
#define Fn_0JDL case CPU_0JDD:case CPU_1JDD:case CPU_0JDR:case CPU_1JDR:case CPU_0JDU:case CPU_1JDU:case CPU_0JDL:case CPU_1JDL:\
    if ( ustep == 0 ) {\
        if ( data_pag == 0xFF ) {\
            if ( TEST_DATA_BIT(idata_col_bits, == (1&(instruction>>5)) ) ) {\
                GO_DATA_THIS_DIRECTION((3&instruction),1) \
                data_read_en = 1;\
            }\
        }else {\
            if ( TEST_DATA_BIT(data_col_bits, == (1&(instruction>>5)) ) ) {\
                GO_DATA_THIS_DIRECTION((3&instruction),1) \
                data_read_en = 1;\
            }\
        }\
        GO_CODE_DIRECTION(1)\
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0;\
        data_read_en = 0;\
        ustep = 0;\
    }\
    break;
#define Fn_JCL case CPU_JCD:case CPU_JCR:case CPU_JCU:case CPU_JCL:\
    if ( ustep == 0 ) {\
        GO_CODE_THIS_DIRECTION_AND_DIRECTION((3&instruction)) \
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0;\
        ustep = 0;\
    }\
    break;
#define Fn_0JCL case CPU_0JCD:case CPU_1JCD:case CPU_0JCR:case CPU_1JCR:case CPU_0JCU:case CPU_1JCU:case CPU_0JCL:case CPU_1JCL:\
    if ( ustep == 0 ) {\
        if ( data_pag == 0xFF ) {\
            if ( TEST_DATA_BIT(idata_col_bits, == (1&(instruction>>5)) ) ) {\
                GO_CODE_THIS_DIRECTION_AND_DIRECTION((3&instruction)) \
            }else {\
                GO_CODE_DIRECTION(1)\
            }\
        }else {\
            if ( TEST_DATA_BIT(idata_col_bits, == (1&(instruction>>5)) ) ) {\
                GO_CODE_THIS_DIRECTION_AND_DIRECTION((3&instruction)) \
            }else {\
                GO_CODE_DIRECTION(1)\
            }\
        }\
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0;\
        ustep = 0;\
    }\
    break;
// +i +o
#define Fn_P1 case CPU_P1:case CPU_PZ:\
    if ( ustep == 0 ) {\
        GO_DATA_DIRECTION(1)\
        data_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        data_read_en = 0;\
        ustep = 2;\
    }else if ( ustep == 2 ) {\
        GO_CODE_DIRECTION(1)\
        if ( (1&instruction) ) {\
            data_bit_set_en = 1;\
        }else {\
            data_bit_clear_en = 1;\
        }\
        code_read_en = 1;\
        data_write_en = 1;\
        ustep = 3;\
    }else if ( ustep == 3 ) {\
        code_read_en = 0;\
        data_write_en = 0;\
        data_bit_set_en = 0;\
        data_bit_clear_en = 0;\
        ustep = 0;\
    }\
    break;
#define Fn_QOC case CPU_QOC:\
    if ( ustep == 0 ) {\
        GO_CODE_DIRECTION(1)\
        code_sp = code_sp - 1;\
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0;\
        ustep = 1;\
    }\
    break;
#define Fn_POC case CPU_POC:\
    if ( ustep == 0 ) {\
        GO_CODE_DIRECTION_FROM_STACK(1)\
        code_sp = code_sp - 1;\
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0;\
        ustep = 0;\
    }\
    break;
#define Fn_TOC case CPU_TOC:\
    if ( ustep == 0 ) {\
        GO_CODE_DIRECTION_FROM_STACK(1)\
        code_read_en = 1;\
        ustep = 1;\
    }else if ( ustep == 1 ) {\
        code_read_en = 0;\
        ustep = 0;\
    }\
    break;
