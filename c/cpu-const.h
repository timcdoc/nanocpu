#ifndef _CPU_CONST_
#define _CPU_CONST_
#define STACK_DEPTH_1 511
#define STACK_DEPTH_BITS_1 8
#define DOWN_DIRECTION 0
#define RIGHT_DIRECTION 1
#define UP_DIRECTION 2
#define LEFT_DIRECTION 3
#define FORE_DIRECTION 4
#define BACK_DIRECTION 5

#define CPU_NOP 0x00

#define CPU_POD 0x01
#define CPU_PUD 0x02
#define CPU_TOD 0x03
#define CPU_QOD 0x04
#define CPU_XD  0x05
#define CPU_XDI 0x06
#define CPU_LDI 0x07
#define CPU_SKP 0x08

#define CPU_POC 0x09
#define CPU_PUC 0x0A
#define CPU_TOC 0x0B
#define CPU_QOC 0x0C
#define CPU_XC  0x0D
#define CPU_XCI 0x0E
#define CPU_LCI 0x0F

#define CPU_DD 0x10
#define CPU_DR 0x11
#define CPU_DU 0x12
#define CPU_DL 0x13
#define CPU_JDD 0x14
#define CPU_JDR 0x15
#define CPU_JDU 0x16
#define CPU_JDL 0x17
#define CPU_CD 0x18
#define CPU_CR 0x19
#define CPU_CU 0x1A
#define CPU_CL 0x1B
#define CPU_JCD 0x1C
#define CPU_JCR 0x1D
#define CPU_JCU 0x1E
#define CPU_JCL 0x1F
#define CPU_HX0 0x20
#define CPU_HX1 0x21
#define CPU_HX2 0x22
#define CPU_HX3 0x23
#define CPU_HX4 0x24
#define CPU_HX5 0x25
#define CPU_HX6 0x26
#define CPU_HX7 0x27
#define CPU_HX8 0x28
#define CPU_HX9 0x29
#define CPU_HXA 0x2A
#define CPU_HXB 0x2B
#define CPU_HXC 0x2C
#define CPU_HXD 0x2D
#define CPU_HXE 0x2E
#define CPU_HXF 0x2F

#define CPU_N1D  0x30 //-
#define CPU_N3D  0x31 //---
#define CPU_N8D  0x32 //b-
#define CPU_N32D 0x33 //d-
#define CPU_P1D  0x34 //+
#define CPU_P3D  0x35 //+++
#define CPU_P8D  0x36 //b+
#define CPU_P32D 0x37 //d+
#define CPU_siN1  0x38 //*-
#define CPU_siN3  0x39 //*---
#define CPU_siN8  0x3A //*b-
#define CPU_siN32 0x3B //*d-

#define CPU_siP1  0x3C //*+
#define CPU_siP3  0x3D //*+++
#define CPU_siP8  0x3E //*b+
#define CPU_siP32 0x3F //*d+
#define CPU_ZP   0x40 //0
#define CPU_1P   0x41 //1
#define CPU_ZNP  0x42 //o
#define CPU_1NP  0x43 //i
#define CPU_siZP  0x44 //*0
#define CPU_si1P  0x45 //*1
#define CPU_siZNP 0x46 //*o
#define CPU_si1NP 0x47 //*i
#define CPU_PZ   0x48 //+o
#define CPU_P1   0x49 //+i
#define CPU_siPZNP  0x4A //*+o
#define CPU_siP1NP  0x4B //*+i
//               0x4C
//               0x4D
#define CPU_PDI  0x4E
#define CPU_PCI  0x4F
#define CPU_ADD  0x50
#define CPU_ADC  0x51
#define CPU_SUB  0x52
#define CPU_SBC  0x53
#define CPU_MUL  0x54
//               0x55
//               0x56
//               0x57
#define CPU_LDA  0x58
#define CPU_LDB  0x59
#define CPU_WRA  0x5A
#define CPU_WRB  0x5B
#define CPU_LIA  0x5C
#define CPU_LIB  0x5D
//           0x5E
//           0x5F
#define CPU_DF   0x60 //This is enough to get started in 3D
#define CPU_DB   0x61
#define CPU_JDF  0x62
#define CPU_JDB  0x63
#define CPU_CF   0x64
#define CPU_CB   0x65
#define CPU_JCF  0x66
#define CPU_JCB  0x67
//           0x61
//           0x62
//           0x63
//           0x64
//           0x65
//           0x66
//           0x67
//           0x68
//           0x69
//           0x6A
//           0x6B
//           0x6C
//           0x6D
//           0x6E
//           0x6F
#define CPU_PUSW 0x70
#define CPU_POSW 0x71
#define CPU_POC2D 0x72
#define CPU_POD2C 0x73
#define CPU_DNM  0x74
#define CPU_DIM  0x75
#define CPU_DSM  0x76
//           0x70
//           0x71
//           0x72
//           0x73
//           0x74
//           0x75
//           0x76
//           0x77
//           0x78
#define CPU_HLT 0x79
#define CPU_WZ  0x7A
#define CPU_W1  0x7B
#define CPU_iWZ 0x7C
#define CPU_iW1 0x7D
//           0x7E
//           0x7F
//           0x80
//           0x81
//           0x82
//           0x83
//           0x84
//           0x85
//           0x86
//           0x87
//           0x88
//           0x89
//           0x8A
//           0x8B
//           0x8C
//           0x8D
//           0x8E
//           0x8F
#define CPU_0DD  0x90 //!
#define CPU_0DR  0x91 //!
#define CPU_0DU  0x92 //!
#define CPU_0DL  0x93 //!
#define CPU_0JDD 0x94 //!
#define CPU_0JDR 0x95 //!
#define CPU_0JDU 0x96 //!
#define CPU_0JDL 0x97 //!
#define CPU_0CD  0x98 //!
#define CPU_0CR  0x99 //!
#define CPU_0CU  0x9A //!
#define CPU_0CL  0x9B //!
#define CPU_0JCD 0x9C //!
#define CPU_0JCR 0x9D //!
#define CPU_0JCU 0x9E //!
#define CPU_0JCL 0x9F //!
//           0xA0
//           0xA1
//           0xA2
//           0xA3
//           0xA4
//           0xA5
//           0xA6
//           0xA7
//           0xA8
//           0xA9
//           0xAA
//           0xAB
//           0xAC
//           0xAD
//           0xAE
//           0xAF
#define CPU_1DD   0xB0 //?
#define CPU_1DR   0xB1 //?
#define CPU_1DU   0xB2 //?
#define CPU_1DL   0xB3 //?
#define CPU_1JDD  0xB4 //?
#define CPU_1JDR  0xB5 //?
#define CPU_1JDU  0xB6 //?
#define CPU_1JDL  0xB7 //?
#define CPU_1CD   0xB8 //?
#define CPU_1CR   0xB9 //?
#define CPU_1CU   0xBA //?
#define CPU_1CL   0xBB //?
#define CPU_1JCD  0xBC //?
#define CPU_1JCR  0xBD //?
#define CPU_1JCU  0xBE //?
#define CPU_1JCL  0xBF //?
//           0xC0
//           0xC1
//           0xC2
//           0xC3
//           0xC4
//           0xC5
//           0xC6
//           0xC7
//           0xC8
//           0xC9
//           0xCA
//           0xCB
//           0xCC
//           0xCD
//           0xCE
//           0xCF
#define CPU_i0DD  0xD0 //*!
#define CPU_i0DR  0xD1 //*!
#define CPU_i0DU  0xD2 //*!
#define CPU_i0DL  0xD3 //*!
#define CPU_i0JDD 0xD4 //*!
#define CPU_i0JDR 0xD5 //*!
#define CPU_i0JDU 0xD6 //*!
#define CPU_i0JDL 0xD7 //*!
#define CPU_i0CD  0xD8 //*!
#define CPU_i0CR  0xD9 //*!
#define CPU_i0CU  0xDA //*!
#define CPU_i0CL  0xDB //*!
#define CPU_i0JCD 0xDC //*!
#define CPU_i0JCR 0xDD //*!
#define CPU_i0JCU 0xDE //*!
#define CPU_i0JCL 0xDF //*!
//           0xE0
//           0xE1
//           0xE2
//           0xE3
//           0xE4
//           0xE5
//           0xE6
//           0xE7
//           0xE8
//           0xE9
//           0xEA
//           0xEB
//           0xEC
//           0xED
//           0xEE
//           0xEF

#define CPU_i1DD  0xF0 //*!
#define CPU_i1DR  0xF1 //*!
#define CPU_i1DU  0xF2 //*!
#define CPU_i1DL  0xF3 //*!
#define CPU_i1JDD 0xF4 //*!
#define CPU_i1JDR 0xF5 //*!
#define CPU_i1JDU 0xF6 //*!
#define CPU_i1JDL 0xF7 //*!
#define CPU_i1CD  0xF8 //*?
#define CPU_i1CR  0xF9 //*?
#define CPU_i1CU  0xFA //*?
#define CPU_i1CL  0xFB //*?
#define CPU_i1JCD 0xFC //*?
#define CPU_i1JCR 0xFD //*?
#define CPU_i1JCU 0xFE //*?
#define CPU_i1JCL 0xFF //*?

#define TEST_DATA_BIT(bits,test) (((data_byte>>bits)&1) test)
#define DIRECT_VAL 0
#define INDIRECT_VAL 1
#define STAR_VAL 2
#define INDIRECT_MODE() ( data_ptr_mode == INDIRECT_VAL )
#define IDATABIT data_byte[data_col[2:0]]
#define IF_TEST_AND_INDIRECT(test,code) \
    if ( ( INDIRECT_MODE() ) && ( TEST_DATA_BIT(idata_col_bits, test ) ) ){\
        code; \
    }else if ( TEST_DATA_BIT(data_col_bits, test ) ){\
        code; \
    }
#define IF_ELSE_TEST_AND_INDIRECT(test,ifcode,elsecode) \
    if ( ( INDIRECT_MODE() ) && ( TEST_DATA_BIT(idata_col_bits, test ) ) ){\
        ifcode; \
    }else if ( TEST_DATA_BIT(data_col_bits, test ) ){\
        ifcode; \
    }else{\
        elsecode; \
    }
#define HANDLE_DATA_PAG_INDIRECT_P(x) \
    if ( INDIRECT_MODE() ){\
        ind_data_pag = ind_data_pag+x; \
    }else{\
        data_pag = data_pag+x; \
    }
#define HANDLE_DATA_PAG_INDIRECT_N(x) \
    if ( INDIRECT_MODE() ){\
        ind_data_pag = ind_data_pag-x; \
    }else{\
        data_pag = data_pag-x; \
    }
#define HANDLE_DATA_ROW_INDIRECT_P(x) \
    if ( INDIRECT_MODE() ){\
        ind_data_row = ind_data_row+x; \
    }else{\
        data_row = data_row+x; \
    }
#define HANDLE_DATA_ROW_INDIRECT_N(x) \
    if ( INDIRECT_MODE() ){\
        ind_data_row = ind_data_row-x; \
    }else{\
        data_row = data_row-x; \
    }
#define HANDLE_DATA_COL_INDIRECT_P(x) \
    if ( INDIRECT_MODE() ){\
        data_col = data_col+x; \
    }else{\
        data_col = data_col+x; \
    }
#define HANDLE_DATA_COL_INDIRECT_N(x) \
    if ( INDIRECT_MODE() ){\
        data_col = data_col-x; \
    }else{\
        data_col = data_col-x; \
    }
#define JUMP_CODE_DOWN_AND \
        if ( code_direction == DOWN_DIRECTION ){\
            code_row = code_row+2;\
        }else if ( code_direction == RIGHT_DIRECTION ) begin\
            code_row = code_row+1;\
            code_col = code_col+1;\
        }else if ( code_direction == LEFT_DIRECTION ) begin\
            code_row = code_row+1;\
            code_col = code_col-1;\
        }
#define JUMP_CODE_RIGHT_AND \
        if ( code_direction == DOWN_DIRECTION ){\
            code_col = code_col+1;\
            code_row = code_row+1;\
        }else if ( code_direction == UP_DIRECTION ) begin\
            code_col = code_col+1;\
            code_row = code_row-1;\
        }else if ( code_direction == RIGHT_DIRECTION ) begin\
            code_col = code_col+2;\
        }
#define JUMP_CODE_UP_AND \
        if ( code_direction == UP_DIRECTION ){\
            code_row = code_row-2; \
        }else if ( code_direction == RIGHT_DIRECTION ){\
            code_row = code_row-1; \
            code_col = code_col+1; \
        }else if ( code_direction == LEFT_DIRECTION ){\
            code_row = code_row-1; \
            code_col = code_col-1; \
        }
#define JUMP_CODE_LEFT_AND \
        if ( code_direction == DOWN_DIRECTION ){ \
            code_col = code_col-1; \
            code_row = code_row+1; \
        }else if ( code_direction == UP_DIRECTION ){\
            code_row = code_row-1; \
        }else if ( code_direction == LEFT_DIRECTION ){\
            code_col = code_col-2; \
        }
#define COND_JUMP_CODE_LEFT_AND_INDIRECT(test) \
    if ( ( INDIRECT_MODE() ) && ( TEST_DATA_BIT(idata_col_bits, test ) ) ){\
        JUMP_CODE_LEFT_AND \
    }else{\
        GO_CODE_DIRECTION(1) \
    }
#define COND_JUMP_CODE_UP_AND_INDIRECT(test) \
    if ( ( INDIRECT_MODE() ) && ( TEST_DATA_BIT(idata_col_bits, test ) ) ){\
        JUMP_CODE_UP_AND \
    }else if ( TEST_DATA_BIT(data_col_bits, test ) ){\
        JUMP_CODE_UP_AND \
    }else{\
        GO_CODE_DIRECTION(1) \
    }
#define COND_JUMP_CODE_RIGHT_AND_INDIRECT(test) \
    if ( ( INDIRECT_MODE() ) && ( TEST_DATA_BIT(idata_col_bits, test ) ) ){\
        JUMP_CODE_RIGHT_AND \
    }else if ( TEST_DATA_BIT(data_col_bits, test ) ){\
        JUMP_CODE_RIGHT_AND \
    }else{\
        GO_CODE_DIRECTION(1) \
    }
#define COND_JUMP_CODE_DOWN_AND_INDIRECT(test) \
    if ( ( INDIRECT_MODE() ) && ( TEST_DATA_BIT(idata_col_bits, test ) ) ){\
        JUMP_CODE_DOWN_AND \
    }else if ( TEST_DATA_BIT(data_col_bits, test ) ){\
        JUMP_CODE_DOWN_AND \
    }else{\
        GO_CODE_DIRECTION(1) \
    }
#define COND_JUMP_CODE_LEFT_AND(test) \
    if ( test ){\
        JUMP_CODE_LEFT_AND \
    }else{\
        GO_CODE_DIRECTION(1) \
    }
#define COND_JUMP_CODE_UP_AND(test) \
    if ( test ){\
        JUMP_CODE_UP_AND \
    }else{\
        GO_CODE_DIRECTION(1) \
    }
#define COND_JUMP_CODE_RIGHT_AND(test) \
    if ( test ){\
        JUMP_CODE_RIGHT_AND \
    }else{\
        GO_CODE_DIRECTION(1) \
    }
#define COND_JUMP_CODE_DOWN_AND(test) \
    if ( test ){\
        JUMP_CODE_DOWN_AND \
    }else{\
        GO_CODE_DIRECTION(1) \
    }
#define GO_OPPOSITE_CODE_DIRECTION(x) \
    if ( code_direction == DOWN_DIRECTION ){\
        code_row = code_row+x; \
    }else if ( code_direction == UP_DIRECTION ){\
        code_row = code_row-x; \
    }else if ( code_direction == RIGHT_DIRECTION ){\
        code_col = code_col+x; \
    }else if ( code_direction == LEFT_DIRECTION ){\
        code_col = code_col-x; \
    }
#define GO_CODE_THIS_DIRECTION(d,x) \
    if ( d == DOWN_DIRECTION ){\
        code_row = code_row+x; \
    }else if ( d == UP_DIRECTION ){\
        code_row = code_row-x; \
    }else if ( d == RIGHT_DIRECTION ){\
        code_col = code_col+x; \
    }else if ( d == LEFT_DIRECTION ){\
        code_col = code_col-x; \
    }
#define GO_CODE_THIS_DIRECTION_AND_DIRECTION(d) \
    if ( d == DOWN_DIRECTION ){\
        if ( code_direction == DOWN_DIRECTION ){\
            code_row = code_row+2; \
        }else if ( code_direction == UP_DIRECTION ){\
        }else if ( code_direction == RIGHT_DIRECTION ){\
            code_row = code_row+1; \
            code_col = code_col+1; \
        }else if ( code_direction == LEFT_DIRECTION ){\
            code_row = code_row+1; \
            code_col = code_col-1; \
        }\
    }else if ( d == UP_DIRECTION ){\
        if ( code_direction == DOWN_DIRECTION ){\
        }else if ( code_direction == UP_DIRECTION ){\
            code_row = code_row-2; \
        }else if ( code_direction == RIGHT_DIRECTION ){\
            code_row = code_row-1; \
            code_col = code_col+1; \
        }else if ( code_direction == LEFT_DIRECTION ){\
            code_row = code_row-1; \
            code_col = code_col-1; \
        }\
    }else if ( d == RIGHT_DIRECTION ){\
        if ( code_direction == DOWN_DIRECTION ){\
            code_row = code_row+1; \
            code_col = code_col+1; \
        }else if ( code_direction == UP_DIRECTION ){\
            code_row = code_row-1; \
            code_col = code_col+1; \
        }else if ( code_direction == RIGHT_DIRECTION ){\
            code_col = code_col+2; \
        }else if ( code_direction == LEFT_DIRECTION ){\
        }\
    }else if ( d == LEFT_DIRECTION ){\
        if ( code_direction == DOWN_DIRECTION ){\
            code_row = code_row+1; \
            code_col = code_col-1; \
        }else if ( code_direction == UP_DIRECTION ){\
            code_row = code_row-1; \
            code_col = code_col-1; \
        }else if ( code_direction == RIGHT_DIRECTION ){\
        }else if ( code_direction == LEFT_DIRECTION ){\
            code_col = code_col-2; \
        }\
    }
#define GO_CODE_DIRECTION_FROM_STACK(x) \
    if ( code_direction == DOWN_DIRECTION ){\
        code_row = code_stack_row[code_sp]+x; \
        code_col = code_stack_col[code_sp]; \
    }else if ( code_direction == UP_DIRECTION ){\
        code_row = code_stack_row[code_sp]-x; \
        code_col = code_stack_col[code_sp]; \
    }else if ( code_direction == RIGHT_DIRECTION ){\
        code_col = code_stack_col[code_sp]+x; \
        code_row = code_stack_row[code_sp]; \
    }else if ( code_direction == LEFT_DIRECTION ){\
        code_row = code_stack_row[code_sp]; \
        code_col = code_stack_col[code_sp]-x; \
    }
#define GO_CODE_DIRECTION(x) \
    if ( code_direction == DOWN_DIRECTION ){\
        code_row = code_row+x; \
    }else if ( code_direction == UP_DIRECTION ){\
        code_row = code_row-x; \
    }else if ( code_direction == RIGHT_DIRECTION ){\
        code_col = code_col+x; \
    }else if ( code_direction == LEFT_DIRECTION ){\
        code_col = code_col-x; \
    }
#define GO_OPPOSITE_DATA_DIRECTION(x) \
    if ( data_direction == DOWN_DIRECTION ){\
        HANDLE_DATA_ROW_INDIRECT_N(x) \
    }else if ( data_direction == UP_DIRECTION ){\
        HANDLE_DATA_ROW_INDIRECT_P(x) \
    }else if ( data_direction == RIGHT_DIRECTION ){\
        HANDLE_DATA_COL_INDIRECT_N(x) \
    }else if ( data_direction == LEFT_DIRECTION ){\
        HANDLE_DATA_COL_INDIRECT_P(x) \
    }
#define GO_DATA_THIS_DIRECTION(d,x) \
    if ( d == DOWN_DIRECTION ){\
        HANDLE_DATA_ROW_INDIRECT_P(x) \
    }else if ( d == UP_DIRECTION ){\
        HANDLE_DATA_ROW_INDIRECT_N(x) \
    }else if ( d == RIGHT_DIRECTION ){\
        HANDLE_DATA_COL_INDIRECT_P(x) \
    }else if ( d == LEFT_DIRECTION ){\
        HANDLE_DATA_COL_INDIRECT_N(x) \
    }
#define GO_DATA_DIRECTION(x) \
    if ( data_direction == DOWN_DIRECTION ){\
        HANDLE_DATA_ROW_INDIRECT_P(x) \
    }else if ( data_direction == UP_DIRECTION ){\
        HANDLE_DATA_ROW_INDIRECT_N(x) \
    }else if ( data_direction == RIGHT_DIRECTION ){\
        HANDLE_DATA_COL_INDIRECT_P(x) \
    }else if ( data_direction == LEFT_DIRECTION ){\
        HANDLE_DATA_COL_INDIRECT_N(x) \
    }
#define POSEDGE(x) (x&1)
#define NEGEDGE(x) (!(x&1))
#endif
