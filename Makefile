AWK_ASM=$(PWD)/awk/asm.awk
AWK_INTERP=$(PWD)/awk/interp.awk
RUN_C_ADDER=cd c/cpu; ./cpu ../../adder/adder.awk.code.hex ../../adder/adder.awk.data.hex ../../adder/adder.stack.hex ../../adder/adder.c.data.out >../../adder/adder.c.run.out; diff -b ../../adder/adder.c.data.out ../../adder/adder.awk.data.out && echo Adder awk vs c pass || echo Adder awk vs c fail
RUN_C_HELLO=cd c/cpu; ./cpu ../../hello/hello.awk.code.hex ../../hello/hello.awk.data.hex ../../hello/hello.stack.hex ../../hello/hello.c.data.out >../../hello/hello.c.run.out; diff -b ../../hello/hello.c.data.out ../../hello/hello.awk.data.out && echo Hello world awk vs c pass || echo Hello world awk vs c fail

all: adder/adder.awk.data.hex adder/adder.awk.code.hex c/cpu/cpu hello/hello.awk.data.hex hello/hello.awk.code.hex c/cpu/cpu

adder/adder.awk.data.hex adder/adder.awk.code.hex: adder/adder.asm adder/main.asm $(AWK_INTERP) $(AWK_ASM)
	cd adder; awk -f $(AWK_ASM) adder.asm;cd ..

hello/hello.awk.data.hex hello/hello.awk.code.hex: hello/hello.asm hello/main.asm $(AWK_INTERP) $(AWK_ASM)
	cd hello; awk -f $(AWK_ASM) hello.asm; cd ..

c/cpu/cpu: c/cpu/cpu.c c/cpu/mne.h
	cd c/cpu; gcc -g -o cpu cpu.c; cd ../..

run:
	@cd adder;echo r | awk -f $(AWK_INTERP) adder.asm; cd ..
	@cd hello;echo r | awk -f $(AWK_INTERP) hello.asm; cd ..
	@$(RUN_C_ADDER)
	@$(RUN_C_HELLO)

