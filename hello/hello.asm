INIT
I DATASPACE 3D CARTESIAN
I DATAINIT 1 5 0 0 ;right row col pag
I CODESPACE 3D CARTESIAN
I CODEINIT 0 0 0 0 ;down row col pag

#include ../inc/system.inc
MACRO        MACRO
@outstr=     @guard=
$outstr##### $guard######
############ ############
############ ############
############ ############
############ ############
############ ############
PDI          PDI


MACRO
@call_puts(&outstr,&stdout,&guard)=
PUC
@guard
@stdout
@outstr
@SMI
$puts#
######
######
######
######
######
LCI

DATA
$guard: 111111110 ;guard bits stop on zero
1111111111111111111 ;extra filler crap for testing
$outstr: "Hello world\n" ; text to output.

CODE
x00:00:00: $main:
SKP
HLT
@call_puts(&outstr,&stdout,&guard)
#include ../hello/main.asm
