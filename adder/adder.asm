INIT
I DATASPACE 3D CARTESIAN
I DATAINIT 1 5 0 0 ;right row col pag
I CODESPACE 3D CARTESIAN
I CODEINIT 0 0 0 0 ;down row col pag

#include ../inc/system.inc
MACRO        MACRO        MACRO        MACRO
@a=          @b=          @guard=      @sum=
$a####       $b####       $guard       $sum##
######       ######       ######       ######
######       ######       ######       ######
######       ######       ######       ######
######       ######       ######       ######
######       ######       ######       ######
PDI          PDI          PDI          PDI

MACRO
@call_adder(&sum,&a,&b,&guard)=
PUC   HLT
@sum
@a
@b
@guard
@SMI
$add## ; call $adder ( $guard, $b, $a, $sum )
######
######
######
######
######
LCI

DATA
$sum:   000000000000000000000000000000000000000000000000000000000000000000
$a:     010110011101011001010001111001011010010010101010101010101001010100
$b:     010100000101110000011101111100000110110011110010110000110011100110
$guard: 011111111111111111111111111111111111111111111111111111111111111110

CODE
x00:00:00: $main:
@call_adder(&sum,&a,&b,&guard)
#include ../adder/main.asm
