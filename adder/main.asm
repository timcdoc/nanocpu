MACRO       MACRO
@find_zero= @enter=
CD          POD
+           DIM
?CU

CODE
x00:00:00:
$add:       @enter
            @find_zero
            DU
            JDL
            PUC   CD
            +     +
            !JCL  ?JCR
      +     +     +     +
      ?JCR  ?JCR  !JCL  !JCL
      +o    +i    +o    +i
      ---   ---   ---   ---
      ?JDL  ?JDL  ?JDL  ?JDL
      !JCR  TOC   NOP   JCL
      TOC   +++   !CR   +++   i     QOC   POC
      o     CR    CR    TOC
      CR    QOC   POC
